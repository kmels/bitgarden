import java.awt.event.ActionListener
import java.awt.event.ActionEvent

import javax.swing.JComboBox
import javax.swing.JPanel

class ControlPanel(exchangeNames: Array[String]
                   , exchange1: String
                   , exchange2: String
                   , setExchange1: String => Unit
                   , setExchange2: String => Unit
                 ) {

  val container = new JPanel()
  
  val market1Selector: JComboBox[String] = new JComboBox(exchangeNames)
  val market2Selector: JComboBox[String]  = new JComboBox(exchangeNames)
  
  market1Selector.setSelectedItem(exchange1);
  market2Selector.setSelectedItem(exchange2);

  market1Selector.addActionListener(new ActionListener{
    override def actionPerformed(e: ActionEvent){
      println("Performed: "+e.getClass.toString)
      println("E: "+market1Selector.getSelectedItem)
      setExchange1(market1Selector.getSelectedItem.toString)
    }
  });

  market2Selector.addActionListener(new ActionListener{
    override def actionPerformed(e: ActionEvent){
      println("Performed: "+e.getClass.toString)
      println("E: "+market2Selector.getSelectedItem)
      setExchange2(market2Selector.getSelectedItem.toString)
    }
  });
  
  container add market1Selector
  container add market2Selector
}

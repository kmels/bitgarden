organization := "BitPaga"
name := "BitGarden"
version := "1.0"

publishTo := Some(Resolver.file("file", new File(Path.userHome.absolutePath + "/.m2/repository")))
scalaVersion := "2.11.7"

lazy val commonSettings = Seq(
  version := "1.0",
  organization := "BitPaga",
  scalaVersion := "2.11.7"
)

resolvers += "MarketceteraRepo" at "http://repo.marketcetera.org/maven"
resolvers +=  "Local Maven Repository" at "file://"+Path.userHome+"/.m2/repository"
resolvers += "elasticsearch-releases" at "https://maven.elasticsearch.org/releases"
/* SNAPSHOT dependencies are subject to change anytime so must be resolved on every run. You can suppress this with */
offline := true

unmanagedJars in Compile += Attributed.blank(
  file(System.getenv("JAVA_HOME") + "/jre/lib/jfxrt.jar"))

fork in run := true

libraryDependencies += "org.knowm.xchart" % "xchart" % "3.0.3-SNAPSHOT"

libraryDependencies += "org.knowm.xchange" % "xchange-anx" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-bitfinex" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-bitstamp" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-bittrex" % "4.0.1-SNAPSHOT"
//libraryDependencies += "org.knowm.xchange" % "xchange-bitvc" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-bleutrade" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-btce" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-btcchina" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-cexio" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-coinfloor" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-core" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.apache.mina" % "mina-core" % "2.0.10"
//libraryDependencies += "org.knowm.xchange" % "xchange-examples" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-gatecoin" % "4.0.1-SNAPSHOT"
libraryDependencies += "io.socket" % "socket.io-client" % "0.6.2"
libraryDependencies += "org.knowm.xchange" % "xchange-huobi" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-itbit" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-kraken" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-okcoin" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-poloniex" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-parent" % "4.0.1-SNAPSHOT"
libraryDependencies += "org.knowm.xchange" % "xchange-quoine" % "4.0.1-SNAPSHOT"

libraryDependencies += "commons-codec" % "commons-codec" % "1.10"
libraryDependencies += "org.bitcoinj"      % "bitcoinj-core" % "0.13.3"

/* database */
libraryDependencies += "org.elasticsearch" % "elasticsearch" % "2.1.0"
libraryDependencies += "org.elasticsearch.plugin" % "shield" % "2.1.0"

/* concurrency */
libraryDependencies += "com.googlecode.concurrentlinkedhashmap" % "concurrentlinkedhashmap-lru" % "1.4.2"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.1"
//libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"

libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.4.1"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.4.1"

libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.60-R9"


initialCommands in console := """
import bgutils._
import GlobalConstants._
import UtilImplicits._
import feedback._
import java.math.RoundingMode
import java.math.BigDecimal._

import org.knowm.xchange.anx.v2._
import org.knowm.xchange.bitfinex.v1._
import org.knowm.xchange.bitstamp._
import org.knowm.xchange.bittrex.v1._
import org.knowm.xchange.btce.v3._
import org.knowm.xchange.cexio._
import org.knowm.xchange.gatecoin._
import org.knowm.xchange.gatecoin.service.polling._
import org.knowm.xchange.gatecoin.dto.trade;
import org.knowm.xchange.bitfinex.v1.service.polling._
import org.knowm.xchange.itbit.v1._
import org.knowm.xchange.kraken._
import org.knowm.xchange.kraken.service.polling._
import org.knowm.xchange.poloniex._
import org.knowm.xchange.quoine._

import org.knowm.xchange._
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.dto.marketdata._
import org.knowm.xchange.dto._
import org.knowm.xchange.bitstamp.service.streaming._
import org.knowm.xchange.service.streaming.ExchangeEventType._
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService

import org.knowm.xchange.btcchina.BTCChinaExchange
import org.knowm.xchange.btcchina.service.streaming.BTCChinaStreamingConfiguration

import java.util.Date
import scala.io.Source
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

import org.knowm.xchange.dto.trade._
import org.knowm.xchange.dto.Order.OrderType

val bitfinex = GlobalConstants.makeExchangeOnSpec("Bitfinex", classOf[BitfinexExchange])
val bitstamp = GlobalConstants.makeExchangeOnSpec("Bitstamp", classOf[BitstampExchange])
val bittrex = GlobalConstants.makeExchangeOnSpec("Bittrex", classOf[BittrexExchange])
val btce = GlobalConstants.makeExchangeOnSpec("Btc-e", classOf[BTCEExchange])
val cexio = GlobalConstants.makeExchangeOnSpec("CEX.IO", classOf[CexIOExchange])
val gatecoin = GlobalConstants.makeExchangeOnSpec("Gatecoin", classOf[GatecoinExchange])
val itbit = GlobalConstants.makeExchangeOnSpec("ItBit", classOf[ItBitExchange])
val kraken = GlobalConstants.makeExchangeOnSpec("Kraken", classOf[KrakenExchange])
val poloniex = GlobalConstants.makeExchangeOnSpec("Poloniex", classOf[PoloniexExchange])
val quoine = GlobalConstants.makeExchangeOnSpec("Quoine", classOf[QuoineExchange])
import CurrencyPair._
"""




lazy val app = (project in file("app")).
  settings(commonSettings: _*).
  settings(
    // your settings here
  )

assemblyJarName in assembly := "bitgarden-1.0.jar"

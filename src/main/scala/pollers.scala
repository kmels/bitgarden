import java.util.Date

import bgutils.APIStates.{APIStatus, APIOFF}
import bgutils.{APIStates, GlobalConstants, CurrencyPriceListing}
import org.knowm.xchange.exceptions.ExchangeException
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService
import org.knowm.xchange.{ExchangeFactory, Exchange}
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata.{OrderBook, Trade}
import org.apache.logging.log4j.LogManager
import panels.PriceChart
import seekers.BOOK_UPDATE
import si.mazi.rescu.HttpStatusIOException
import types.BufferedTimeSeries

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.concurrent.duration.Duration
import scala.concurrent.duration._

import GlobalConstants._
import scala.collection.mutable.Map

package object pollers {
  import GlobalConstants._
  import akka.actor.Actor

  sealed trait PollRequest

  case class GET_DEPTH(m: (String, CurrencyPair), tick: Int) extends PollRequest
  case class GET_TRADES(m: (String, CurrencyPair), tick: Int) extends PollRequest

  case class OrderbookUpdate(o: OrderBook, latency: Duration)

  sealed trait PollingScheduler {

    def isNext(cp: CurrencyPair, pollRequest: PollRequest): Boolean

    def lastCounted : Long
  }

  class KrakenScheduler extends PollingScheduler {
    var _count = 0d
    val maxCount = 15
    var _lastCounted: Long = new Date().getTime
    var nextPairPos = 0
    val krakenPairs = currencyListings.filter(_._2.contains("Kraken")).keys.toList

    def lastCounted = _lastCounted

    def isNext(cp: CurrencyPair, r: PollRequest): Boolean = {
      val now = new Date().getTime
      val idleTime = now - _lastCounted

      logger.trace(s"Kraken $cp. Scheduler, idleTime = $idleTime, count = ${_count}, last counted: ${new Date(lastCounted)}")

      if (idleTime > 2000) {
        //"a couple secondss"
        _lastCounted = now
        val _decrement = idleTime.toDouble / 2000
        _count = _count - _decrement
        logger.trace(s"Kraken $cp. Scheduler, idleTime = $idleTime, decreased payoff by ${_decrement}, count = ${_count}")
      }

      val nextPair = krakenPairs(nextPairPos)
      logger.trace(s"Kraken $cp. Scheduler has next: $nextPair, nextPos = $nextPairPos, pairs = ${krakenPairs.mkString(",")}")

      val turn = _count < maxCount && nextPair.equals(cp)

      if (turn) {
        _lastCounted = now

        val payoff = (if (r.isInstanceOf[GET_TRADES]) 2 else if (r.isInstanceOf[GET_DEPTH]) 1 else {
          logger.warn(s"Doesn't make sense to be counting ${r.getClass}.. " )
          0
        })

        _count += payoff
        logger.trace(s"Kraken $cp. Scheduler, added cost of ${payoff}: ${r.getClass}, count = ${_count}")

        if (nextPairPos+1 < krakenPairs.size) nextPairPos += 1 else nextPairPos = 0
      }

      turn
    }

  }

  class PollerActor(poller: PollingMarketDataService, scheduler: Option[PollingScheduler] = None) extends Actor {
    val logger = LogManager.getLogger(getClass)

    def reviveMaybe(n: String, cp: CurrencyPair, t: Int, off: APIOFF): Unit = {
      val now = new Date().getTime
      val timeOff = now - off.time
      if (timeOff > 1000){
        logger.info(s"Tick $t $n. Turning api on after $timeOff ms. ")

        APIStates.init((n,cp))
        //apiStatus.put(n, APIStatus(OK = true, BUSY = false, now))
      } else {
        logger.info(s"Tick $t $n. Leaving api off after $timeOff ms.")
      }
    }

    /* Sets the api state to OFF */
    def failGracefully(n: String, cp: CurrencyPair, m: String, t: Int, err: Throwable) : Unit = err match {
      case e: org.knowm.xchange.exceptions.ExchangeException => {
        logger.warn(s"${n},${cp}: ExchangeException en ${m} - ${e}")
        apiFail(n, cp, t)
        logger.trace(s"Tick ${t}. ${n}. La API fallo.")
      }

      case e: HttpStatusIOException => {
        logger.warn(s"${n} ${cp}: ExchangeException en ${m} - ${e} ")
        apiFail(n, cp, t)
        logger.trace(s"Tick ${t}. ${n}. La API fallo.")
      }

      case e: java.net.SocketTimeoutException => {
        logger.warn(s"${n} ${cp}: SocketTimeoutException en ${m} - ${e}")
        apiFail(n, cp, t)
        logger.trace(s"Tick ${t}. ${n}. La API fallo.")
      }

      case e: java.net.ConnectException => {
        logger.warn(s"${n} ${cp}: ConnectException en ${m} - ${e}")
        apiFail(n, cp, t)
        logger.trace(s"Tick ${t}. ${n}. La API fallo.")
        pollPerformances.remove(cp)
      }

      case e: java.net.UnknownHostException => {
        logger.warn(s"Tick ${t}. ${n} ${cp}: UnknownHostException en ${m} - ${e}")
        apiFail(n, cp, t)
        logger.trace(s"Tick ${t}. ${n}. La API fallo.")
      }

      case off@APIOFF(time: Long) => {
        reviveMaybe(n,cp,t,off)
      }

      case asdf : Throwable => {
        logger.error(s"Tick $t. $n $cp poller recibio ASDF GER ?????? " + asdf.getClass.toString)
      }
    }

    def busy(m: (String,CurrencyPair)): Boolean = {
      pollPerformances.contains(m)
    }

    def idle(cp: CurrencyPair, r: PollRequest): Boolean = {
      scheduler.isDefined && !scheduler.get.isNext(cp, r)
    }

    def perform(r: GET_TRADES): Either[APIStatus, BufferedTimeSeries[Trade]] = {
      val m@(n,cp)= r.m
      val t = r.tick

      val listing = currencyListings(cp)(n)

      val response: Either[APIStatus, BufferedTimeSeries[Trade]] = try {
        logger.trace(s"Tick $t. $n $cp. Poller (TRADES) buscando health ... ${APIStates(m).OK}")

        val health = APIStates.getHealth(m)
        logger.trace(s"Tick $t. $n $cp. Descargando precios." )

        val before = new Date().getTime
        pollPerformances.put(m, before)
        APIStates.acquire(m, t)

        val trades: java.util.List[Trade] = poller.getTrades(listing._1).getTrades()
        val after = new Date().getTime
        pollPerformances.remove(m)
        APIStates.release(m, t)
        //APIStates.put(n, APIStatus(OK = true, BUSY = false, before))

        logger.trace(s"${n} $cp. Tick ${t}. Se descargaron ${trades.size()} precios ${cp} ... en ${after-before}ms. Listing: ${listing} " )

        val adaptedTrades: Seq[Trade]  = if (listing._2) trades else trades.map(CurrencyPriceListing.invert)

        GlobalConstants.marketTradeUpdates.put(m, new Date().getTime)
        val ps = new BufferedTimeSeries(n, adaptedTrades, 90 minutes)
        marketTrades.put(m, ps)
        Right(ps)
      } catch {
        case off: APIOFF => {
          val st = APIStates(m)
          logger.warn(s"${n} ${cp}. Tick ${t}. GET_TRADES. API is down, time down: ${new Date().getTime - st.TIME}ms ")
          reviveMaybe(n,cp,t,off)
          Left(st)
        } case e : Throwable => {
          failGracefully(n,cp,"trades",t, e)
          logger.warn(s"${n} no pudo encontrar health ... ")
          pollPerformances.remove(m)
          Left(APIStates(m))
        }
      }

      response
    }

    // poll
    def perform(r: GET_DEPTH): Either[APIStatus, OrderbookUpdate] = {
      val m@(n,cp)= r.m

      val t = r.tick
      val listing = currencyListings(cp)(n)

      val response : Either[APIStatus, OrderbookUpdate] = try {
        logger.trace(s"Tick $t. $n $cp. Poller (DEPTH) buscando health ... ${APIStates(m).OK}")

        val health = APIStates.getHealth(m)
        logger.trace(s"Tick $t. $n $cp. Poller. Descargando libros." )

        val before = new Date().getTime
        //logger.info(s"${n} ${p} Putting performance ")
        pollPerformances.put(m, before)
        APIStates.acquire(m,t)

        //descargar
        val limitBook: OrderBook = poller.getOrderBook(listing._1)

        val after = new Date().getTime
        //logger.trace(s"${n} ${p} Freeing performance")
        pollPerformances.remove(m)
        APIStates.release(m,t)

        // adaptar
        val ab = if (listing._2) limitBook else CurrencyPriceListing.invert(limitBook)
        logger.trace(s"Tick ${t}. ${n} ${cp}. Se descargaron libros ${cp} ... " +
          s"en ${after - before}ms. ask: ${ab.getAsks.headOption.map(_.getLimitPrice)} bid: ${ab.getBids.headOption.map(_.getLimitPrice)}")

        // actualizar
        GlobalConstants.marketBookUpdates.put(m, new Date().getTime)
        marketBooks.put(m, ab)

        val latency = Duration(after - before, MILLISECONDS) //TODO add latency to performance

        val b = OrderbookUpdate(ab, latency)
        Right(b)
      } catch {
        case off: APIOFF => {
          val st = APIStates(m)
          logger.warn(s"${n} ${cp}. Tick ${t}. GET_DEPTH. API is down, time down: ${new Date().getTime - st.TIME}ms ")
          reviveMaybe(n,cp,t,off)
          Left(st)
        }
        case e : Throwable => {
          logger.warn(s"${n} ${cp}. Tick ${t}. GET_DEPTH Respondiendo con un error - ${e.getClass.toString}")
          failGracefully(n, cp, "depth", t, e)
          pollPerformances.remove(m)
          Left(APIStates(m))
        }
      }

      response
    }

    def receive = {

      case r@GET_DEPTH(m, t) => {
        val (n,cp) = m

        val performance = if (busy(n, cp)) {
          logger.error(s"Tick $t. $n $cp poller está ocupado.")
          Left(pollPerformances((n,cp)))
        } else if (idle(cp, r)) {
          logger.trace(s"Tick $t. $n $cp poller no está listo. Scheduler says GET_DEPTH request is not next. (CURRENT = ${pollPerformances.mkString(",")})")
          Left(scheduler.get.lastCounted)
        } else  {
          logger.trace(s"Tick $t. $n $cp poller no está ocupado.")
          val _response = perform(r)
          if (_response.isRight){
            val orderbookUpdate = _response.right.get
            marketSeekers(n) ! BOOK_UPDATE(n, cp, t, orderbookUpdate.latency)
          }
          logger.trace(s"Tick ${t}. $n $cp poller realizó GET_DEPTH ... esta bien? ${_response.isRight}")
          _response
        }

      }

      case r@GET_TRADES(m, t) => {
        val (n,cp) = m
        val listing = currencyListings(cp)(n)

        val response = if (busy(n, cp)) {
          logger.error("Shouldn't be called")
          Left(pollPerformances((n,cp)))
        } else if (idle(cp, r)) {
          logger.trace(s"Tick $t. $n $cp Scheduler says GET_TRADES request is not next. (CURRENT = ${pollPerformances.mkString(",")})")
          Left(scheduler.get.lastCounted)
        } else {
          logger.trace(s"$n $cp Poller no ocupado: $n $cp. Tick $t")
          val _response = perform(r)
          logger.trace(s"$n $cp Poller $n $cp tiene respuesta para GET_TRADES en el tick $t... esta bien? ${_response.isRight}")
          _response
        }
      }

    }

  }

  class BitfinexPoller  (poller: PollingMarketDataService) extends PollerActor (poller)
  class BitstampPoller  (poller: PollingMarketDataService) extends PollerActor (poller)
  class BleutradePoller (poller: PollingMarketDataService) extends PollerActor (poller)
  class BittrexPoller   (poller: PollingMarketDataService) extends PollerActor (poller)
  class BTCEPoller      (poller: PollingMarketDataService) extends PollerActor (poller)
  class CEXIOPoller     (poller: PollingMarketDataService) extends PollerActor (poller)
  class GatecoinPoller  (poller: PollingMarketDataService) extends PollerActor (poller)
  class ItBitPoller     (poller: PollingMarketDataService) extends PollerActor (poller)
  class KrakenPoller    (poller: PollingMarketDataService) extends PollerActor (poller, Option(new KrakenScheduler))
  class PoloniexPoller  (poller: PollingMarketDataService) extends PollerActor (poller)

}

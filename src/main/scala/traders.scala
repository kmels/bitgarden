import akka.actor.Actor
import bgutils._
import bgutils.UtilImplicits._
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.Order
import org.knowm.xchange.dto.trade.{LimitOrder, MarketOrder}
import org.knowm.xchange.service.polling.trade.PollingTradeService
import org.apache.logging.log4j.LogManager
import seekers.SeekerActor
import java.util.{Date,Timer,TimerTask}

import org.bitcoinj.core.Sha256Hash
import GlobalConstants._
import feedback.{BUY_FEEDBACK_REQUEST, SELL_FEEDBACK_REQUEST}

package object traders {
  sealed trait TradeRequest
  case class BUY_ORDER(n: String, cp: CurrencyPair,
                       amount: Volume, priceTarget: Price,
                       tick: Int, arbHash: Sha256Hash,
                       opportunityMatrix: OpportunityMatrix) extends TradeRequest

  case class SELL_ORDER(n: String, cp: CurrencyPair,
                        amount: Volume, priceTarget: Price,
                        tick: Int, arbHash: Sha256Hash,
                        opportunityMatrix: OpportunityMatrix) extends TradeRequest

  class TradeActor(val exchangeName: String, val tradeAgent: PollingTradeService, val supportsMarketOrder: Boolean = true) extends Actor{
    val logger = LogManager.getLogger(getClass)

    def finishArbitrage(t: Int, h : Sha256Hash): Unit = {
      val arb: CurrencyArbitrage = marketArbitrages.get(h).getOrElse(return)

      val bought = arb.buys.map(_.amount).sum
      val isBought = bought.equals(arb.amount)

      val sold = arb.sells.map(_.amount).sum
      val isSold = sold.equals(arb.amount)

      logger.info(s"Terminando arbitrage $h ... bought = $bought; sold = $sold; isBought = $isBought; isSold = $isSold")

      if (isBought && isSold) {

        val currencyInUSD: Option[Price] = agents.currencyPriceInDollar(arb.om.cp.counter.toString)
        logger.info(s"Tick $t. $h. Arbitrage COMPLETADO ... $arb - " +
          s"expected profit = ${arb.expectedProfit} ${arb.om.cp.counter}, ca " +
          s"(${currencyInUSD.map(_.multiply(arb.expectedProfit))})" +
          s" @ ${currencyInUSD}")

        val feedbackTask = new TimerTask {

          override def run(): Unit = {
            val (buyer, seller, cp) = arb.combo

            val buyId = arb.buys.head.id
            val buyTarget = arb.om.buyOrders.head._1
            val buyFeedback = feedback.orderFeedback(buyer, BUY_FEEDBACK_REQUEST(buyTarget, buyId, cp))
            if (buyFeedback.isEmpty)
              logger.warn(s"$h couldn't have order feedback: $buyer $buyId (buy)")

            val sellId = arb.sells.head.id
            val sellTarget = arb.om.sellOrders.head._1
            val sellFeedback = feedback.orderFeedback(seller, SELL_FEEDBACK_REQUEST(sellTarget, sellId, cp))
            if (sellFeedback.isEmpty)
              logger.warn(s"$h couldn't have order feedback: $seller $sellId (sell)")

            val profit: Option[Volume] = sellFeedback.map(f => f.cost).flatMap(revenue =>
              buyFeedback.map(f => f.cost).map(cost => revenue.subtract(cost))
            )

            logger.info(s"Tick $t. $h. Arbitrage COMPLETADO ... " +
              s" cost = ${buyFeedback.map(_.cost)} " +
              s" revenue = ${sellFeedback.map(_.cost)} " +
              s" actual profit = ${profit}" +
              s" buyer slippage = ${buyFeedback.map(_.slippage)}" +
              s" seller slippage = ${sellFeedback.map(_.slippage)} -- http://eratostenes:9200/arb/${cp.base.toString.toLowerCase()}-${cp.counter.toString.toLowerCase()}/$h")
            
            
            try {
              logger.info(s"Guardando arbitrage .. $h")
            
              val json = SavedData.saveArbitrage(h, arb, buyFeedback, sellFeedback, profit, arb.om)
              
              logger.info(s"Guardando arbitrage .. $h ... HECHO. ${json}")
              
            } catch {
              case e: Throwable => logger.error("Error al guardar arbitrage: $h",e)
            }

          }
        }
                
        new Timer(s"SaveArbitrage $h").schedule(feedbackTask, 8000)

        marketArbitrages.remove(key = h)
      } else {
        val age = new Date().getTime - marketArbitrages(h).time
        logger.warn(s"ARBITRAGE $h no ha terminado despues de $age ms... ${marketArbitrages(h)}")
      }
    }

    /* Seeker replies with the number of opportunities */
    def receive = {

      case BUY_ORDER(n, cp, amount, pt, t, h, om) => {
        val m = (n,cp)
        logger.info(s"Tick $t. $h. $n BUYING $amount $cp at target $pt .. balance = ${accountBalances.get((n, cp.base)).map(_.getTotal)} ${cp.counter.toString}")
        val o = new MarketOrder(Order.OrderType.BID, amount, cp)
        val id : Option[String] = try {
          val _placedId = if (supportsMarketOrder)
            tradeAgent.placeMarketOrder(o)
          else {
            val buyPrice = marketBooks(m).getAsks.get(0).getLimitPrice
            logger.warn(s"Tick $t. $h. $n BUYING $amount $cp ... DOESNT SUPPORT MARKET ORDER, ONLY LIMIT. BUYING AT $buyPrice ${cp.counter}" +
              s" ...  ${om.executables.map(_._1)} ...")
            //throw new Exception(s"Tick $t. $h. $n BUYING $amount $cp ... CANNOT BUY - DOESNT SUPPORT MARKET ORDER")
            tradeAgent.placeLimitOrder(LimitOrder.Builder.from(o).limitPrice(buyPrice).build())
          }

          logger.info(s"Tick $t. $h. $n BUYING $amount $cp at target $pt ... BOUGHT ${_placedId} ")
          marketArbitrages(h).buys += FulfilledOrder(_placedId, amount, new Date().getTime)
          Some(_placedId)
        } catch {
          case e: Exception => {
            GlobalUtils.soundPlayer ! "/home/campesino/bitgarden/src/main/resources/sounds/woop-woop.wav"
            logger.error(s"Tick $t. $h. $n BUYING $amount $cp at target $pt ... FAILED - ${e.toString}", e)
            None
          }
        }

        marketBooks.remove(m)
        marketBookUpdates.remove(m)
        accountBalances.remove((n,cp.counter))

        finishArbitrage(t, h)
      }

      case SELL_ORDER(n, cp, amount, pt, t, h, om) => {
        val m = (n,cp)
        logger.info(s"Tick $t. $h. $n SELLING $amount $cp at target $pt .. balance = ${accountBalances.get((n, cp.counter)).map(_.getTotal)} ${cp.counter.toString}")
        val o = new MarketOrder(Order.OrderType.ASK, amount, cp)

        val id : Option[String] = try {
          val _placedId = if (supportsMarketOrder)
            tradeAgent.placeMarketOrder(o)
          else {
            val sellPrice = marketBooks(m).getBids.get(0).getLimitPrice
            logger.warn(s"Tick $t. $h. $n SELLING $amount $cp ... DOESNT SUPPORT MARKET ORDER, ONLY LIMIT. SELLING AT $sellPrice ${cp.counter}" +
              s"${om.executables.map(_._2)}")
            //throw new Exception(s"Tick $t. $n SELLING $amount $cp ... CANNOT SELL - DOESNT SUPPORT MARKET ORDER")
            tradeAgent.placeLimitOrder(LimitOrder.Builder.from(o).limitPrice(sellPrice).build())
          }

          logger.info(s"Tick $t. $h. $n SELLING $amount $cp at target $pt ... SOLD ${_placedId}")
          marketArbitrages(h).sells += FulfilledOrder(_placedId, amount, new Date().getTime)
          Some(_placedId)
        } catch {
          case e: Exception => {
            GlobalUtils.soundPlayer ! "home/campesino/bitgarden/src/main/resources/sounds/woop-woop.wav"
            logger.error(s"Tick $t. $h. $n SELLING $amount $cp at target $pt ... FAILED - ${e.toString}", e)
            None
          }
        }

        marketBooks.remove(key = m)
        marketBookUpdates.remove(key = m)
        accountBalances.remove((n,cp.base))
        finishArbitrage(t, h)
      }

      case x => new Exception(s".... THE INEVITABLE HAPPENED (TRADES) ${x}")
    }
  }

  class BitfinexTrader(n: String, t: PollingTradeService)    extends TradeActor(n, t)
  class BitstampTrader(n: String, t: PollingTradeService)    extends TradeActor(n, t, false)
  class BleutradeTrader(n: String, t: PollingTradeService)   extends TradeActor(n, t)
  class BittrexTrader(n: String, t: PollingTradeService)     extends TradeActor(n, t)
  class BTCETrader(n: String, t: PollingTradeService)        extends TradeActor(n, t)
  class CEXIOTrader(n: String, t: PollingTradeService)       extends TradeActor(n, t)
  class GatecoinTrader(n: String, t: PollingTradeService)    extends TradeActor(n, t)
  class ItBitTrader(n: String, t: PollingTradeService)       extends TradeActor(n, t, false)
  class KrakenTrader(n: String, t: PollingTradeService)      extends TradeActor(n, t)
  class PoloniexTrader(n: String, t: PollingTradeService)    extends TradeActor(n, t, false)

}

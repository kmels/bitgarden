package huob

import org.knowm.xchange._
import org.knowm.xchange.currency._
import org.knowm.xchange.huobi._
import org.knowm.xchange.service.polling.account._
import org.knowm.xchange.service.polling.trade._
import org.knowm.xchange.service.polling.marketdata._
import org.knowm.xchange.dto.trade._
import org.knowm.xchange.dto._

class HUOBRebalance()  {


  val huobAPI = "1b363f76-f06a546c-9dffcd69-bfeae"
  val huobSEC = "4f840de2-b32b7093-bed0f60b-30f9b"

  //val huobiSpec: ExchangeSpecification = new ExchangeSpecification(classOf[HuobiExchange])

  //val huob = ExchangeFactory.INSTANCE.createExchange(huobiSpec);
  val huob = ExchangeFactory.INSTANCE.createExchange(classOf[HuobiExchange].getName);
  val huobiSpec = huob.getDefaultExchangeSpecification()
  huobiSpec.setSecretKey(huobSEC);
  huobiSpec.setApiKey(huobAPI);

  huob.applySpecification(huobiSpec)

  val huobiAccountService: PollingAccountService  = huob.getPollingAccountService();
  val huobiTradeAgent: PollingTradeService  = huob.getPollingTradeService();
  val huobiPoller: PollingMarketDataService = huob.getPollingMarketDataService

  val huobiAccount = huobiAccountService.getAccountInfo.getWallet

  val huobBTC = huobiAccount.getBalance(Currency.BTC).getAvailable
  val huobCNY = huobiAccount.getBalance(Currency.CNY).getAvailable

  /* Checkpoint 1
   * ---
   *
   * 0.0479 BTC
   * 5249.52 CNYiim
   */

  val huobPrices =
    huobiPoller.getOrderBook(CurrencyPair.BTC_CNY)
  val BTC_CNY_value = huobPrices.getBids.get(0).getLimitPrice

  val huobBTC_CNY = huobBTC.multiply(BTC_CNY_value)

  val diff = huobCNY.subtract(huobBTC_CNY)

  println ("BTC: " + huobBTC)
  println ("CNY: " + huobCNY)

  val currencyPair = CurrencyPair.BTC_CNY

  val order = if (huobBTC_CNY.compareTo(huobCNY) < 0) {
    println("Sell CNY. Buy BTC")

    val huobPrices = huobiPoller.getOrderBook(CurrencyPair.BTC_CNY)
    val BTC_CNY_value = huobPrices.getBids.get(0).getLimitPrice

    val amount = diff.divide(java.math.BigDecimal.valueOf(2d)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP)

    println(amount)

    val order: MarketOrder = new MarketOrder(Order.OrderType.BID, amount, currencyPair);
    order
  } else {
    println("Sell BTC. Buy CNY")
    val cnyamount = diff.abs().divide(java.math.BigDecimal.valueOf(2d)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP)

    val btcamount = cnyamount.divide(BTC_CNY_value, 2, java.math.RoundingMode.HALF_UP)
    println(btcamount)

    val order: MarketOrder = new MarketOrder(Order.OrderType.ASK, btcamount, currencyPair);
    order
  }
}

import java.math.RoundingMode

import akka.actor.Actor
import bgutils.GlobalConstants._
import bgutils.MemoryData._
import bgutils.MemoryData.logger
import bgutils._
import bgutils.UtilImplicits.{ExchangeName, ArbitrageCombo}
import bgutils.UtilImplicits._

import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.account.Balance
import org.knowm.xchange.dto.marketdata.OrderBook
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService
import org.apache.logging.log4j.LogManager
import org.bitcoinj.core.Sha256Hash
import panels.OpportunityPanel
import seekers.SeekerActor
import traders.{SELL_ORDER, BUY_ORDER}

import scala.collection.mutable.ListBuffer
import scala.collection.parallel.ParIterable
import scala.collection.parallel.mutable.ParSeq
import scala.concurrent.duration.Duration
import java.util.Date

import java.math.BigDecimal._
import java.math.RoundingMode

import scala.collection.JavaConversions._

package object seekers {
  case class BOOK_UPDATE(n: String, cp : CurrencyPair, t: Int, lat: Duration)

  class SeekerActor extends Actor{
    val logger = LogManager.getLogger(getClass)

    def processArbitrage(buyerMeta: (String, OrderBook), sellerMeta: (String, OrderBook), cp: CurrencyPair, t: Int): Boolean = {

      val (buyerName, buyerBook) = buyerMeta
      val (sellerName, sellerBook) = sellerMeta
      val combo = (buyerName, sellerName, cp)

      // check balances
      if (!accountBalances.contains((sellerName, cp.base)))
        return false
      if (!accountBalances.contains((buyerName, cp.counter)))
        return false

      val sellerBalance = accountBalances((sellerName, cp.base)).getAvailable
      val buyerBalance = accountBalances((buyerName, cp.counter)).getAvailable

      // get OM
      val topask = buyerBook.getAsks.get(0)
      val topbid = sellerBook.getBids.get(0)

      assert ( topask.getLimitPrice().compareTo(topbid.getLimitPrice) < 0)
      val bd@(asks, bids) = bookData((buyerName,cp), (sellerName,cp))
      assert ( asks.head.getLimitPrice().compareTo(bids.head.getLimitPrice) < 0)

      val om = new OpportunityMatrix(MemoryData.makeDepth(bd), FEE_SPEC(buyerName), FEE_SPEC(sellerName), cp)
      val arbs = om.executables.unzip

      /* Check market orders */
      if (!SUPPORTS_MARKET_ORDERS(buyerName) || !SUPPORTS_MARKET_ORDERS(sellerName)) {
        marketOpportunities.put(key = combo, value = (om, OPS_TODO))
        logger.trace(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. TODO: limit orders. Arbitrage forbidden.")
        return false
      }

      /* Check needs flip */
      val buyerListing = currencyListings(cp)(buyerName)
      val sellerListing = currencyListings(cp)(sellerName)
      val flip = (!buyerListing._2 || !sellerListing._2)

      if (flip) {
        logger.trace(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. TODO: Flip order pair. Arbitrage forbidden.")
        marketOpportunities.put(key = combo, value = (om, OPS_TODO))
        return false
      }

      logger.debug(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. " +
        s"Opportunity. Executables: ${arbs._1.size}. Profit over cost: ${om.pocPercentage} - (${om.poc})")

      logger.trace(s"Updating combo: ${combo}")
      marketOpportunities.put(key = combo, value = (om, OPS_DETECTED))

      val buys = arbs._1
      val sells = arbs._2

      // only execute 25% of the opportunity for now.
      val hardcorePercentage = java.math.BigDecimal.valueOf(0.85d)
      val liquidity = buys.map(_._2).sum
      var arbAmount : java.math.BigDecimal = sells.map(_._2).sum.multiply(hardcorePercentage)

      if (arbAmount.compareTo(ZERO) <= 0) {
        logger.trace(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. MISSING LIQUIDITY 1, arbAmount =" +
          s" ${arbAmount}; liquidity = ${sells.map(_._2).sum} .. $liquidity")
        marketOpportunities.put(key = combo, value = (om, OPS_MISSING_LIQUIDITY))
        return false
      }

      if (arbAmount.compareTo(MIN_ARB_SPEC(cp)) <= 0) {
        logger.trace(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. MISSING LIQUIDITY 2, arbAmount =" +
          s" ${arbAmount}; liquidity = ${sells.map(_._2).sum} .. $liquidity")
        marketOpportunities.put(key = combo, value = (om, OPS_MISSING_LIQUIDITY))
        return false
      }

      // every exchange own at least some base/counter position
      val minimumSellerBalance = MemoryData.getAccountMinimum(cp.base)
      val minimumBuyerBalance = MemoryData.getAccountMinimum(cp.counter)

      var sellAmount : java.math.BigDecimal =
        if (sellerBalance.subtract(minimumSellerBalance).compareTo(arbAmount) >= 0) {
        logger.debug(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Arbitrage opportunity. " +
          s"Seller will have enough balance after selling $arbAmount ${cp.base}")
        arbAmount
      } else {
        val _sellAmount = sellerBalance.subtract(minimumSellerBalance)
        logger.debug(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Arbitrage opportunity. " +
          s"Seller shouldn't sell $arbAmount (sellerBalance = $sellerBalance; minimumSellerBalance = " +
          s"$minimumSellerBalance) only ${_sellAmount} ....")
        _sellAmount
      }

      if (sellAmount.compareTo(ZERO) <= 0) {
        logger.trace(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Insufficient funds. Seller doesn't have $arbAmount ${cp.base}, only ${sellerBalance}")
        marketOpportunities.put(key = combo, value = (om, OPS_INSUFFICIENT_SELLER_FUNDS))
        return false
      }

      val maxBalanceRate = java.math.BigDecimal.valueOf(0.60)

      if (sellAmount.compareTo(MIN_ARB_SPEC(cp)) <= 0) {
        logger.info(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. INSUFFICIENT FUNDS (SELL), arbAmount =" +
          s" ${arbAmount}; sellAmount = ${sellAmount} liquidity = $liquidity")
        marketOpportunities.put(key = combo, value = (om, OPS_INSUFFICIENT_SELLER_FUNDS))
        return false
      } else {
        val maxAmount = sellerBalance.multiply(maxBalanceRate)
        if (sellAmount.compareTo(maxAmount)>=0)
          sellAmount = maxAmount
      }

      val counterCost = marketBooks.get((buyerName, cp)).map(b =>
        GlobalUtils.costOfBuying(sellAmount, b, FEE_SPEC(buyerName))).getOrElse(return false)
      //cost of buying sellAmount

      var buyAmount : java.math.BigDecimal = if (buyerBalance.subtract(minimumBuyerBalance).compareTo(counterCost) >= 0) {
        logger.debug(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Opportunity. Buyer will have enough balance post $arbAmount, " +
          s" spending ${counterCost}")
        sellAmount
      } else {
        var spendable = buyerBalance.subtract(minimumBuyerBalance);
        val buyFee = FEE_SPEC(buyerName)

        val maxAmount = buyerBalance.multiply(maxBalanceRate)
        if (spendable.compareTo(maxAmount)>=0)
            spendable = maxAmount

        val ba = MemoryData.getBuyAmount(cp, spendable, marketBooks((buyerName, cp)), buyFee)

        logger.debug(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Opportunity. Buyer shouldn't buy " +
          s"$arbAmount (buyerBalance = $buyerBalance; minimumBuyerBalance = "+
          s"$minimumBuyerBalance) only " +
          s" $ba spending $spendable ${cp.counter}  .... ${marketBooks((buyerName, cp)).getAsks.take(10).mkString(",")} - Fee: ${buyFee}")
        ba
      }

      if (buyAmount.compareTo(ZERO) <= 0) {
        logger.trace(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Insufficient funds. Buyer doesn't have ${counterCost} ${cp.counter} " +
          s"only ${buyerBalance} - liquidity: ${liquidity}")
        marketOpportunities.put(key = combo, value = (om, OPS_INSUFFICIENT_BUYER_FUNDS))
        return false
      }

      if (buyAmount.compareTo(MIN_ARB_SPEC(cp)) <= 0) {
        logger.info(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Insufficient funds. Buyer doesn't have ${counterCost} ${cp.counter} " +
          s"only ${buyerBalance}")
        marketOpportunities.put(key = combo, value = (om, OPS_INSUFFICIENT_BUYER_FUNDS))
        return false
      }

      val goAmount = buyAmount.min(sellAmount)
      assert(goAmount.compareTo(arbAmount) <= 0, s"Tick $t Buy ${buyerName} - Sell ${sellerName} ${cp}. Go amount ${goAmount} " +
        s"shouldn't be greater than ${arbAmount}. Buy: ${buyAmount}, Sell: ${sellAmount}")
      logger.debug(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. Checking requirements ... " +
        s"buyAmount = $buyAmount; sellAmount = $sellAmount; goAmount = $goAmount; minArbSpec = ${MIN_ARB_SPEC(cp)}")

      val nDecimals = PIP_SPEC(cp).toString.count(_ == '0')
      arbAmount = goAmount.setScale(nDecimals, java.math.RoundingMode.HALF_EVEN)
      buyAmount = arbAmount
      sellAmount = arbAmount

      val liquidPercentage = arbAmount.divide(liquidity, 6, RoundingMode.HALF_EVEN).multiply(HUNDRED).setScale(2, RoundingMode.HALF_EVEN)
      assert (arbAmount.compareTo(ZERO) > 0)
      assert (arbAmount.compareTo(MIN_ARB_SPEC(cp)) > 0)

      /* Acceptable to arbitrage */
      logger.info(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. " +
        s"We should arbitrage ${arbAmount} ($liquidPercentage % of ${liquidity}). " +
        s"OM ... $om")

      val buyerArb  = marketArbitrages.find(_._2.combo._1.equals(buyerName))
      val sellerArb = marketArbitrages.find(_._2.combo._2.equals(sellerName))

      val isArbitraging = buyerArb.isDefined || sellerArb.isDefined

      val willArbitrage = if (isArbitraging) {
        logger.warn(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. " +
          s"Not arbitraging because arb is being performed... Buyer: $buyerArb. Seller: $sellerArb")

        marketOpportunities.put(key = combo, value = (om, OPS_BLOCKED))
        false
      } else {
        val arbHash: Sha256Hash = Sha256Hash.twiceOf(s"${new Date().getTime.toString} - $combo - $buyAmount ".getBytes("UTF-8"))

        marketOpportunities.put(key = combo, value = (om, OPS_ONGOING))

        marketArbitrages.put(arbHash, CurrencyArbitrage(combo, arbAmount, om,
          ListBuffer.empty, ListBuffer.empty))

        marketTraders(buyerName) ! BUY_ORDER(
          buyerName, buyerListing._1,
          buyAmount, buys.head._1, t, arbHash, om
        )

        marketTraders(sellerName) ! SELL_ORDER(
          sellerName, sellerListing._1,
          sellAmount, sells.head._1, t, arbHash, om
        )

        logger.info(s"Tick $t. Buy ${buyerName} - Sell ${sellerName} ${cp}. " +
          s"Arbitrage of $arbAmount ${cp.base} (${liquidPercentage}%, full liquidity: $liquidity) assigned hash $arbHash. " +
          s"Total ${cp.base} Balance: ${accountBalances.filter(_._1._2.equals(cp.base)).values.map(_.getTotal).sum}")

        GlobalUtils.soundPlayer ! "/home/campesino/bitgarden/src/main/resources/sounds/sonicboom.wav"

        true
      }

      willArbitrage
    }

    def updateArbitrage(buyer: (String, OrderBook), seller: (String, OrderBook), cp: CurrencyPair, t: Int): Boolean = {
      /* buy x sell y? */
      if (buyer._2.getAsks.isEmpty || seller._2.getBids.isEmpty)
        return false

      val topAsk = buyer._2.getAsks.get(0)
      val topBid = seller._2.getBids.get(0)

      val askPrice = topAsk.getLimitPrice
      val bidPrice = topBid.getLimitPrice

      val askVol = topAsk.getTradableAmount
      val bidVol = topBid.getTradableAmount

      val executionAmount = askVol.min(bidVol)

      val buyFee = FEE_SPEC(buyer._1)
      val sellFee = FEE_SPEC(seller._1)

      val cost = askPrice.multiply( executionAmount ).multiply( ONE.add(buyFee) )
      val revenue = bidPrice.multiply( executionAmount ).multiply( ONE.subtract(sellFee) )

      val pips = askPrice.subtract(bidPrice).multiply(PIP_SPEC(cp))
      val profitable = revenue.subtract(cost).compareTo(ZERO) > 0

      if (profitable){
        try {
          processArbitrage(buyer, seller, cp, t)
        } catch {
          case uom: UnprofitableOMException => { false }
          case e : Exception => {
            val combo = (buyer._1, seller._1, cp)
            logger.error(s"Error procesando arbitrage $combo. Comprador: ${buyer._1}, ${buyer._2.getAsks.take(5).mkString(",")}" +
              s". Vendedor: ${seller._1}, ${seller._2.getBids.take(5).mkString(",")}",e)

            false
          }
        }
      } else {
        val combo = (buyer._1, seller._1, cp)
        if (marketOpportunities.contains(combo)) {
          logger.trace(s"Removing combo: ${combo}")
          marketOpportunities.remove(combo)
        }
        false
      }
    }

    def seekArbitrage(e1: String, e2: String, cp: CurrencyPair, t: Int): Boolean = {
      val validity = !e1.equals(e2)

      val data = marketBooks.contains((e1,cp)) && marketBooks.contains((e2,cp))

      if (validity && data){
        val buyer = (e1, marketBooks((e1,cp)))
        val seller = (e2, marketBooks((e2,cp)))

        try {
          updateArbitrage(buyer, seller, cp, t)
        } catch {
          case e: Exception => {
            logger.error(s"Buy ${buyer._1} - Sell ${seller._1} $cp. Ocurrio un error en updateArbitrage ... Buyer Top Asks: ${buyer._2.getAsks.take(5).mkString(",")}" +
              s" ... Seller Top Bids: ${seller._2.getBids.take(5).mkString(",")}", e)
            false
          }
        }
      }
      else {
        if (!data)
          logger.trace(s"${e1} not seeking ${e2} on ${cp}. Reason: There is no data.")
        false
      }
    }

    /* Seeker replies with the number of opportunities */
    def receive = {
      case BOOK_UPDATE(n, cp, t, lat) => {

        logger.trace(s"Tick ${t}. ${n} ${cp}. Latencia: ${lat.toMillis} ms. Buscando oportunidades en seeker." )

        val before = new Date().getTime
        val coExchanges: ParIterable[ExchangeName] = currencyListings(cp).keys.par

        val response: Either[Int, Int] = try {
          var _ops = 0
          coExchanges.foreach(e => {
            if (seekArbitrage(n, e, cp, t)) _ops += 1
            if (seekArbitrage(e, n, cp, t)) _ops += 1
          })

          Right(_ops)
        } catch {
          case e : Exception => {
            logger.error(s"Tick $t. ${n} ${cp}. Seeker no pudo buscar", e)
            Left(0)
          }
        }

        val now = new Date().getTime

        if (response.isRight && response.right.get>0)
          logger.debug(s"Tick $t. ${n} ${cp}. Seeker se comparo con ${coExchanges.mkString(",")} y " +
            s"encontro ${response} ... en ${now-before} ms")
        else
          logger.trace(s"Tick $t. ${n} ${cp}. Seeker no encontro oportunidades con ${coExchanges.mkString(",")} " +
            s" ... en ${now-before} ms")
      }

      case inevitable => logger.error(s".... THE INEVITABLE HAPPENED. RECEIVED: ${inevitable.getClass}")
    }
  }

  class BitfinexSeeker    extends SeekerActor
  class BitstampSeeker    extends SeekerActor
  class BleutradeSeeker   extends SeekerActor
  class BittrexSeeker     extends SeekerActor
  class BTCESeeker        extends SeekerActor
  class CEXIOSeeker       extends SeekerActor
  class GatecoinSeeker    extends SeekerActor
  class ItBitSeeker       extends SeekerActor
  class KrakenSeeker      extends SeekerActor
  class PoloniexSeeker    extends SeekerActor

}

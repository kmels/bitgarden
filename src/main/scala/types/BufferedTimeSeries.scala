package types

import java.util.Date

import org.knowm.xchange.dto.marketdata.Trade
import org.apache.logging.log4j.LogManager

import scala.concurrent.duration.Duration
import scala.concurrent.duration._
import bgutils.UtilImplicits._
import scala.collection.SeqLike

case class BufferedTimeSeries[T <: { def getTimestamp(): Date }](val exchangeName: String,
                                                                 val elems: Seq[T],
                                                                 val maxDistance: Duration)
  extends scala.collection.mutable.Queue[T]() {

  val before = new Date().getTime

  val logger = LogManager.getLogger(getClass)

  def distance(x: Date, y: Date): Duration = (y.getTime - x.getTime, MILLISECONDS)

  override def enqueue(ts: T*): Unit = {

    for (t <- ts.toList) {

      while (size > 0 && distance(head.getTimestamp(), t.getTimestamp()) > maxDistance) {

        dequeue()
      }

      super.enqueue(t)
    }
  }
  

  if (elems.size == 1)
    enqueue(head)

  if (elems.size > 1){
    val isOrdered = elems.head.getTimestamp().before(elems.last.getTimestamp())

    if (isOrdered)
      elems.foreach(this.enqueue(_))
    else {
      elems.sortBy(_.getTimestamp).foreach(this.enqueue(_))    
    }
  }

  val now = new Date().getTime

  if (this.size > 1 ) {
    logger.trace(s"""$exchangeName: Se termino de encolar en ${now-before}ms, la duracion es de ${distance(head.getTimestamp(), last.getTimestamp()).toMinutes} minutos. La cabeza es: ${sdf.format(head.getTimestamp())}. El ultimo es ${sdf.format(last.getTimestamp())} """)

    assert (distance(head.getTimestamp(), last.getTimestamp()) >= Duration(0, MILLISECONDS), s"La distancia entre el primero y el ultimo precio debe existir: ${head.getTimestamp} - ${last.getTimestamp} ... " + this.map(_.getTimestamp).mkString("\t") )
    assert (distance(head.getTimestamp(), last.getTimestamp()) <= maxDistance)
  }

}


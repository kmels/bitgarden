import java.lang.reflect.UndeclaredThrowableException

import akka.actor.Actor
import akka.actor.Actor.Receive
import bgutils.UtilImplicits._
import org.knowm.xchange.currency.{Currency, CurrencyPair}
import org.knowm.xchange.dto.account.{Wallet, Balance}
import org.knowm.xchange.service.polling.account.PollingAccountService
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService
import org.knowm.xchange.service.polling.trade.PollingTradeService
import bgutils.GlobalConstants._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

import java.math.BigDecimal._
import java.math.RoundingMode

package object agents {

  sealed trait AgentRequest

  case class UPDATE_BALANCES(n: String, tick: Int) extends AgentRequest
  case class CALCULATE_PROFIT(n: String, id: String, isBuy: Boolean, tick: Int) extends AgentRequest

  val navCurrency = "USD"

  def currencyPriceInDollar(currency: String): Option[Price] = {
    val cp = new CurrencyPair(currency, navCurrency)

    marketTrades.find(m => m._1._2.equals(cp) && m._2.nonEmpty)
      .flatMap(_._2.lastOption.map(_.getPrice))
  }

  def nav(currency: Currency, currencyTotal: Volume): Price = {
    if (currency.toString.toUpperCase.equals(navCurrency))
      currencyTotal
    else {
      val currencySymbol = currency.toString
      val priceInDollars = currencyPriceInDollar(currencySymbol)
      if (priceInDollars.isDefined) {
        val price = priceInDollars.get
        val currencyNav = currencyTotal.multiply(price)
        val nDecimals = PIP_SPEC(new CurrencyPair(currencySymbol, navCurrency)).toString.count(_ == '0')
        currencyNav.setScale(2, java.math.RoundingMode.HALF_EVEN)
      } else {
        ZERO
      }
    }
  }

  def totalCurrencyBalance(currency: Currency): Precision = accountBalances.filter(_._1._2.equals(currency)).map(_._2.getTotal).sum

  class TradeAgent(account: PollingAccountService) extends Actor {
    override def receive: Receive = {
      case UPDATE_BALANCES(n, t) => {

        val response : Either[Throwable, scala.collection.Map[Currency, Balance]] = try {

          assert(account != null, s"${n} - el agente debe tener una cuenta")
          val info = account.getAccountInfo

          assert(info != null, s"${n} - el agente debe tener info")
          val wallet: Wallet = try {
            assert(info.getWallet != null, s"${n} - el agente debe tener una billetera")
            info.getWallet
          } catch {
            case _: Throwable => {
              assert(info.getWallets.head != null, s"${n} - el agente debe tener una billetera")
              info.getWallets.head._2
            }
            case ute: UndeclaredThrowableException => {
              assert(info.getWallets.head != null, s"${n} - el agente debe tener una billetera")
              info.getWallets.head._2
            }
          }

          val balances: scala.collection.Map[Currency, Balance] = wallet.getBalances

          balances.filter(b => currencies.contains(b._1)).foreach(b => {
            val currency = if (b._1.equals(Currency.XBT)) Currency.BTC else b._1
            val key = (n, currency)
            val value = b._2
            accountBalances.put(key, value)

            val currencyTotal = totalCurrencyBalance(currency)
            //logger.info(s"Inserting currencyNav $currency = ${nav(currency, currencyTotal)}... of total ${currencyTotal} $currency")
            currencyNavs.put(currency, nav(currency, currencyTotal))
          })

          Right(balances)
        } catch {
          case e : Throwable => {
            logger.warn(s"Tick $t. ${n}. Agent no pudo actualizar balance - ${e.getClass.toString}", e)
            Left(e)
          }
        }
      } // end UPDATE_BALANCES


    }
  }

  class BitfinexAgent  (account: PollingAccountService) extends TradeAgent (account)
  class BitstampAgent  (account: PollingAccountService) extends TradeAgent (account)
  class BleutradeAgent (account: PollingAccountService) extends TradeAgent (account)
  class BittrexAgent   (account: PollingAccountService) extends TradeAgent (account)
  class BTCEAgent      (account: PollingAccountService) extends TradeAgent (account)
  class CEXIOAgent      (account: PollingAccountService) extends TradeAgent (account)
  class GatecoinAgent  (account: PollingAccountService) extends TradeAgent (account)
  class ItBitAgent     (account: PollingAccountService) extends TradeAgent (account)
  class KrakenAgent    (account: PollingAccountService) extends TradeAgent (account)
  class PoloniexAgent  (account: PollingAccountService) extends TradeAgent (account)
}

package bgutils

import java.math.BigDecimal._
import java.math.RoundingMode

import bgutils.GlobalConstants.{CurrencyArbitrage, _}
import bgutils.UtilImplicits.{Price, Volume, _}
import org.knowm.xchange.bitfinex.v1.dto.trade.BitfinexTradeResponse
import org.knowm.xchange.bitfinex.v1.service.polling.BitfinexTradeServiceRaw
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.gatecoin.dto.trade.GatecoinTradeHistory
import org.knowm.xchange.gatecoin.service.polling.GatecoinTradeServiceRaw
import org.knowm.xchange.kraken.dto.trade.KrakenTrade
import org.knowm.xchange.kraken.service.polling.KrakenTradeServiceRaw
import org.bitcoinj.core.Sha256Hash

import scala.collection.JavaConversions._

package object feedback {

  case class ARBITRAGE_FEEDBACK_REQUEST(key: Sha256Hash, currencyArbitrage: CurrencyArbitrage)

  type Slippage = Price
  type ExecutedPrice = Price;
  type ExecutedVolume = Volume
  type Cost = Volume;
  type Revenue = Volume

  class ORDER_FEEDBACK(val cost: Price, val slippage: Volume, val weightedPrice: Price)

  case class BUY_ORDER_FEEDBACK(c: Price, s: Volume, wp: Price) extends ORDER_FEEDBACK(c, s, wp)

  case class SELL_ORDER_FEEDBACK(r: Price, s: Volume, wp: Price) extends ORDER_FEEDBACK(r, s, wp)

  class FEEDBACK_REQUEST(val target: Price, val orderId: String, val cp: CurrencyPair, val isBUY: Boolean)

  case class BUY_FEEDBACK_REQUEST(t: Price, o: String, p: CurrencyPair) extends FEEDBACK_REQUEST(t, o, p, true)

  case class SELL_FEEDBACK_REQUEST(t: Price, o: String, p: CurrencyPair) extends FEEDBACK_REQUEST(t, o, p, false)

  def orderFeedback(n: String, req: FEEDBACK_REQUEST): Option[ORDER_FEEDBACK] = n match {
    case "Bitfinex" => BitfinexOrders.getFeedback(req)
    case "Kraken" => KrakenOrders.getFeedback(req)
    case "Gatecoin" => GatecoinOrders.getFeedback(req)
    case _ => None
  }

  trait OrderFeedbackSupport {
    type RAW
    type CACHE
    type ORDER

    def getFeedback(order: FEEDBACK_REQUEST): Option[ORDER_FEEDBACK]

    var lastHistory: CACHE

    def download(r: RAW, ps: Object*): Unit

    def look(r: CACHE, req: FEEDBACK_REQUEST): Option[ORDER]

    def respond(o: ORDER, req: FEEDBACK_REQUEST): ORDER_FEEDBACK
  }

  case object BitfinexOrders extends OrderFeedbackSupport {
    type RAW = BitfinexTradeServiceRaw
    type ORDER = Array[BitfinexTradeResponse]
    type CACHE = Array[BitfinexTradeResponse]
    var lastHistory = Array[BitfinexTradeResponse]()

    override def look(r: CACHE, req: FEEDBACK_REQUEST): Option[ORDER] = {
      println("Looking")
      val os = r.filter(o => o.getOrderId.equals(req.orderId))
      if (os.nonEmpty) Some(os) else None
    }

    override def respond(tx: ORDER, req: FEEDBACK_REQUEST): ORDER_FEEDBACK = {
      val totalVolume = tx.map(_.getAmount).sum
      val totalInFees = tx.map(_.getFeeAmount).sum
      val weightedPrice = tx.map(t => t.getAmount.multiply(t.getPrice)).sum.divide(totalVolume, DECIMAL_SPEC(req.cp.toString), ROUND_HALF_EVEN)

      val slippage = if (req.isBUY) weightedPrice.subtract(req.target) else req.target.subtract(weightedPrice)
      val cost = if (req.isBUY) weightedPrice.multiply(totalVolume).add(totalInFees) else weightedPrice.multiply(totalVolume).subtract(totalInFees)
      if (req.isBUY) BUY_ORDER_FEEDBACK(cost, slippage, weightedPrice) else SELL_ORDER_FEEDBACK(cost, slippage, weightedPrice)
    }

    override def download(r: RAW, ps: Object*): Unit = {
      println("Downloading")
      val cp: CurrencyPair = ps(0).asInstanceOf[CurrencyPair]
      val sym: String = cp.base.toString + cp.counter.toString
      lastHistory = r.getBitfinexTradeHistory(sym, 0l, 100)
    }

    override def getFeedback(req: FEEDBACK_REQUEST): Option[ORDER_FEEDBACK] = {
      val exchange = GlobalConstants.findExchangeByName("Bitfinex").get.getPollingTradeService.asInstanceOf[RAW]
      val cache = look(lastHistory, req)
      if (cache.isEmpty) println("Cache miss")
      val order = if (cache.isDefined) cache
      else {
        download(exchange, req.cp); look(lastHistory, req)
      }
      order.map(o => respond(o, req))
    }
  }

  case object KrakenOrders extends OrderFeedbackSupport {
    type RAW = KrakenTradeServiceRaw
    type ORDER = Iterable[KrakenTrade]
    type CACHE = scala.collection.mutable.Map[String, KrakenTrade];
    var lastHistory = scala.collection.mutable.Map[String, KrakenTrade]()
    var isExhaust = false;

    def look(r: CACHE, req: FEEDBACK_REQUEST): Option[ORDER] = {
      println("Looking")
      val os = r.filter(t => t._2.getOrderTxId.equals(req.orderId)).map(_._2)
      if (os.isEmpty) None else Some(os)
    }

    def respond(tx: ORDER, req: FEEDBACK_REQUEST): ORDER_FEEDBACK = {
      val totalInFees = tx.map(_.getFee).sum
      val totalVolume = tx.map(_.getVolume).sum
      val weightedPrice = tx.map(t => t.getVolume.multiply(t.getPrice)).sum.divide(totalVolume, DECIMAL_SPEC(req.cp.toString), ROUND_HALF_EVEN)

      val slippage = if (req.isBUY) weightedPrice.subtract(req.target) else req.target.subtract(weightedPrice)
      val cost = if (req.isBUY) weightedPrice.multiply(totalVolume).add(totalInFees) else weightedPrice.multiply(totalVolume).subtract(totalInFees)
      if (req.isBUY) BUY_ORDER_FEEDBACK(cost, slippage, weightedPrice) else SELL_ORDER_FEEDBACK(cost, slippage, weightedPrice)
    }

    def download(r: RAW, ps: Object*): Unit = {
      println("Downloading");
      val offset = if (!isExhaust) lastHistory.size.toLong else 0l
      val temp = r.getKrakenTradeHistory(null, false, 0l, new java.util.Date().getTime, offset).toMap
      val curSize = lastHistory.size
      lastHistory = lastHistory ++ temp
      if (lastHistory.size == curSize)
        isExhaust = true
    }

    override def getFeedback(req: FEEDBACK_REQUEST): Option[ORDER_FEEDBACK] = {
      //get historical transactions
      val exchange = GlobalConstants.findExchangeByName("Kraken").get.getPollingTradeService.asInstanceOf[RAW]
      val cached = look(lastHistory, req)
      if (cached.isEmpty) println("Cache miss")
      val orders = if (cached.isDefined) cached
      else {
        download(exchange, req.cp.asInstanceOf[Object]); look(lastHistory, req)
      }
      orders.map(o => respond(o, req))
    }
  }

  case object GatecoinOrders extends OrderFeedbackSupport {
    type RAW = GatecoinTradeServiceRaw
    type ORDER = List[GatecoinTradeHistory]
    type CACHE = Array[GatecoinTradeHistory];
    var lastHistory = Array[GatecoinTradeHistory]()

    def look(r: CACHE, req: FEEDBACK_REQUEST): Option[ORDER] = {
      println("Looking")
      val matches: CACHE = if (req.isBUY) r.filter(th => th.getBidOrderID.equals(req.orderId))
      else r.filter(th => th.getAskOrderID.equals(req.orderId))
      if (matches.isEmpty) None else Some(matches.toList)
    }

    def respond(o: ORDER, req: FEEDBACK_REQUEST): ORDER_FEEDBACK = {
      val nDecimals: Int = DECIMAL_SPEC(req.cp.toString)
      if (req.isBUY) {
        val buyFee = ONE.add(lastHistory.headOption.map(_.getFeeRate).getOrElse(FEE_SPEC("Gatecoin")))
        def cost(e: GatecoinTradeHistory) = e.getPrice.multiply(e.getQuantity).multiply(buyFee).setScale(nDecimals, ROUND_HALF_EVEN)

        val totalVolume = o.map(e => e.getQuantity).sum.setScale(nDecimals, ROUND_HALF_EVEN)
        val totalInFees = o.map(e => e.getQuantity.multiply(e.getFeeRate).multiply(e.getPrice)).sum.setScale(nDecimals, ROUND_HALF_EVEN)
        val totalCost = o.map(cost).sum.setScale(nDecimals, ROUND_HALF_EVEN)
        val weightedPrice = (totalCost.subtract(totalInFees)).divide(totalVolume, nDecimals, ROUND_HALF_EVEN).setScale(nDecimals, ROUND_HALF_EVEN)
        val slippage = weightedPrice.multiply(buyFee).subtract(req.target).setScale(nDecimals, ROUND_HALF_EVEN)
        BUY_ORDER_FEEDBACK(totalCost, slippage, weightedPrice)
      } else {
        val sellFee = ONE.subtract(o.headOption.map(_.getFeeRate).getOrElse(FEE_SPEC("Gatecoin")))
        def revenue(e: GatecoinTradeHistory) = e.getPrice.multiply(e.getQuantity).multiply(sellFee).setScale(nDecimals, RoundingMode.HALF_UP)

        val totalVolume = o.map(e => e.getQuantity).sum.setScale(nDecimals, ROUND_HALF_EVEN)
        val totalInFees = o.map(e => e.getQuantity.multiply(e.getFeeRate).multiply(e.getPrice)).sum.setScale(nDecimals, ROUND_HALF_EVEN)
        val totalRevenue = o.map(revenue).sum.setScale(nDecimals, ROUND_HALF_EVEN) //with fees
        val weightedPrice = (totalRevenue.add(totalInFees)).divide(totalVolume, nDecimals, ROUND_HALF_EVEN).setScale(nDecimals, ROUND_HALF_EVEN) // no fees
        val slippage = req.target.subtract(weightedPrice).setScale(nDecimals, ROUND_HALF_EVEN)
        SELL_ORDER_FEEDBACK(totalRevenue, slippage, weightedPrice)
      }
    }

    def download(r: RAW, ps: Object*): Unit = {
      println("Downloading");
      lastHistory = r.getGatecoinUserTrades(1000).getTransactions
    }

    override def getFeedback(req: FEEDBACK_REQUEST): Option[ORDER_FEEDBACK] = {
      val exchange = GlobalConstants.findExchangeByName("Gatecoin").get.getPollingTradeService.asInstanceOf[RAW]
      val cache = look(lastHistory, req)
      if (cache.isEmpty) println("Cache miss")
      val order = if (cache.isDefined) cache
      else {
        download(exchange, req.cp.asInstanceOf[Object]); look(lastHistory, req)
      }
      order.map(o => respond(o, req))
    } // end getOrderFeedback

  }

}

package bgutils

import java.awt.Color
import java.util.Date

import agents._
import akka.actor.{ActorRef, ActorSystem, Props}
import org.knowm.xchange.dto.account.Balance
import org.knowm.xchange.service.polling.account.PollingAccountService
import org.knowm.xchange.service.polling.trade.PollingTradeService
import org.knowm.xchange.{Exchange, ExchangeFactory, ExchangeSpecification}
import org.knowm.xchange.bitfinex.v1.BitfinexExchange
import org.knowm.xchange.bitstamp.BitstampExchange
import org.knowm.xchange.bittrex.v1.BittrexExchange
import org.knowm.xchange.bleutrade.BleutradeExchange
import org.knowm.xchange.btce.v3.BTCEExchange
import org.knowm.xchange.cexio.CexIOExchange
import org.knowm.xchange.currency.{Currency, CurrencyPair}
import org.knowm.xchange.dto.marketdata.{OrderBook, Trade}
import org.knowm.xchange.gatecoin.GatecoinExchange
import org.knowm.xchange.itbit.v1.ItBitExchange
import org.knowm.xchange.kraken.KrakenExchange
import org.knowm.xchange.poloniex.PoloniexExchange
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService
import org.apache.logging.log4j.LogManager
import org.bitcoinj.core.Sha256Hash
import pollers._
import seekers._
import traders._
import types.BufferedTimeSeries
import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap

import scala.collection.mutable
import scala.collection.mutable.Set
import scala.collection.mutable.ListBuffer
import UtilImplicits._

import scala.collection.JavaConversions._
import scala.io.Source
import GlobalUtils.SoundPlayer
import java.math.BigDecimal
import java.math.BigDecimal._
import java.math.RoundingMode

import com.google.gson._

object GlobalConstants {

  val logger = LogManager.getLogger(getClass)

  val pollerSystem = ActorSystem("Pollers")
  val seekerSystem = ActorSystem("Seekers")
  val agentSystem = ActorSystem("Agents")
  val tradeSystem = ActorSystem("Traders")
  val feedbackSystem = ActorSystem("Analytics")
  val soundSystem = ActorSystem("Effects")

  //pending (scale)
  //GHS/USD (CEX.IO)
  //DOGE/BTC (Bittrex)

  final val ETH_USD = new CurrencyPair("ETH", "USD")

  final val ETH_EUR = new CurrencyPair("ETH", "EUR")

  //waiting (higher volume)
  //DASH/BTC (BTER)
  val currencyListings: Map[CurrencyPair, Map[ExchangeName, (CurrencyPair,Boolean)]] = Map( //TODO: Make ParMap
    CurrencyPair.BTC_USD -> Map(
      //"ANX" -> (CurrencyPair.BTC_USD, true)
      //"Bitfinex" ->(CurrencyPair.BTC_USD, true)
      "Bitstamp" ->(CurrencyPair.BTC_USD, true)
      , "Btc-e" -> (CurrencyPair.BTC_USD,true)
      , "Gatecoin" -> (CurrencyPair.BTC_USD,true)
      , "Kraken" -> (CurrencyPair.BTC_USD,true)
      //, "ItBit" -> (new CurrencyPair("XBT","USD"),true)
      //, "CEX.IO" -> (CurrencyPair.BTC_USD,true)
      //      , "Poloniex" -> (new CurrencyPair("XUSD","BTC"),false)
    ),

    CurrencyPair.EUR_USD -> Map(
      "Btc-e" -> (CurrencyPair.EUR_USD, true)
    ),

    //more on tether http://coinmarketcap.com/assets/tether/#markets
    //new CurrencyPair("BTC", "USDT") -> Map(
      //"Bitfinex" -> (CurrencyPair.
      //"Bittrex" -> (new CurrencyPair("BTC","USDT"), true)
      //, "Poloniex" -> (new CurrencyPair("BTC","USDT"), true)
    //),

    CurrencyPair.BTC_EUR -> Map(
      //"ANX" -> (CurrencyPair.BTC_EUR, true)
      "Btc-e" -> (CurrencyPair.BTC_EUR,true)
      //, "CEX.IO" -> (CurrencyPair.BTC_EUR, true)
      , "Bitstamp" -> (CurrencyPair.BTC_EUR, true)
      , "Gatecoin" -> (CurrencyPair.BTC_EUR, true)
      , "Kraken" -> (CurrencyPair.BTC_EUR,true)
      //, "ItBit" -> (new CurrencyPair("XBT","EUR"),true)
    ),

    CurrencyPair.ETH_BTC -> Map(
      "Bittrex" -> (CurrencyPair.ETH_BTC, true)
      , "Btc-e" ->(CurrencyPair.ETH_BTC, true)
      , "Gatecoin" ->(CurrencyPair.ETH_BTC, true)
      , "Kraken" ->(CurrencyPair.ETH_BTC, true)
      //, "Poloniex" ->(CurrencyPair.ETH_BTC, true)
    ),

    ETH_USD -> Map(
      "Bitfinex" ->(ETH_USD, true)
      , "Btc-e" ->(ETH_USD, true)
      , "Kraken" ->(ETH_USD, true)
      //, "Poloniex" ->(new CurrencyPair("ETH", "USDT"), true)
    ),

    ETH_EUR -> Map(
      "Gatecoin" ->(ETH_EUR, true)
      , "Kraken" ->(ETH_EUR, true)
    )

    , CurrencyPair.LTC_BTC -> Map(
      //"ANX" -> (CurrencyPair.LTC_BTC, true)
      "Bitfinex" ->(CurrencyPair.LTC_BTC, true)
      //"Bittrex" -> (CurrencyPair.LTC_BTC, true)
      , "Btc-e" ->(new CurrencyPair("LTC", "BTC"), true)
      //, "CEX.IO" -> (CurrencyPair.LTC_BTC, false)
      //, "Bleutrade" -> (new CurrencyPair("LTC","BTC"),true)
      , "Kraken" ->(new CurrencyPair("XBT", "LTC"), false)
      //, "Poloniex" -> (new CurrencyPair("LTC","BTC"),true)
    )

    //other counter BTC: CAD, CNY, HKD, JPY

    /* Base LTC */
    , CurrencyPair.LTC_USD -> Map(
      "Bitfinex" ->(CurrencyPair.LTC_USD, true)
      , "Btc-e" ->(CurrencyPair.LTC_USD, true)
      //, "CEX.IO" -> (CurrencyPair.LTC_USD, true)
      , "Kraken" -> (CurrencyPair.LTC_USD, true)
      //, "Poloniex" -> (new CurrencyPair("LTC","USDT"), true)
    )

    /*, CurrencyPair.LTC_EUR -> Map(
      "Btc-e" -> (CurrencyPair.LTC_EUR, true)
      //, "CEX.IO" -> (CurrencyPair.LTC_EUR, true)
      , "Kraken" -> (CurrencyPair.LTC_EUR, true)
    )*/

    /* Base DASH */
    /*new CurrencyPair("DASH","BTC") -> Map(
      "Bittrex" -> (new CurrencyPair("DASH","BTC"), true)
      , "Poloniex" -> (new CurrencyPair("DASH","BTC"), true)
    ),*/

    /*new CurrencyPair("DASH","USDT") -> Map(
      "Poloniex" -> (new CurrencyPair("DASH","USDT"), true)
    ),
    CurrencyPair.DASH_LTC -> Map(
      //"Bittrex"
      "Bleutrade" -> (CurrencyPair.DASH_LTC, true)
    ),*/

    /* Base ETH */
    /*CurrencyPair.ETH_LTC -> Map(
      "Bleutrade" -> (CurrencyPair.ETH_LTC, true)
    ),
    */



  ).filter({
    case (pair,_) => {
      val installed = List( CurrencyPair.ETH_BTC) // CurrencyPair.BTC_USD, CurrencyPair.BTC_USD, CurrencyPair.BTC_EUR, CurrencyPair.LTC_EUR, CurrencyPair.LTC_BTC )
      val isInstalled = true//installed.contains(pair)

      if (!isInstalled) {
        logger.info(s"$pair not installed")
        false
      } else {
        val coins = List(Currency.ETH, Currency.BTC, Currency.USD, Currency.EUR, Currency.LTC)

        val filters = coins.contains(pair.base) && coins.contains(pair.counter)

        filters
      }
    }
  })

  //val allPairs = GlobalConstants.currencyListings.keys
  val listings = GlobalConstants.currencyListings.values.flatMap(_.values)
  val subPairs : Set[CurrencyPair] = Set() ++ listings.map(_._1)
  val currencies : Set[Currency] = Set(subPairs.map(pair => pair.base) ++ subPairs.map(pair => pair.counter)).flatten

  logger.info(s"Enabled ${currencyListings.keys.mkString(",")}")

  type BrokerSpec = (String, Exchange, ActorRef, ActorRef, ActorRef, ActorRef, Option[StreamingConfig])

  val brokers: ListBuffer[BrokerSpec] = ListBuffer(
    //("ANX", new ANXExchange(), None) //error on trades (disabled/v2 deprecated)
    makeBroker("Bitfinex", classOf[BitfinexExchange], classOf[BitfinexPoller], classOf[BitfinexSeeker], classOf[BitfinexAgent], classOf[BitfinexTrader])
    , makeBroker("Bitstamp", classOf[BitstampExchange], classOf[BitstampPoller], classOf[BitstampSeeker], classOf[BitstampAgent], classOf[BitstampTrader])
    //, makeBroker("Bleutrade", new BleutradeExchange,  classOf[BleutradePoller],   classOf[BleutradeSeeker])
    , makeBroker("Bittrex",   classOf[BittrexExchange],    classOf[BittrexPoller],     classOf[BittrexSeeker],   classOf[BittrexAgent],   classOf[BittrexTrader])
    , makeBroker("Btc-e",     classOf[BTCEExchange],       classOf[BTCEPoller],        classOf[BTCESeeker],      classOf[BTCEAgent],      classOf[BTCETrader])
    //, makeBroker("CEX.IO",    classOf[CexIOExchange],       classOf[CEXIOPoller],        classOf[CEXIOSeeker],      classOf[CEXIOAgent],      classOf[CEXIOTrader])
    , makeBroker("Gatecoin",  classOf[GatecoinExchange],   classOf[GatecoinPoller],    classOf[GatecoinSeeker],  classOf[GatecoinAgent],  classOf[GatecoinTrader])
    , makeBroker("ItBit",     classOf[ItBitExchange],      classOf[ItBitPoller],       classOf[ItBitSeeker],     classOf[ItBitAgent],     classOf[ItBitTrader])
    , makeBroker("Kraken",    classOf[KrakenExchange],     classOf[KrakenPoller],      classOf[KrakenSeeker],    classOf[KrakenAgent],    classOf[KrakenTrader])
    , makeBroker("Poloniex",  classOf[PoloniexExchange],   classOf[PoloniexPoller],    classOf[PoloniexSeeker],  classOf[PoloniexAgent],  classOf[PoloniexTrader])
  ).filter(broker => {
    //val installed = List("GATECOIN","KRAKEN","BITSTAMP","POLONIEX","ITBIT")
    //installed.contains(broker._1.toUpperCase)
    true
  })

  val exchanges: Seq[ExchangeName] = brokers.map(_._1)

  def findExchangeByName(name: String): Option[Exchange] = brokers.find(bspec => bspec._1.equals(name)).map(_._2)

  def makeExchangeOnSpec[E](n: String, c: Class[E]): Exchange ={
    val exSpec: ExchangeSpecification = new ExchangeSpecification(c)
    val fileName = s"${n.toLowerCase()}.fold"

    if (new java.io.File(fileName).exists()){
      val json: String = Encryption.readDecryptedFile(fileName)
      val parser = new JsonParser()
      val api: JsonObject = parser.parse(json).getAsJsonObject

      if (api.has("secret")) exSpec.setSecretKey(api.get("secret").getAsString)
      if (api.has("apikey")) exSpec.setApiKey(api.get("apikey").getAsString)
      if (api.has("username")) exSpec.setUserName(api.get("username").getAsString)
      if (api.has("specifics")) api.get("specifics").getAsJsonObject.entrySet().foreach(e =>
        exSpec.setExchangeSpecificParametersItem(e.getKey, e.getValue.getAsString))
    } else
      logger.warn(s"No such file: $fileName")

    val instance = ExchangeFactory.INSTANCE.createExchange(exSpec)
    instance.remoteInit()
    instance
  }

  def makeBroker[E,P,S,A,T](n: String, e: Class[E], ap: Class[P], as: Class[S], aa: Class[A], at: Class[T]): BrokerSpec = {
    val exchange = makeExchangeOnSpec(n, e)

    (n,
      exchange,
      makePoller(ap, exchange.getPollingMarketDataService),
      makeSeeker(n, as),
      makeAgent(n, aa, exchange.getPollingAccountService),
      makeTrader(n, at, exchange.getPollingTradeService),
      None)

  }

  def makePoller[T](c: Class[T], p: PollingMarketDataService) = pollerSystem.actorOf(Props(c, p))

  def makeSeeker[S](n: String, c: Class[S]) = seekerSystem.actorOf(Props(c))

  def makeAgent[T](n: String, aa: Class[T], ac: PollingAccountService) = agentSystem.actorOf(Props(aa, ac))

  def makeTrader[T](n: String, at: Class[T], ts: PollingTradeService ) = tradeSystem.actorOf(Props(at, n, ts))

  val marketReps : Map[String, Exchange] = brokers.map(b => (b._1, b._2)).toMap

  val marketPollers : Map[String, ActorRef] = brokers.map(b => (b._1, b._3)).toMap

  val marketSeekers : Map[String, ActorRef] = brokers.map(b => (b._1, b._4)).toMap

  val marketAgents : Map[String, ActorRef] = brokers.map(b => (b._1, b._5)).toMap

  val marketTraders : Map[String, ActorRef] = brokers.map(b => (b._1, b._6)).toMap

  //val _combos : mutable.Buffer[ArbitrageCombo] = ListBuffer()
  val _marketSize = brokers.size * currencyListings.size

  logger.info("Market size: " + _marketSize)

  val marketTrades : ConcurrentMarket[BufferedTimeSeries[Trade]] =
    new ConcurrentLinkedHashMap.Builder[MarketId, BufferedTimeSeries[Trade]]()
      .initialCapacity(_marketSize).maximumWeightedCapacity(1000).build();

  val marketTradeUpdates : ConcurrentMarket[Long] =
    new ConcurrentLinkedHashMap.Builder[MarketId, Long]()
      .initialCapacity(_marketSize).maximumWeightedCapacity(1000).build()

  val marketBooks: ConcurrentMarket[OrderBook] =
    new ConcurrentLinkedHashMap.Builder[MarketId, OrderBook]()
      .initialCapacity(_marketSize).maximumWeightedCapacity(1000).build();

  val marketBookUpdates : ConcurrentMarket[Long] =
    new ConcurrentLinkedHashMap.Builder[MarketId, Long]()
      .initialCapacity(_marketSize).maximumWeightedCapacity(1000).build()

  val pollPerformances: ConcurrentMarket[Long] = new ConcurrentLinkedHashMap.Builder[MarketId, Long]()
    .initialCapacity(_marketSize).maximumWeightedCapacity(1000).build()

  sealed trait OpportunityState { def name: String ; def color: Color}
  //http://prettycolors.tumblr.com/
  case object OPS_INSUFFICIENT_SELLER_FUNDS extends OpportunityState { val name = "Insufficient Funds (SELL)" ; val color = new Color(255, 100, 25)} //orange
  case object OPS_INSUFFICIENT_BUYER_FUNDS extends OpportunityState { val name = "Insufficient Funds (BUY)" ; val color = new Color(255, 100, 25)} //orange
  case object OPS_TODO extends OpportunityState { val name = "TODO" ; val color = new Color(212, 161, 144)} //coffee
  case object OPS_MISSING_LIQUIDITY extends OpportunityState { val name = "Not enough liquidity" ; val color = new Color(51, 204, 255)} //blue #2e49d1?
  case object OPS_BLOCKED extends OpportunityState { val name = "Blocked"; val color = new Color(219,0,0)} //red
  case object OPS_ONGOING extends OpportunityState { val name = "Ongoing"; val color = new Color(25, 219, 100) } //green
  case object OPS_DETECTED extends OpportunityState { val name = "Detected"; val color = new Color(255, 255, 153) } //white-yellow

  val marketOpportunities: scala.collection.concurrent.Map[ArbitrageCombo, (OpportunityMatrix, OpportunityState)] =
    new ConcurrentLinkedHashMap.Builder[ArbitrageCombo, (OpportunityMatrix, OpportunityState)]()
      .initialCapacity(_marketSize * 2).maximumWeightedCapacity(1000).build()

  case class FulfilledOrder(val id: String, amount: Volume, timestamp: Long)

  trait Arbitrage {
    val buys: ListBuffer[FulfilledOrder];
    val sells: ListBuffer[FulfilledOrder]
  }

  case class CurrencyArbitrage(val combo: ArbitrageCombo, amount: Volume, om: OpportunityMatrix,
                               buys: ListBuffer[FulfilledOrder],
                               sells: ListBuffer[FulfilledOrder]
                              ) extends Arbitrage {

    // update time
    def time = List(if (buys.nonEmpty) buys.maxBy(_.timestamp).timestamp else 0l,
      if (sells.nonEmpty) sells.maxBy(_.timestamp).timestamp else 0l).max

    def pair = combo._3

    def expectedProfit: BigDecimal = {

      val _buyFee = ONE.add(om.buyFee)
      val _sellFee = ONE.subtract(om.sellFee)

      val _askPrice = om.accDepth.under._1.head.multiply(_buyFee)
      val _bidPrice = om.accDepth.under._3.head.multiply(_sellFee)

      _bidPrice.subtract(_askPrice).multiply(amount)
    }
  }

  case class ArbitrageFeedback(currencyArbitrage: CurrencyArbitrage)

  val marketArbitrages: CMap[Sha256Hash, CurrencyArbitrage] =
    new ConcurrentLinkedHashMap.Builder[Sha256Hash, CurrencyArbitrage]()
      .initialCapacity(_marketSize * 10).maximumWeightedCapacity(1000).build()

  //val marketExecutions: Cmap[Sha256Hash, CurrencyArbitrage] =

  val accountBalances : scala.collection.concurrent.Map[(String,Currency),Balance]=
    new ConcurrentLinkedHashMap.Builder[(String,Currency), Balance]()
      .initialCapacity(_marketSize).maximumWeightedCapacity(1000).build()

  val currencyNavs : scala.collection.concurrent.Map[(Currency),java.math.BigDecimal]=
    new ConcurrentLinkedHashMap.Builder[Currency, java.math.BigDecimal]()
      .initialCapacity(currencyListings.size).maximumWeightedCapacity(1000).build()

  currencyListings.foreach(l => {
    val pair = l._1
    val pairExchanges = l._2.keys
    pairExchanges.foreach(e => APIStates.init(e,pair))
  })

  logger.info(s"Initialized ${APIStates.keys.size} api states.")

  def apiFail(e: String, cp: CurrencyPair, tick: Int): Unit = {
    val m = (e,cp)
    APIStates.suspend(m, tick)
    logger.debug(s"Tick ${tick}. ${m}. Failing exchange. API STATUS = ${APIStates(m)}.")
  }

  def currencyListing(exchange: ExchangeName)(pair: CurrencyPair): Option[(CurrencyPair, Boolean)] =
    currencyListings(pair).get(exchange)

  def shuffle[A](xs: Seq[A]): Seq[A] = {
    def _shuffle(ys: Seq[A], used: Seq[Int]): Seq[A] = {
      if (used.size == ys.size)
        Nil
      else {
        var r: Int = (Math.random() * ys.size).toInt
        while (used.contains(r))
          r = (Math.random() * ys.size).toInt

        _shuffle(ys, used.+:(r)).+:(ys(r))
      }
    }
    _shuffle(xs, Nil)
  }

  final val COLORS: scala.collection.immutable.Map[String, Color] = exchanges.sorted.zip(Seq(
    new Color(62,164,193), //pretty cyan
    new Color(237,166,199), //pretty pink
    new Color(173,230,162), //pretty light green
    new Color(168,73,182),  //pretty purple
    new Color(255,213,0),    //pretty yellow (alternative: f9c406, f5e342)
    new Color(30,29,62),     //navy blue
    new Color(31,137,42),     //pretty green
    new Color(200,55,77),      //pretty dark pink
    new Color(88,24,33)       //coffee
  )).toMap

  val FEE_SPEC : String => java.math.BigDecimal = e => e match {
    case "Btc-e" => new java.math.BigDecimal("0.002") // 0.20%
    case "CEX.IO" => new java.math.BigDecimal("0.002") // 0.20%
    case "Kraken" => new java.math.BigDecimal("0.001") // 0.10%
    case "Poloniex" => new java.math.BigDecimal("0.002") // 0.20%
    case "Bitfinex" => new java.math.BigDecimal("0.002") // 0.20%
    case "Bitstamp" => new java.math.BigDecimal("0.0025")
    case "Bittrex" => new java.math.BigDecimal("0.0025") // 0.25%
    case "Bleutrade" => new java.math.BigDecimal("0.0025") // 0.25%
    case "Gatecoin" => new java.math.BigDecimal("0.0035") // 0.35 % < 20BTC
    case "ItBit" => new java.math.BigDecimal("0.002")
    case other => throw new RuntimeException(s"$other doesn't have fees defined.")
  }

  val PIP_SPEC : CurrencyPair => java.math.BigDecimal = e => e match {
    case CurrencyPair.BTC_USD => new java.math.BigDecimal("10000")
    case CurrencyPair.LTC_USD => new java.math.BigDecimal("10000")
    case ETH_USD => new java.math.BigDecimal("10000")

    case CurrencyPair.ETH_BTC => new java.math.BigDecimal("1000000") //poloniex, gatecoin & bittrex = 6
    case CurrencyPair.LTC_BTC => new java.math.BigDecimal("100000") //btc-e has 5 decimal points as lowest

    case CurrencyPair.BTC_EUR => new java.math.BigDecimal("10000")
    case CurrencyPair.LTC_EUR => new java.math.BigDecimal("10000")
    case ETH_EUR => new java.math.BigDecimal("10000")

    case _ =>  new java.math.BigDecimal("1000000") //altcoin - 6 decimals
  }

  final val DECIMAL_SPEC: Map[String, Int] = currencyListings.keys.map(key => (key.toString, PIP_SPEC(key).toString.count(_ == '0'))).toMap

  val MIN_ARB_SPEC : CurrencyPair => java.math.BigDecimal = e => e match {
    case CurrencyPair.BTC_USD => java.math.BigDecimal.valueOf(0.02d) // Bitstamp requires 5 USD
    case CurrencyPair.BTC_EUR => java.math.BigDecimal.valueOf(0.02d) // Bitstamp requires 5 USD
    case CurrencyPair.ETH_BTC => java.math.BigDecimal.valueOf(0.1d) //Bitfinex requires 0.1
    case ETH_USD => java.math.BigDecimal.valueOf(0.1d) //Bitfinex requires 0.1

    case CurrencyPair.LTC_BTC => java.math.BigDecimal.valueOf(1d)
    case CurrencyPair.LTC_USD => java.math.BigDecimal.valueOf(1d)
    case CurrencyPair.LTC_EUR => java.math.BigDecimal.valueOf(1d)

    case _ => java.math.BigDecimal.valueOf(1d)
  }

  val SUPPORTS_MARKET_ORDERS : String => Boolean =  e => e match {
    case "ItBit" => false
    case "Poloniex" => false
    case _ => true
  }
}

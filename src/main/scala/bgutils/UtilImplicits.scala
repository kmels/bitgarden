package bgutils

import java.text.SimpleDateFormat
import java.util

import org.knowm.xchange.dto.trade.LimitOrder
import org.knowm.xchange.service.streaming.ExchangeStreamingConfiguration
import types.BufferedTimeSeries
import scala.collection.parallel.mutable.{ParSeq, ParMap}
import scala.collection.mutable.{Buffer, Map}
import org.knowm.xchange.currency.CurrencyPair

object UtilImplicits {
  type Precision = java.math.BigDecimal
  type Price = java.math.BigDecimal
  type Volume = java.math.BigDecimal
  type AccVolume = java.math.BigDecimal
  
  type ArbitrageSpec = Seq[(OrderSpec,OrderSpec)]

  type UnparallelBookPage = Buffer[LimitOrder]

  type CMap[A, B] = scala.collection.concurrent.Map[A, B]

  /* just the pointers */
  type BookData = (UnparallelBookPage, UnparallelBookPage)

  /* the pointers parallelized */
  type DepthDataUnder = (ParSeq[Price], ParSeq[Volume], ParSeq[Price], ParSeq[Volume])
  class DepthData(val under: DepthDataUnder) extends Tuple4(under._1, under._2, under._3, under._4)

  /* the parallelized accumulations */
  type AccDetphDataUnder = (ParSeq[Price], ParSeq[AccVolume], ParSeq[Price], ParSeq[AccVolume])

  class AccDepthData(val under: AccDetphDataUnder) extends Tuple4(under._1, under._2, under._3, under._4)

  class OrderSpec(val price: Price, val volume: Volume) extends Tuple2[Price,Volume](price, volume)

  type MatrixOf[A] = Array[Array[A]]

  type MarketId = (String, CurrencyPair)

  type Market[A] = Map[MarketId, A]
  type ParallelizableMarket[A] = ParMap[MarketId, A] // Paralelizable Market
  type ConcurrentMarket[A] = scala.collection.concurrent.Map[MarketId, A]

  type ArbitrageCombo = (String,String,CurrencyPair)

  type CListing = (CurrencyPair, Boolean)

  type ExchangeName = String
  type StreamingConfig = ExchangeStreamingConfiguration

  /* Values */
  //val ZERO = java.math.BigDecimal.ZERO
  //val ONE = java.math.BigDecimal.ONE
  val HUNDRED = java.math.BigDecimal.valueOf(100)


  val serialDateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
  val sdf = new SimpleDateFormat(serialDateFormat)

  implicit def numericPrecision: Numeric[Precision] = new Numeric[Precision]{
    override def compare(x: Precision, y: Precision): Int = x.compareTo(y)
    override def fromInt(x: Int): Precision = new java.math.BigDecimal(x)
    override def negate(x: Precision): Precision = x.negate()
    override def minus(x: Precision, y: Precision): Precision = x.subtract(y)
    override def plus(x: Precision, y: Precision) = x.add(y)
    override def times(x: Precision, y: Precision) = ???
    override def toDouble(x: Precision) = x.doubleValue
    override def toFloat(x: Precision) = x.floatValue
    override def toInt(x: Precision) = x.intValue
    override def toLong(x: Precision) = x.longValue
  }

  implicit class BGD(d: Double) {
    def BIG = java.math.BigDecimal.valueOf(d.doubleValue)
  }
}

package bgutils

import java.math.RoundingMode
import java.text.DecimalFormat

import bgutils.GlobalConstants._
import bgutils.UtilImplicits._
import org.knowm.xchange.currency.CurrencyPair
import org.apache.logging.log4j.LogManager

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Buffer

import java.math.BigDecimal._
import java.math.RoundingMode

class UnprofitableOMException(message: String) extends java.lang.Exception(message)

class OpportunityMatrix(val accDepth: AccDepthData, val buyFee: Price, val sellFee: Price, val cp: CurrencyPair) {

  val logger = LogManager.getLogger(getClass)
  val nDecimals = PIP_SPEC(cp).toString.count(_ == '0')
  var numberRows : Buffer[Buffer[BigDecimal]] = Buffer()

  private def executableOrders(depthData: AccDepthData, buyFee: Price, sellFee: Price): ArbitrageSpec = {
    val buyOrders = scala.collection.mutable.Buffer[OrderSpec]()
    val sellOrders = scala.collection.mutable.Buffer[OrderSpec]()
    var (askPrices, askVolumes, bidPrices, bidVolumes) = depthData.under
    var aski = 0
    var bidi = 0

    val _buyFee = ONE.add(buyFee)
    val _sellFee = ONE.subtract(sellFee)

    //def askPrice(aski: Int) = askPrices(aski)
    def askPrice  = askPrices(aski)
    def askVolume = askVolumes(aski)

    //def bidPrice(aski: Int) = bidPrices(bidi)
    def bidPrice = bidPrices(bidi) //.multiply(_buyFee).setScale(nDecimals, RoundingMode.HALF_UP)
    def bidVolume = bidVolumes(bidi) //.multiply(_sellFee).setScale(nDecimals, RoundingMode.HALF_UP)

    def askPriceCommissioned = askPrices(aski).multiply(_buyFee).setScale(nDecimals, RoundingMode.HALF_UP)
    def bidPriceCommissioned = bidPrices(bidi).multiply(_sellFee).setScale(nDecimals, RoundingMode.HALF_UP)

    var executableIsProfit = true

    while (executableIsProfit
         && (aski < askVolumes.size)
         && (bidi < bidVolumes.size)
         && ((askPriceCommissioned compareTo bidPriceCommissioned) < 0)){ //overlap

      /*println(s"Hay overlap: ${askPrice.multiply( _buyFee ).setScale(nDecimals, RoundingMode.HALF_UP)} -" +
        s" ${bidPrice.multiply( _sellFee ).setScale(nDecimals, RoundingMode.HALF_UP)} ... ($askPriceCommissioned) - " +
        s"($bidPriceCommissioned) ")*/
      val ev : Volume = askVolumes(aski).min(bidVolumes(bidi))

      //buy

      if ((bidPriceCommissioned.subtract(askPriceCommissioned).multiply(ev).setScale(nDecimals, RoundingMode.HALF_EVEN) compareTo ZERO) > 0) {
        buyOrders.+= (new OrderSpec(askPrice, ev))
        if ((askVolumes(aski) compareTo ev) <= 0) aski += 1
        else askVolumes = askVolumes.updated(aski, askVolumes(aski).subtract(ev))
        //sell
        sellOrders.+= (new OrderSpec(bidPrice, ev))
        if ((bidVolumes(bidi) compareTo ev) <= 0) bidi += 1
        else bidVolumes = bidVolumes.updated(bidi, bidVolume.subtract(ev))
      } else {
        executableIsProfit = false
      }

    }

    assert(buyOrders.size == sellOrders.size)
    //println(s"Retornando ${buyOrders.size} ejecuciones")
    buyOrders.zip(sellOrders)
  }

  val modelRows: ListBuffer[Array[Object]] = ListBuffer[Array[Object]]()

  /* matrix formatting */
  val columnFormats: Array[String] = Array(
    cp.counter, cp.base,
    cp.counter, cp.base,
    cp.counter, cp.counter, cp.counter
  ).map(_.getCurrencyCode)

  /* get execution orders from books  */
  val executables: ArbitrageSpec = executableOrders(accDepth, buyFee, sellFee)
  val buyOrders = executables.map(_._1)
  val sellOrders = executables.map(_._2)
  assert(buyOrders.size == sellOrders.size)
  val arbitrageSize = executables.size

  /* Construct execution column 1 by 1 */
  var modelColumns = ListBuffer[String]()
  var numberColumns = ListBuffer[BigDecimal]()

  var highestAsk: Price = null
  var lowestBid: Price = null

  var accumulatedPips = ZERO

  def addExecutionColumn(n: BigDecimal, f: BigDecimal => String, c: String = ""): Unit = {
    numberColumns += n

    if (n.equals(ZERO)) {
      modelColumns += "-"
    } else if (!c.isEmpty)
      modelColumns += f(n) + " " + c
    else
      modelColumns += f(n)
  }


  val format4 : BigDecimal => String = n => new DecimalFormat("#0.0000").format(n)
  val format0 : BigDecimal => String = n => new DecimalFormat("#").format(n)
  val formatn : Int => BigDecimal => String = n => m =>
    new DecimalFormat(("#0.".toList ++ List.fill(n)('0')).mkString).format(m)

  val pipformat = formatn(nDecimals )

  /* Add the executions */
  var accumulatedProfit: Volume = ZERO
  var accumulatedCost: Volume = ZERO
  var accumulatedRevenue: Volume = ZERO
  var thereAreExecutions = arbitrageSize > 0

  while (thereAreExecutions && modelRows.size < arbitrageSize) {
    val executionNumber = modelRows.size

    val askPrice = buyOrders( executionNumber )  ._1
    val askVolume = buyOrders( executionNumber ) ._2
    val bidPrice = sellOrders( executionNumber ) ._1
    val bidVolume = sellOrders( executionNumber) ._2

    assert (askVolume == bidVolume)
    val executionAmount = askVolume

    val pips = bidPrice.subtract(askPrice).multiply(PIP_SPEC(cp))

    addExecutionColumn ( pips, format0 ) //1. pips

    addExecutionColumn ( executionAmount, pipformat, cp.base.getCurrencyCode ) // 2. amount

    addExecutionColumn ( askPrice, pipformat, cp.counter.getCurrencyCode) //3. buy price

    addExecutionColumn ( bidPrice, pipformat, cp.counter.getCurrencyCode)  //4. sell price

    val cost = askPrice.multiply( executionAmount ).multiply( ONE.add(buyFee) )
      .setScale(nDecimals, RoundingMode.HALF_UP)
    addExecutionColumn ( cost, pipformat, cp.counter.getCurrencyCode )  //5. cost

    val revenue = bidPrice.multiply( executionAmount ).multiply( ONE.subtract(sellFee) )
      .setScale(nDecimals, RoundingMode.HALF_DOWN)
    addExecutionColumn ( revenue, pipformat, cp.counter.getCurrencyCode) //6. revenue

    val profit = revenue.subtract(cost)
    addExecutionColumn ( profit, pipformat, cp.counter.getCurrencyCode) //7. profit

    //println("Amount: " + executionAmount + " Cost: " + cost + s" (Ask = ${askPrice}) " +
    // s"Revenue: " + revenue + s" (Bid = ${bidPrice} Profit: " + revenue.subtract(cost) + " ? " + ((profit compareTo ZERO)>0) )

    /* Add the columns to the row */
    if ((profit compareTo ZERO) > 0) {
      modelRows += (modelColumns.toArray)
      numberRows += numberColumns

      accumulatedPips = accumulatedPips.add(pips)
      accumulatedProfit = accumulatedProfit.add(profit)
      accumulatedCost = accumulatedCost.add(cost)
      accumulatedRevenue = accumulatedRevenue.add(revenue)
    } else {
      //println("Stop executions. ")
      thereAreExecutions = false
    }
    modelColumns.remove(0, modelColumns.size) //clean columns
  }

  val format2 : BigDecimal => String = n => new DecimalFormat("#0.00").format(n)

  val poc = pipformat(accumulatedProfit) + "/" + pipformat(accumulatedCost) + " " + cp.counter.getCurrencyCode

  val pocPercentage =
    if (accumulatedCost.equals(ZERO))
      pipformat(ZERO) + " %"
    else pipformat(accumulatedProfit.divide(accumulatedCost, 4, RoundingMode.HALF_UP).multiply(
      java.math.BigDecimal.valueOf(100))) + " %"

  val executionsModel = modelRows.toArray.map(_.toArray.map(_.asInstanceOf[Object]))

  override def toString() = s"ACPIPS: ${accumulatedPips}. PROFIT: ${accumulatedProfit} ${cp.counter}. EXECUTABLES: ${executables.size} " +
    s"ROI: (${pocPercentage})"

  if (executionsModel.isEmpty) {

    /*
    // TODO: When volume in the top is low, the opportunity (executionsModel) is rendered Unprofitable, even if there
    .are legit executable pips

    if (executables.nonEmpty)
      throw new Exception(s"No puede ser que hayan ${executables.size} ejecutables " +
        s" en un modelo vacio ... ${toString()} ... " +
        s"Asks: ${accDepth._1.take(5).zip(accDepth._2.take(5)).mkString(",")} ... " +
        s"Bids: ${accDepth._3.take(5).zip(accDepth._4.take(5)).mkString(",")} ... " +
        s"Buy fee: ${buyFee}. Sell fee: ${sellFee} ($cp)"
      )
    */

    throw new UnprofitableOMException(s"KEIN OM - ${toString()} (${executables.mkString(",")})")
  }

}

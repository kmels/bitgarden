package bgutils

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap
import org.knowm.xchange.currency.{CurrencyPair, Currency}
import GlobalConstants._
import java.util.Date
import scala.collection.JavaConversions._


object APIStates {

  case class APIStatus(OK: Boolean, BUSY: Boolean, TIME: Long){
    override def toString() = s"OK = $OK; BUSY = $BUSY; TIME = ${new Date().getTime - TIME} ms ago"
  }

  case class APIOFF(time: Long) extends Exception

  val apiStatus : scala.collection.concurrent.Map[(String,CurrencyPair),APIStatus]=
    new ConcurrentLinkedHashMap.Builder[(String,CurrencyPair), APIStatus]()
      .initialCapacity(currencyListings.values.flatten.size).maximumWeightedCapacity(1000).build()

  def suspend(m: (String, CurrencyPair), t: Int) : Unit = {
    apiStatus.put(m, apiStatus(m).copy(OK=false).copy(TIME = new Date().getTime))
  }

  def init(m: (String, CurrencyPair)) = apiStatus.put(m, APIStatus(true, false, new Date().getTime))

  def apply(m: (String, CurrencyPair)) = apiStatus(m)

  def getHealth(m: (String, CurrencyPair)): Boolean = if (apiStatus(m).OK) true else throw new APIOFF(apiStatus(m).TIME)

  def acquire(m: (String, CurrencyPair), t: Int) : Unit = {
    apiStatus.put(m, apiStatus(m).copy(BUSY=true).copy(TIME = new Date().getTime))
  }

  def release(m: (String, CurrencyPair), t: Int) : Unit = {
    apiStatus.put(m, apiStatus(m).copy(BUSY=false).copy(TIME = new Date().getTime))
  }

  def keys = apiStatus.keys


}

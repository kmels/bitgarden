package bgutils

import java.net.InetAddress

import bgutils.GlobalConstants.CurrencyArbitrage
import com.google.gson.{GsonBuilder, JsonObject, JsonParser}
import org.knowm.xchange.dto.marketdata.Trade
import bgutils.feedback.ORDER_FEEDBACK
import org.bitcoinj.core.Sha256Hash
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.shield.ShieldPlugin
import org.elasticsearch.search.sort.SortOrder

//import com.typesafe.scalalogging.LazyLogging
import java.util.{Date,TimeZone}
import java.text.SimpleDateFormat

import bgutils.UtilImplicits._
import org.apache.logging.log4j.LogManager
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.index.query.QueryBuilders._

object SavedData {
  val logger = LogManager.getLogger()

  val settings = Settings.settingsBuilder()
    .put("http.enabled", false)
    .put("path.home", "/home/campesino/bin/elasticsearch-2.1.1")

  val client = TransportClient.builder.addPlugin(classOf[ShieldPlugin]).settings(Settings.builder().put("shield.user", "campesino:morral-tipico").build()).build.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("eratostenes"), 9300))

  val gsonInstance = new GsonBuilder().setDateFormat(serialDateFormat).create()

  def findLastPrice(exchangeName: String): Date = {
    val req = client.prepareSearch(exchangeName.toLowerCase())
                    .setQuery(matchAllQuery())
                    .setSize(1)
                    .addSort("timestamp", SortOrder.DESC)
                    .execute()
    val response = req.actionGet()
    val timestamp = response.getHits.getAt(0).getSource.get("timestamp")
    try {
      sdf.parse(timestamp.toString)
    } catch {
      case fe: NumberFormatException => {
        logger.error(s"$exchangeName: No se pudo parsear timestamp ${timestamp.toString}", fe)
        new Date()
      }
    }
  }

  def saveNewPrices(n: String, ps: Trade*): Unit = {
    val index = n.toLowerCase
    val doc = "precios"

    val lastTradeTime = findLastPrice(n)
    val thereAreNew = ps.last.getTimestamp.after(lastTradeTime)
    if (!thereAreNew)
      return

    val pss = ps.reverse.takeWhile(trade => trade.getTimestamp.after(lastTradeTime) )

    val before = new Date().getTime
    logger.debug(s"$n: Seran ingresados ${pss.size}s precios.")

    for (p <- pss){
      val src = gsonInstance.toJson(p)
      logger.trace(s"$n: Indexando en $index/precios: ${sdf.format(p.getTimestamp)}. Poniendose al dia desde: ${sdf.format(lastTradeTime)}")

      val result: IndexResponse = client.prepareIndex(index, doc).setSource(src).get
    }

    val after = new Date().getTime
    logger.trace(s"$n: Se termino de guardar en ${after - before}ms")
  }

  def saveArbitrage(arbHash: Sha256Hash, currencyArbitrage: CurrencyArbitrage,
                    buyFeedback: Option[ORDER_FEEDBACK], sellFeedback: Option[ORDER_FEEDBACK], profit: Option[Volume], 
                    om: OpportunityMatrix): String = {

    logger.info(s"Guardando arbitrage: $arbHash")
    
    val cp = currencyArbitrage.combo._3
    val index = "arb" 
    val doc = s"${cp.base.toString.toLowerCase()}-${cp.counter.toString.toLowerCase()}"
    val _id : String = arbHash.toString

    logger.info(s"Guardando arbitrage: $arbHash - index: $index - doc: $doc")
    val src = new JsonObject()
    val date = new Date
    val df  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    df.setTimeZone(TimeZone.getTimeZone("GMT"))
    
    src.addProperty("hash", _id)
    src.addProperty("timestamp", date.getTime)
    src.addProperty("date", df.format(date))

    src.addProperty("amount", currencyArbitrage.amount)
    src.addProperty("currencyPair", currencyArbitrage.combo._3.toString)

    try {      
      val omj = new JsonObject()      
      omj.add("buy_targets", gsonInstance.toJsonTree(om.buyOrders))
      omj.add("sell_targets", gsonInstance.toJsonTree(om.sellOrders))
      omj.addProperty("buy_fee", om.buyFee)
      omj.addProperty("sell_fee", om.sellFee)
            

      omj.addProperty("accumulated_cost", om.accumulatedCost)
      omj.addProperty("accumulated_revenue", om.accumulatedCost)
      omj.addProperty("accumulated_profit", om.accumulatedProfit)
      omj.addProperty("accumulated_pips", om.accumulatedPips)
      omj.addProperty("profit_over_cost", om.poc)
      omj.addProperty("profit_over_cost_percentage", om.pocPercentage)
      src.add("opportunity_matrix", omj)
    } catch {
      case e: Throwable => logger.error("Error al agregar opportunity matrix", e)
    }
        
    src.addProperty("buyer", currencyArbitrage.combo._1)
    src.addProperty("seller", currencyArbitrage.combo._2)
    
    src.addProperty("expectedProfit", currencyArbitrage.expectedProfit)

    if (buyFeedback.isDefined)
      src.add("buyFeedback", gsonInstance.toJsonTree(buyFeedback.get))
    if (sellFeedback.isDefined)
      src.add("sellFeedback", gsonInstance.toJsonTree(sellFeedback.get))

    if (profit.isDefined)
      src.addProperty("actualProfit", profit.get)

    src.add("buyOrders", gsonInstance.toJsonTree(currencyArbitrage.buys.toArray))
    src.add("sellOrders", gsonInstance.toJsonTree(currencyArbitrage.sells.toArray))    

    val result: IndexResponse = client.prepareIndex(index, doc, _id).setSource(src.toString).get
    
    logger.info(s"Arbitrage guardado: $arbHash")

    return src.toString
  }

}

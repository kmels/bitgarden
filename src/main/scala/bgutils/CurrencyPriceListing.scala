package bgutils

import java.util

import org.knowm.xchange.dto.Order.OrderType
import org.knowm.xchange.dto.marketdata.{OrderBook, Trade}
import org.knowm.xchange.dto.trade.LimitOrder

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

object CurrencyPriceListing {

  def invert(p: java.math.BigDecimal): java.math.BigDecimal =
    java.math.BigDecimal.ONE.divide(p, 8, java.math.BigDecimal.ROUND_UP)

  def invert(t: Trade): Trade = {
    //print("Was: " + t.getPrice)
    val _t = new Trade(t.getType,
      invert(t.getTradableAmount),
      t.getCurrencyPair,
      invert(t.getPrice),
      t.getTimestamp,
      t.getId
    )
    //println(" is: " + _t.getPrice)
    _t
  }

  def invert(o: LimitOrder): LimitOrder = {
    //print("Was: " + o.getLimitPrice)
    val _o = new LimitOrder(if (o.getType().equals(OrderType.ASK)) OrderType.BID else OrderType.ASK,
      o.getTradableAmount.multiply(o.getLimitPrice).setScale(8, java.math.BigDecimal.ROUND_HALF_UP),
      o.getCurrencyPair,
      o.getId(),
      o.getTimestamp,
      invert(o.getLimitPrice))
    //println(" is: " + _o.getLimitPrice)
    _o
  }

  def invert(ob: OrderBook): OrderBook = {
    println("Flipping " + ob.getAsks.head.getCurrencyPair)

    def pp(b : LimitOrder): (java.math.BigDecimal, java.math.BigDecimal, OrderType) = (b.getTradableAmount, b.getLimitPrice, b.getType)
    def ppp(os: java.util.List[LimitOrder]): String = os.map(pp).take(5).mkString("\n\t")

    //println("Bids: " + ppp(ob.getBids))
    //println("Asks: " + ppp(ob.getAsks))

    val asks = new util.ArrayList[LimitOrder]()
    val bids = new util.ArrayList[LimitOrder]()

    asks.addAll(ob.getBids.map(invert))
    bids.addAll(ob.getAsks.map(invert))

    val _ob = new OrderBook(ob.getTimeStamp(), asks, bids)

    //println("Bids: " + ppp(_ob.getBids))
    //println("Asks: " + ppp(_ob.getAsks))

    _ob
  }
}

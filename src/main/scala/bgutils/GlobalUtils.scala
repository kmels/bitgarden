package bgutils

import bgutils.UtilImplicits._
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata.OrderBook
import akka.actor.{Actor, Props}
import java.math.BigDecimal._
import java.math.RoundingMode

import org.apache.logging.log4j.LogManager

object GlobalUtils {
  val logger = LogManager.getLogger()

  def costOfBuying(amount: java.math.BigDecimal, book: OrderBook, buyFee: Price): Price = {
    val asks = book.getAsks
    var buyingAmount = amount
    var nextPos = 0
    var accCost = ZERO

    //println(s"buyAmount: $buyingAmount")
    while (buyingAmount.compareTo(ZERO) > 0) {

      val ask = asks.get(nextPos)
      val available = ask.getTradableAmount
      val price = ask.getLimitPrice.multiply(ONE.add(buyFee))

      val cost : java.math.BigDecimal = if (available.compareTo(buyingAmount) <= 0){
        buyingAmount = buyingAmount.subtract(ask.getTradableAmount)
        available.multiply(price)
      } else {
        //tradable is enough
        val _bought = buyingAmount.multiply(price)
        buyingAmount = ZERO
        _bought
      }

      nextPos = nextPos + 1
      accCost = accCost.add(cost)
    }

    //println(s"acc cost = $accCost")
    accCost
  }

  class SoundPlayer extends Actor {

    def playSound(filepath: String): Unit = try {
      import java.net.URL
      import javax.sound.sampled._
      /*val url = new URL(s"file://$filepath")
      val audioIn = AudioSystem.getAudioInputStream(url)
      val clip = AudioSystem.getClip
      clip.open(audioIn)
      clip.start*/
    } catch {
      case e: IllegalArgumentException => {
        logger.warn(s"Could not play sound filepath: $filepath", e)
      }
      case uh: java.net.UnknownHostException => {
        logger.warn(s"Could not play sound filepath: $filepath", uh)
      }
      case ex: Exception => logger.warn(s"Could not play sound file: $filepath", ex)
    }

    def receive = {
      case filepath: String => {
        playSound(filepath)
      }
    }
  }

  val soundPlayer = GlobalConstants.soundSystem.actorOf(Props(classOf[SoundPlayer]))
}

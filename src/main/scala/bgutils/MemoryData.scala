package bgutils

import java.math.RoundingMode
import java.util.Date

import agents.UPDATE_BALANCES
import akka.util.Timeout
import bgutils.GlobalConstants._
import org.knowm.xchange.currency.{Currency, CurrencyPair}
import org.knowm.xchange.dto.account.Balance
import org.knowm.xchange.dto.marketdata.{OrderBook, Trade}
import org.knowm.xchange.dto.trade.LimitOrder
import org.apache.logging.log4j.LogManager
import panels._
import pollers.{GET_DEPTH, GET_TRADES, OrderbookUpdate}
import seekers.BOOK_UPDATE
import types.BufferedTimeSeries

import scala.collection.mutable.{ListBuffer, Map}
import scala.collection.mutable.Buffer
import scala.collection.parallel.mutable.{ParMap, ParSeq}
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.concurrent.duration._
import akka.pattern.ask
import akka.util.Timeout

import scala.util.control.Breaks._
import java.math.BigDecimal._
import java.math.RoundingMode

import org.bitcoinj.core.Sha256Hash

object MemoryData {
  val logger = LogManager.getLogger();

  import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService
  import org.knowm.xchange.service.streaming.ExchangeEventType._
  import org.knowm.xchange.service.streaming.StreamingExchangeService
  import bgutils.UtilImplicits._

  val ticks : Map[String,Int] = Map()

  /* Add a trade */
  def addTrade(exchange: String, 
               pair: CurrencyPair, 
               trade: Trade,
               marketTrades: Market[BufferedTimeSeries[Trade]]): Unit = {

    marketTrades((exchange, pair)) enqueue trade
  }

  def poll(exchange: String,
           pair: CurrencyPair,
           poller: PollingMarketDataService,
           marketTrades: Market[BufferedTimeSeries[Trade]],
           currencyListing: String => CurrencyPair => Option[(CurrencyPair, Boolean)]
          ): Unit = {

    try {
      val maybeListing = currencyListing(exchange)(pair)

      if (maybeListing.isDefined) {
        val listing = maybeListing.get

        val _last = marketTrades((exchange,pair)).last.getTimestamp

        val ts = poller.getTrades(listing._1).getTrades.filter(t => t.getTimestamp.after(_last))

        val tss: Seq[Trade] = if (listing._2) { ts } else { ts.map(CurrencyPriceListing.invert) }

        val downloadedDuration = Duration(ts.last.getTimestamp.getTime - ts.head.getTimestamp.getTime, MILLISECONDS)
        logger.trace(s"$exchange: Precios descargados ${ts.size} con duracion de ${downloadedDuration.toMinutes} minutos. El ultimo en memoria es ${_last}. ${tss.size} son nuevos.")

        tss.foreach(trade => addTrade(exchange, pair, trade, marketTrades))
        //SavedData.saveNewPrices( exchange, tss : _*)

        assert (tss.head.getTimestamp.before(tss.last.getTimestamp), " El tiempo debe estar ordenado.")
      }

      logger.info(s"PRECIOS maybeListing ${exchange} == " + maybeListing)

    } catch {
      case e1: java.net.SocketTimeoutException => println("Prices timed out: " + exchange)
      case e2: org.knowm.xchange.exceptions.ExchangeException => println("Exchange Exception: " + e2)
    }
  }

  /* Update exchange prices */
  def updatePrices() = {

    /*def stream(s: StreamingExchangeService): Unit = {
      logger.info("Streaming " + s)
      while (s.countEventsAvailable() > 0) {
        val ev = s.getNextEvent
        ev.getEventType match {
          case TRADE => {
            
            val maybeListing = currencyListing(exchange)

            if (maybeListing.isDefined) {
              val listing = maybeListing.get

              var newTrade = if (listing._2){
                ev.getPayload.asInstanceOf[Trade]
              } else {
                CurrencyPriceListing.invert(ev.getPayload.asInstanceOf[Trade])
              }

              if (newTrade.getTimestamp == null){
                val now = new java.util.Date()
                println("WARN : TIMESTAMP is NULL, now IS: "+ now.getTime)
                newTrade = Trade.Builder.from(newTrade).timestamp(now).build
              }
              addTrade(exchange, newTrade, marketTrades)
            }

          }
          case other => println("GOT Another stuff " + other.toString)
        }
      }*/

  }

  def updateBook(exchange: String, pair: CurrencyPair, book: OrderBook, inverted: Boolean, marketBooks: Market[OrderBook]): Unit = {

    val (asks, bids) = if (!inverted){
      (book.getAsks, book.getBids)
    } else {
      (book.getBids, book.getAsks)
    }
    
    val oldBook = marketBooks((exchange,pair))

    for (newAsk <- asks.take(10)){
      oldBook.update(newAsk)
    }
    for (newBid <- bids.take(10)){
      oldBook.update(newBid)
    }
    
    marketBooks.put((exchange,pair), oldBook)
  }

  def streamBook(marketBooks: Market[OrderBook],
                 pair: CurrencyPair,
                 s: StreamingExchangeService,
                 exchange: String,                 
                 currencyListing: String => CurrencyPair => Option[(CurrencyPair, Boolean)],
                 prices: Market[BufferedTimeSeries[Trade]]): Unit = {

    while (s.countEventsAvailable() > 0) {
      val ev = s.getNextEvent

      ev.getEventType match {
        case DEPTH => {
          
          val maybeListing = currencyListing(exchange)(pair)

          if (maybeListing.isDefined){
            val listing = maybeListing.get

            val inverted = listing._2
            val ob = if (inverted){
              ev.getPayload.asInstanceOf[OrderBook]
            } else {
              CurrencyPriceListing.invert(ev.getPayload.asInstanceOf[OrderBook])
            }
            logger.error("FIX ME1.")
            //updateBook(exchange, ob, inverted)
          }
        }

        case SUBSCRIBE_ORDERS => {
          
          val maybeListing = currencyListing(exchange)(pair)

          if (maybeListing.isDefined){
            val listing = maybeListing.get

            val inverted = listing._2
            val ob = if (listing._2){
              ev.getPayload.asInstanceOf[OrderBook]
            } else {
              CurrencyPriceListing.invert(ev.getPayload.asInstanceOf[OrderBook])
            }
            logger.error("FIX ME2.")

            //updateBook(exchange, ob, inverted)
          }

        }
        case TRADE => addTrade(exchange, pair, ev.asInstanceOf[Trade], prices) //onTrade(exchange, ev.asInstanceOf[Trade])
        case other => println(s"GOT $other .. " + other.toString)
      }
    }
  }

  /* Update order book for exchange*/
  def updateDepth(exchange: String,
                  pair: CurrencyPair,
                  marketPollers: ParMap[String, Either[PollingMarketDataService, StreamingExchangeService]],
                  marketBooks: Market[OrderBook],
                  currencyListing: String => CurrencyPair => Option[(CurrencyPair, Boolean)],
                  marketTrades: Market[BufferedTimeSeries[Trade]]): Unit =  {

    def poll(marketBooks: Market[OrderBook], p: PollingMarketDataService): Unit = {

      try {

        val maybeListing = currencyListing(exchange)(pair)

        if (maybeListing.isDefined) {
          val listing = maybeListing.get
          val orderBook = p.getOrderBook(listing._1)

          //logger.trace(s"$exchange polling book. Top asks: ${orderBook.getAsks.take(3).map(a => (a.getTradableAmount, a.getLimitPrice)).mkString("-")}. Top bids: ${orderBook.getBids.take(3).map(a => (a.getTradableAmount, a.getLimitPrice)).mkString("-")} ");

          //val inverted = currencyListing(exchange)._2

          val ob = if (listing._2){
            orderBook
          } else {
            CurrencyPriceListing.invert(orderBook)
          }

          marketBooks.put((exchange, listing._1), ob)

        }
        
      } catch {
        case e: java.net.SocketTimeoutException => println("Timed out on " + exchange)
      }
    }

    val initially = new java.util.Date().getTime
    val e: String = exchange
    marketPollers(exchange).fold(poller => poll(marketBooks, poller), it => streamBook(marketBooks, pair, it, exchange, currencyListing, marketTrades))
    val tick = ticks.get(exchange).getOrElse(0) + 1
    ticks.put(exchange,tick)
    val ms = (new java.util.Date().getTime() - initially)
    logger.trace("Poll (depth) of "+exchange + " in " + ms +" ms (tick "+tick+")")
  }

  def bookData(x: (String, CurrencyPair), y: (String, CurrencyPair)): BookData = {

    val askPage = marketBooks.get(x).getOrElse(return (Buffer.empty, Buffer.empty)).getAsks
    val bidPage = marketBooks.get(y).getOrElse(return (Buffer.empty, Buffer.empty)).getBids

    (askPage, bidPage)
  }

  def makeDepth(bookData: BookData): AccDepthData = {
    val asks = bookData._1
    val bids = bookData._2

    val askPrices = asks.map(_.getLimitPrice)
    val bidPrices = bids.map(_.getLimitPrice)

    var askVolume = ZERO
    val askVolumes = asks.map(l => {
      askVolume = askVolume.add(l.getTradableAmount)
      askVolume
    })

    var bidVolume = ZERO
    val bidVolumes = bids.map(l => {
      bidVolume = bidVolume.add(l.getTradableAmount);
      bidVolume
    })

    new AccDepthData(askPrices.par, askVolumes.par, bidPrices.par, bidVolumes.par)
  }

  def makeDepth(x: (String, CurrencyPair), y: (String, CurrencyPair)): AccDepthData = makeDepth(bookData(x,y))

  /* Poll pair prices*/
  def updatePriceData(priceChart: PriceChart, cp: CurrencyPair, tick: Int): Unit = {
    val initially = new Date().getTime
    implicit val ctx = pollerSystem.dispatcher

    currencyListings.foreach( pairListings => {

      val pair = pairListings._1

      pairListings._2.foreach( listing => breakable {

        val exchangeName = listing._1

        val before = new Date().getTime

        logger.trace(s"Tick $tick. ${exchangeName} ${pair}. Orquestrando precios")

        val m = (exchangeName, pair)
        marketPollers(exchangeName) ! GET_TRADES(m, tick)
      })
    })

    val after = new Date().getTime
    logger.trace(s"Tick $tick. Precios orquestrados en ${(after-initially)}ms")
  }

  /* Poll all books */
  def updateDepthData(tick: Int): Unit = {

    //val visibleDepths = ListBuffer[Future[Any]]()
    val initially = new Date().getTime

    synchronized{ currencyListings.foreach( pairListing =>
      pairListing._2.foreach( l => {
        val n = l._1
        val cp = pairListing._1
        val m = (n,cp)

        //marketPollers(n) ! GET_DEPTH(m, tick)
        //logger.trace(s"Tick $tick. Orquestrando libros. Esperando descarga de libros ${n} ${cp}. ")

        if (!pollPerformances.contains(m)) {
          marketPollers(n) ! GET_DEPTH(m, tick)
          logger.trace(s"Tick $tick. Orquestrando libros. Esperando descarga de libros ${n} ${cp}. ")
        } else
          logger.trace(s"Tick $tick. Orquestrando libros. Postergando descarga de libros ${n} ${cp}. ")

        logger.trace(s"Tick $tick. $n $cp. Libros orquestrados en ${(new Date().getTime-initially)}ms ... performances: ${pollPerformances.size}")
      })
    )}

    val after = new Date().getTime
    logger.trace(s"Tick $tick. Libros orquestrados en ${(after-initially)}ms")
  }

  def getAccountMinimum(c: Currency): java.math.BigDecimal = {
    val acs = accountBalances.filter(ak => ak._1._1.equals(c))
    val numberOfExchanges :java.math.BigDecimal = java.math.BigDecimal.valueOf(acs.size)

    if (numberOfExchanges.equals(ZERO))
      return ZERO

    assert (numberOfExchanges.compareTo(ZERO) > 0, s"Currency ${c} has to have exchanges.")
    val minimumPercentage : java.math.BigDecimal = ONE.divide(numberOfExchanges, java.math.RoundingMode.HALF_EVEN).divide(
      java.math.BigDecimal.valueOf(4), java.math.RoundingMode.HALF_EVEN
    )
    val totalBalanz : java.math.BigDecimal = acs.map(_._2.getTotal).sum

    val allowable = totalBalanz.multiply(minimumPercentage).multiply(java.math.BigDecimal.valueOf(0.75))
    logger.debug(s"TOTAL $c BALANZ: $totalBalanz; minimumPercentage = $minimumPercentage; allowable = $allowable")
    allowable //spend only 75% of "allowed" post equilibria
  }

  def getBuyAmount(pair: CurrencyPair, counterSpendable: java.math.BigDecimal, book: OrderBook, fees: java.math.BigDecimal): java.math.BigDecimal = {
    val asks = book.getAsks
    var spendable = counterSpendable;
    var nextAskPos = 0
    val buyFee = ONE.add(fees)
    var buyAmount = ZERO

    while (spendable.compareTo(ZERO) > 0){

      val asko = asks.get(nextAskPos)

      val oCost = asko.getLimitPrice.multiply(asko.getTradableAmount).multiply(buyFee)

      val spent = if (oCost.compareTo(spendable) <= 0) {
        buyAmount = buyAmount.add(asko.getTradableAmount)
        oCost
      } else {
        val buyCost = asko.getLimitPrice.multiply(buyFee)
        buyAmount = buyAmount.add(spendable.divide(buyCost, RoundingMode.DOWN))
        spendable
      }

      nextAskPos = nextAskPos + 1
      spendable = spendable.subtract(spent)
    }

    buyAmount
  }

  /*def revenueFromSelling(pair: CurrencyPair, amount: java.math.BigDecimal, book: OrderBook, sellFee: Price): java.math.BigDecimal = {
    val bids = book.getBids
    var toSell = amount
    var nextBidPos = 0
    var accRevenue = ZERO

    //while (toSell.comp)
    accRevenue
  }*/

  def updateAccountBalances(acsPanel: AccountsPanel, tick: Int, pair: CurrencyPair): Unit = {
    val before = new Date().getTime

    exchanges.foreach(e => {
      marketAgents(e) ! UPDATE_BALANCES(e, tick)
    })

    val after = new Date().getTime
    logger.trace(s"Tick $tick. Balances orquestrados en ${(after-before)}ms")
  }

}


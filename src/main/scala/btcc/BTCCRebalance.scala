package btcc

import org.knowm.xchange.currency.{CurrencyPair, Currency}
import org.knowm.xchange.{ExchangeFactory, ExchangeSpecification}

class BTCCRebalance {
  import org.knowm.xchange.btcchina._
  import org.knowm.xchange.service.polling.account._
  import org.knowm.xchange.service.polling.trade._
  import org.knowm.xchange.service.polling.marketdata._
  import org.knowm.xchange.dto.trade._
  import org.knowm.xchange.dto._

  val btccAPI = "4fad88b2-cf38-4031-9eb9-ac2c8ae5631d"
  val btccSEC = "e1ae42fe-71f2-409e-89fc-118d6d62ff1e"
  //val btccAPI = "243e7cf1-0416-47f2-8bc8-4d97e068c5fe"
  //val btccSEC = "e3909064-8c57-4d99-976f-c7d7704ce068"

  val btccSpec: ExchangeSpecification = new ExchangeSpecification(classOf[BTCChinaExchange])

  btccSpec.setSecretKey(btccSEC);
  btccSpec.setApiKey(btccAPI);

  val btcc = ExchangeFactory.INSTANCE.createExchange(btccSpec);
  val btccAccountService: PollingAccountService  = btcc.getPollingAccountService();
  val btccTradeAgent: PollingTradeService  = btcc.getPollingTradeService();
  val btccPoller: PollingMarketDataService = btcc.getPollingMarketDataService

  val btccAccount = btccAccountService.getAccountInfo.getWallet

  val btccBTC = btccAccount.getBalance(Currency.BTC).getAvailable
  val btccCNY = btccAccount.getBalance(Currency.CNY).getAvailable

  /* Checkpoint 1
   * ---
   *
   * 2.18570000 BTC
   * 16.51103 CNY
   */

  /* Checkpoint 2
   *
   * 1.09570000 BTC
   * 3171.39249 CNY
   */

  val btccPrices = btccPoller.getOrderBook(CurrencyPair.BTC_CNY)
  val BTC_CNY_value = btccPrices.getBids.get(0).getLimitPrice

  val btccBTC_CNY = btccBTC.multiply(BTC_CNY_value)

  val diff = btccCNY.subtract(btccBTC_CNY)

  println ("BTC: " + btccBTC)
  println ("CNY: " + btccCNY)

  val currencyPair = CurrencyPair.BTC_CNY

  val order = if (btccBTC_CNY.compareTo(btccCNY) < 0) {
    println("Sell CNY. Buy BTC")
    val amount = diff.divide(java.math.BigDecimal.valueOf(2d)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP)
    println(amount)

    val order: MarketOrder = new MarketOrder(Order.OrderType.BID, amount, currencyPair);

    order
  } else {
    println("Sell BTC. Buy CNY")
    val cnyamount = diff.abs().divide(java.math.BigDecimal.valueOf(2d)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP)

    val btcamount = cnyamount.divide(BTC_CNY_value, 2, java.math.RoundingMode.HALF_UP)
    println(btcamount)

    val order: MarketOrder = new MarketOrder(Order.OrderType.ASK, btcamount, currencyPair);
    order
  }


  //btccTradeAgent.placeMarketOrder(order)

}

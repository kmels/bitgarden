package panels

import java.awt.GridLayout
import javax.swing.JPanel
import javax.swing.border.TitledBorder

import bgutils.GlobalConstants
import org.knowm.xchange.currency.{Currency, CurrencyPair}
import org.knowm.xchange.dto.account.Balance
import org.knowm.xchange.dto.marketdata.OrderBook
import org.apache.logging.log4j.LogManager
import GlobalConstants._
import scala.concurrent.Lock
import bgutils.UtilImplicits._

import java.math.BigDecimal._
import java.math.RoundingMode

class AccountsPanel {

  val logger = LogManager.getLogger(getClass)
  val _brush = new Lock()

  val container = new JPanel()
  container.setLayout(new GridLayout(1,2));

  val chart1 = new CurrencyPieChart()
  val chart2 = new CurrencyPieChart()

  def create(pair: CurrencyPair) : JPanel = {
    val p1 = chart1.recreate(pair.base)
    val p2 = chart2.recreate(pair.counter)

    container.add(p1)
    container.add(p2)

    container.setBorder(getBorder())

    container
  }

  def hasCurrency(c: Currency): Unit = chart1.getCurrency

  def getBorder() : TitledBorder = {
    val pair = new CurrencyPair(chart1.getCurrency, chart2.getCurrency);

    val scale = PIP_SPEC(pair).toPlainString.count(c => c.equals('0'))
    val navCurrency = "USD"

    var baseBalance : Precision = agents.totalCurrencyBalance(pair.base).setScale(scale, java.math.RoundingMode.HALF_EVEN)
    val baseNav     : Precision = currencyNavs.get(pair.base).getOrElse(ZERO)
    val counterBalance: Precision = agents.totalCurrencyBalance(pair.base).setScale(scale, java.math.RoundingMode.HALF_EVEN)
    val counterNav  : Precision = currencyNavs.get(pair.counter).getOrElse(ZERO)

    val totalNav : Precision = currencyNavs.values.sum.setScale(4, java.math.RoundingMode.HALF_UP)

    var baseShare = ZERO
    var counterShare = ZERO

    if (totalNav.compareTo(ZERO) > 0) {
      baseShare = baseNav.divide(totalNav, java.math.RoundingMode.HALF_EVEN).multiply(HUNDRED).setScale(4, java.math.RoundingMode.HALF_UP)
      counterShare = counterNav.divide(totalNav, java.math.RoundingMode.HALF_EVEN).multiply(HUNDRED).setScale(4, java.math.RoundingMode.HALF_UP)
    }

    javax.swing.BorderFactory.createTitledBorder(
      s"${pair.toString} - " +
        s"${pair.base} NAV: ${baseNav} ($baseShare%). ${pair.counter} NAV: ${counterNav} ($counterShare%). TOTAL NAV: ${totalNav} $navCurrency")
  }

  def updateBalances(n: String, cur: Currency, bal: Balance): Unit = {

    if (chart1.getCurrency.equals(cur)) {
      chart1.updateBalance(n, cur, bal)
      val total = GlobalConstants.accountBalances.filter(_._1._2.equals(cur)).map(_._2.getTotal).sum
      chart1.setTitle(s"${total} ${cur.toString}")
    }

    if (chart2.getCurrency.equals(cur)) {
      chart2.updateBalance(n, cur, bal)
      val total = GlobalConstants.accountBalances.filter(_._1._2.equals(cur)).map(_._2.getTotal).sum
      chart2.setTitle(s"${total} ${cur.toString}")
    }

    container.setBorder(getBorder())
  }

  def recreate(cp: CurrencyPair): Unit = {
    logger.debug(s"Recreando PriceChart: ${cp}")
    _brush.acquire()
    logger.debug(s"Removiendo contenido")
    container.removeAll()
    create(cp)
    logger.debug(s"Creando")
    logger.debug(s"Liberando")
    _brush.release()
  }
}

package panels


import java.awt.event._
import javax.swing._

import org.knowm.xchange.currency.CurrencyPair
import scala.collection.parallel.ParIterable

import org.apache.logging.log4j.{Logger, LogManager}

class MenuBar(currencyListings: Map[CurrencyPair, Seq[String]]
              , setExchange1: String => Boolean => Unit
              , setExchange2: String => Boolean => Unit
              , onCurrencyPairSelection: CurrencyPair => Boolean => Unit
              , currencyPair: () => CurrencyPair
            ) extends JMenuBar {

  val logger = LogManager.getLogger();

  //Create available exchanges dropdown
  var exchanges = new JMenu("Exchange");
  exchanges.setMnemonic(KeyEvent.VK_0);
  exchanges.getAccessibleContext().setAccessibleDescription(
    "The only menu in this program that has menu items");

  val exchange1Options : ButtonGroup  = new ButtonGroup();
  val exchange2Options : ButtonGroup  = new ButtonGroup();        

  val exchange1 = new JMenu("Exchange 1");
  exchange1.setMnemonic(KeyEvent.VK_1);
  
  val exchange2 = new JMenu("Exchange 2")
  exchange2.setMnemonic(KeyEvent.VK_2)

  exchanges.add(exchange1);
  exchanges.add(exchange2);

  def updateExchangeList(exchanges: Seq[String], exchangeGroup: ButtonGroup, submenu: JMenu, selectedIndex: Int, selectCallback: String => Boolean => Unit){
    submenu.removeAll()
    exchangeGroup.clearSelection()
    
    logger.debug("MenuBar: Exchanges = " + exchanges.mkString(","))
    for ((exchangeName,i) <- exchanges.zipWithIndex){
      var item = new JRadioButtonMenuItem(exchangeName)

      item.addActionListener(new ActionListener() {
        def actionPerformed(evt: ActionEvent) {
          val selectedExchange = evt.getActionCommand
          selectCallback(selectedExchange)(true)
        }
      })

      if (i==selectedIndex) {
        logger.info("MenuBar: Selecting " + exchangeName)
        item.setSelected(true)
      }
      
      val k = exchangeName.toLowerCase.charAt(0)
      item.setAccelerator(KeyStroke.getKeyStroke(k));

      exchangeGroup.add(item)
      submenu.add(item);
    }

  }

  def setCurrencyListing(l: String, onCurrencyPairChange: CurrencyPair => Boolean => Unit, recreate: Boolean) = {
    logger.info("MenuBar. Updating pair to " + l)
    val base = l.split('/')(0)
    val counter = l.split('/')(1)
    val pair = new CurrencyPair(base,counter)

    onCurrencyPairChange(pair)(false)

    val _exchanges = currencyListings(pair)

    updateExchangeList(_exchanges, exchange1Options, exchange1, 0, setExchange1 )

    if (_exchanges.size > 1) {
      updateExchangeList(_exchanges, exchange2Options, exchange2, 1, setExchange2)
      setExchange1( _exchanges (0)) (false)
      setExchange2( _exchanges (1)) (recreate)
    } else {
      setExchange1( _exchanges (0)) (recreate)
    }

  }

  //Create available listings dropdown
  var listings = new JMenu("Listings")
  listings.setMnemonic(KeyEvent.VK_X)

  val listingsGroup : ButtonGroup  = new ButtonGroup();
  var initialPairButton : JRadioButtonMenuItem = null

  for ((listing,i) <- currencyListings.keys.zipWithIndex){

    var item = new JRadioButtonMenuItem(listing.toString)

    item.addActionListener(new ActionListener() {
      def actionPerformed(e: ActionEvent) {
        logger.info("Action performed on " + e.getActionCommand)
        setCurrencyListing(e.getActionCommand, onCurrencyPairSelection, true)
      }
    })

    if (listing.equals(currencyPair())){
      item.setSelected(true)
    }

    listingsGroup.add(item)
    listings.add(item);
  }

  logger.info(s"MenuBar. Choosing ${currencyPair().toString()}")
  setCurrencyListing(currencyPair().toString, onCurrencyPairSelection, false)

  add(listings)
  add(exchanges)
}

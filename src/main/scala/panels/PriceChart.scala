package panels

import java.util
import java.util.{Date, Dictionary}
import javax.swing.{JLabel, JPanel, JSlider, SwingConstants}

import bgutils.GlobalConstants
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata.Trade
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService
import org.knowm.xchange.service.streaming.StreamingExchangeService
import org.apache.logging.log4j.LogManager
import org.knowm.xchart.internal.Series
import org.knowm.xchart.style.Styler.LegendPosition
import org.knowm.xchart.style.markers.SeriesMarkers
import org.knowm.xchart.{ChartBuilder_XY, Chart_XY, Series_XY, XChartPanel}
import types.BufferedTimeSeries

import scala.concurrent.Lock
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.collection.mutable.{ListBuffer, TreeSet}
import scala.concurrent.duration.Duration
import scala.concurrent.duration._

//, Map,
import scala.collection.parallel.mutable.ParMap
import scala.collection.parallel.ParIterable

import bgutils.UtilImplicits._

/**
 * @params
 * w - width
 * h - height
 */ 

class PriceChart (val timespan: Duration) {
  import GlobalConstants._
  
  type Price = java.math.BigDecimal
  type ChartLimits = (Date, Date, Price, Price) //left to right, bottom up

  val logger = LogManager.getLogger(getClass)
  val _brush = new Lock()

  implicit def orderedTrade: Ordering[Trade] = new Ordering[Trade] {
    override def compare(x: Trade, y: Trade): Int = {
      if (x.getTimestamp before y.getTimestamp) 
        -1 else
      if (x.getTimestamp after y.getTimestamp) 1 
      else 0
    }
  }

  def hasSeries(n: String) = _chart.getChart.getSeriesMap.contains(n)

  val container = new JPanel()

  container.setLayout(new javax.swing.BoxLayout(container, javax.swing.BoxLayout.PAGE_AXIS));

  container.setBorder(javax.swing.BorderFactory.createTitledBorder(
    "Prices"))

  val _w = 600
  val _h = 400

  /*static final int FPS_MIN = 0;
  static final int FPS_MAX = 30;
  static final int FPS_INIT = 15;    //initial frames per second
  . . .
  JSlider framesPerSecond = new JSlider(JSlider.HORIZONTAL,
    FPS_MIN, FPS_MAX, FPS_INIT);
  framesPerSecond.addChangeListener(this);

  //Turn on labels at major tick marks.
  framesPerSecond.setMajorTickSpacing(10);
  framesPerSecond.setMinorTickSpacing(1);
  framesPerSecond.setPaintTicks(true);
  framesPerSecond.setPaintLabels(true);*/

  val sliderLabels: Dictionary[Int, JLabel] = {
    val m: Map[Int, JLabel] = Array("M1", "M5", "M15", "M30", "H1", "H4", "H12", "D1", "W1").zipWithIndex.map { case (a, b) =>
      (b, new JLabel(a))
    }.toMap
    new java.util.Hashtable[Int, JLabel](m)
  }
  val slider = new JSlider(SwingConstants.HORIZONTAL, 0, sliderLabels.size - 1, 0)
  slider.setLabelTable(sliderLabels)
  slider.setPaintLabels(true)
  slider.setPaintTicks(true)
  //slider.setPaintTrack(true)

  // all the data
  var _chart : XChartPanel[Chart_XY] = _

  // display data
  def create(pair: CurrencyPair) : JPanel = {

    val picturableTrades = marketTrades.filter(m => m._1._2.equals(pair))

    val exchangeList = currencyListings(pair).keys

    logger.info(s"Creando grafica de precios con ${picturableTrades.size} exchanges: ${picturableTrades.keys.mkString(",")}")

    val canvas = new ChartBuilder_XY().xAxisTitle("Date").yAxisTitle(s"Price (${pair})").width(_w).height(_h).build()

    exchangeList.foreach( e => {
      val xs : java.util.List[Date] = List(new Date())
      val ys : java.util.List[java.math.BigDecimal] = List(java.math.BigDecimal.valueOf(4d))
      var series: Series_XY = canvas.addSeries(e, xs, ys)

      logger.debug(s"PriceChart. Color de $e es ${COLORS(e)}")

      series.setFillColor(COLORS(e))
      series.setLineColor(COLORS(e))
      series.setMarker(SeriesMarkers.CIRCLE)
      series.setMarkerColor(COLORS(e))
    })

    canvas.getStyler.setLegendPosition(LegendPosition.InsideSW)
    canvas.getStyler.setAxisTitlesVisible(false)

    val chartLimits = setLimits(canvas, picturableTrades, pair, timespan )
    contain(canvas, chartLimits, picturableTrades)
    return container
  } 

  def contain(canvas: Chart_XY, limits: ChartLimits, ss: Market[BufferedTimeSeries[Trade]]){
    _chart = new XChartPanel[Chart_XY](canvas)
    container.add(slider)
    container.add(_chart)
  }

  def recreate(cp: CurrencyPair): Unit = {
    logger.debug(s"Recreando PriceChart: ${cp}")
    _brush.acquire()
    logger.debug(s"Removiendo contenido")
    container.removeAll()
    logger.debug(s"Creando")
    create(cp)
    logger.debug(s"Liberando")
    _brush.release()

    currencyListings(cp).foreach(pairListing => {

      if (marketTrades.contains((pairListing._1,cp))){
        logger.debug(s"Recreando PriceChart: ${pairListing._1} ${cp}")
        this.updatePrices((pairListing._1, cp))
      }

    })

  }

  /* Given all trades, find the earliest in common and the oldest */
  def setLimits(chart: Chart_XY, lines: Market[BufferedTimeSeries[Trade]], pair: CurrencyPair, duration: Duration): ChartLimits = {
    var xAxisMax: Date = new Date()
    var xAxisMin: Date = new Date(xAxisMax.getTime - duration.toMillis)

    logger.debug(s"Precios. La fecha inferior es ${sdf.format(xAxisMin)} - la superior es ${sdf.format(xAxisMax)} - duracion: ${ Duration(xAxisMax.getTime - xAxisMin.getTime, MILLISECONDS).toMinutes} minutos" )

    val framed = lines.flatMap( m => {
      val (k,ts) = m

      assert(k._2.equals(pair), s"Pintando precios ${pair} pero se pidio ${k._2}")

      val _ts : Seq[Trade] = ts.toSeq.filter(t => t.getTimestamp.after(xAxisMin) )

      if (_ts.isEmpty)
        logger.debug(s"PriceChart. ${k._1} ${k._2}: No hay")
      else
        logger.debug(s"PriceChart. ${k._1} ${k._2}: high ${_ts.map(_.getPrice).max} low ${_ts.map(_.getPrice).min}.")
      _ts
    })

    val prices = framed.map(_.getPrice)

    val (yAxisMin, yAxisMax): (Price, Price) = if (prices.nonEmpty) {
      val yAxisMin = prices.min
      val yAxisMax = prices.max

      chart.getStyler().setYAxisMin(yAxisMin.doubleValue)
      chart.getStyler().setYAxisMax(yAxisMax.doubleValue)
      (yAxisMin, yAxisMax)
    } else (null, null)

    chart.getStyler().setXAxisMin(xAxisMin.getTime)
    chart.getStyler().setXAxisMax(xAxisMax.getTime)

    logger.debug(s"PriceChart. Los limites son: " +  (xAxisMin, xAxisMax, yAxisMin, yAxisMax) + s". Enmarcados: ${prices.size}")
    (xAxisMin, xAxisMax, yAxisMin, yAxisMax)
  }
  
  def updatePrices(market: (String, CurrencyPair)): Unit = {
    _brush.acquire()

    val (exchange,pair) = market

    val canvas: XChartPanel[Chart_XY] = container.getComponent(container.getComponentCount-1).asInstanceOf[XChartPanel[Chart_XY]]

    assert(marketTrades.contains(market), s"Antes de pintar, debe haber informacion para ${market}")

    val times: java.util.List[java.util.Date] = marketTrades(market).map(_.getTimestamp)
    val prices: java.util.List[java.math.BigDecimal] = marketTrades(market).map(_.getPrice())

    if (prices.size > 1){

      assert (!times.head.after(times.last), s"El tiempo debe estar ordenado en los precios de ${market._1}, pero el primer precio es ${times.head} y el ultimo es ${times.last}.")

      val picturableTrades = marketTrades.filter(m => m._1._2.equals(pair))

      //logger.info("PriceChart. Llaves de precios: " + marketTrades.keys.mkString(",") )

      logger.trace(s"PriceChart. Actualizando ${market}. Precios ilustrables: ${picturableTrades.size} - ${picturableTrades.map({ case(k,ps) => (k,ps.size) }).mkString(",") } ")

      setLimits(canvas.getChart.asInstanceOf[Chart_XY], picturableTrades, pair, timespan)

      logger.debug(s"PriceChart #${exchange}: Actualizando serie con ${times.size()} puntos. Primero: ${sdf.format(times.head.getTime)}=>${prices.head}, ultimo: ${sdf.format(times.last.getTime)}=>${prices.last}. ")

      if (hasSeries(exchange)) {
        val series: Series_XY = canvas.updateSeries(market._1, times, prices, null).asInstanceOf[Series_XY]
        series.setFillColor(COLORS(exchange))
        series.setLineColor(COLORS(exchange))
        series.setMarker(SeriesMarkers.CIRCLE)
        series.setMarkerColor(COLORS(exchange))
      } else{
        val series =
          canvas.getChart.asInstanceOf[Chart_XY].addSeries(market._1, times, prices)

        series.setFillColor(COLORS(exchange))
        series.setLineColor(COLORS(exchange))
        series.setMarker(SeriesMarkers.CIRCLE)
        series.setMarkerColor(COLORS(exchange))
      }
    }

    _brush.release()
  }
}

package panels

import javax.swing.JPanel

import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.Order.OrderType
import org.knowm.xchange.dto.marketdata._
import org.knowm.xchange.dto.trade.LimitOrder
import org.knowm.xchart.Series_XY.ChartXYSeriesRenderStyle
import org.knowm.xchart.style.Styler.LegendPosition
import org.knowm.xchart.style.markers.SeriesMarkers
import org.knowm.xchart.{ChartBuilder_XY, XChartPanel, Chart_XY}

import scala.collection.JavaConversions._
import scala.collection.parallel.mutable.{ParSeq, ParMap}
import scala.concurrent.Lock

import bgutils._
import MemoryData._
import java.util.Date

import UtilImplicits._
import java.math.BigDecimal._
import java.math.RoundingMode

class DepthChart(val e1: String, val e2: String, val cp: CurrencyPair) extends JPanel{
  import GlobalConstants._

  type Precision = java.math.BigDecimal
  type ChartLimits = (Price, Price, Volume, Volume) //left to right, bottom up  
  
  var tick = 0
  val canvasIndexes  = scala.collection.mutable.Map[(String,String),Int]()

  val _brush = new Lock()

  this.setLayout(new java.awt.BorderLayout(5, 10))

  this.setBorder(javax.swing.BorderFactory.createTitledBorder(
    "Order books"))
    
  private def contain(canvas: Chart_XY, chartLimits: ChartLimits, x: String, y: String, cp: CurrencyPair): Unit = {
    val buy = "Buy " + x
    val sell = "Sell " + y
    
    //val depths@(askPrices, askVolumes, bidPrices, bidVolumes) = dataPoints((x,cp),(y,cp))

    val askPrices: Seq[Price] = Seq(java.math.BigDecimal.valueOf(4d))
    val askVolumes: Seq[Volume] = Seq(java.math.BigDecimal.valueOf(20d))
    val bidPrices: Seq[Price] = Seq(java.math.BigDecimal.valueOf(2d))
    val bidVolumes: Seq[Volume] = Seq(java.math.BigDecimal.valueOf(40d))

    logger.info(s"Anadiendo depth serie ${buy}")
    logger.info(s"Anadiendo depth serie ${sell}")

    try {
      val asksSeries = canvas.addSeries(buy, askPrices, askVolumes)
      val bidsSeries = canvas.addSeries(sell, bidPrices.reverse, bidVolumes.reverse)

      bidsSeries.setMarker(SeriesMarkers.TRIANGLE_DOWN);
      asksSeries.setMarker(SeriesMarkers.TRIANGLE_UP);

      asksSeries.setLineColor(COLORS(x))
      asksSeries.setFillColor(COLORS(x))
      asksSeries.setMarkerColor(COLORS(x))
      asksSeries.setMarker(SeriesMarkers.TRIANGLE_UP)

      bidsSeries.setLineColor(COLORS(y))
      bidsSeries.setFillColor(COLORS(y))
      bidsSeries.setMarkerColor(COLORS(y))
      bidsSeries.setMarker(SeriesMarkers.TRIANGLE_DOWN)

      canvasIndexes += (((buy, sell), this.getComponentCount))
      val _chart = new XChartPanel[Chart_XY](canvas)
      this.add(_chart, java.awt.BorderLayout.CENTER)
    } catch {
      case e: Exception => logger.error(s"""Buy $e1 - Sell $e2 $cp. Error al anadir series en la creacion.""", e)
    }
  }

  private def create(exchange1: String, exchange2: String, cp: CurrencyPair): Unit = {
    val canvas = new ChartBuilder_XY().title(s"Buy $exchange1 / Sell $exchange2").xAxisTitle("Date").yAxisTitle("Price")
      .width(600).height(400).build()

    canvas.getStyler.setDefaultSeriesRenderStyle(ChartXYSeriesRenderStyle.Area)

    assert (currencyListing(exchange1)(cp).isDefined, exchange1 + " debe tener el mercado " + cp.toString )
    assert (currencyListing(exchange2)(cp).isDefined, exchange2 + " debe tener el mercado " + cp.toString )

    if (currencyListing(exchange1)(cp).get._2){
      val currencyPair = currencyListing(exchange1)(cp).get._1
      canvas.setYAxisTitle(currencyPair.base.getCurrencyCode)
      canvas.setXAxisTitle(currencyPair.counter.getCurrencyCode)
    } else {
      val currencyPair = currencyListing(exchange2)(cp).get._1
      canvas.setYAxisTitle(currencyPair.base.getCurrencyCode)
      canvas.setXAxisTitle(currencyPair.counter.getCurrencyCode)
    }

    val num3 = java.math.BigDecimal.valueOf(3d)
    val num2= java.math.BigDecimal.valueOf(2d)
    val dummyAsks: ParSeq[LimitOrder] = ParSeq(new LimitOrder(OrderType.ASK, num3, cp, "id" , new Date(), num3))
    val dummyBids: ParSeq[LimitOrder] = ParSeq(new LimitOrder(OrderType.BID, num2, cp, "id" , new Date(), num2))

    val dummyData: AccDepthData = new AccDepthData(dummyAsks.map(_.getLimitPrice), dummyAsks.map(_.getTradableAmount),
                                dummyBids.map(_.getLimitPrice), dummyBids.map(_.getTradableAmount))

    val chartLimits = setLimits(canvas, dummyData, None)

    canvas.getStyler.setLegendPosition(LegendPosition.InsideNW)
    canvas.getStyler.setAxisTitlesVisible(false)

    contain(canvas, chartLimits, exchange1, exchange2, cp)
  }

  create(e1,e2,cp)

  def updateData(deepestArb: Option[(OrderSpec,OrderSpec)]): Unit = {
    _brush.acquire()
    val initially = new java.util.Date().getTime

    val buy = "Buy " + e1
    val sell = "Sell " + e2

    val depths = makeDepth((e1,cp), (e2,cp))
    if (depths.under._1.isEmpty) {
      logger.warn(s"Empty book! $e1 - asks")
      _brush.release()
      return
    }
    if (depths.under._3.isEmpty) {
      logger.warn(s"Empty book! $e2 - bids")
      _brush.release()
      return
    }

    /* Update area */
    val canvasId = canvasIndexes.get((buy,sell))
    assert(canvasId.isDefined, (buy,sell) + " is not defined")
    val canvas: XChartPanel[Chart_XY] = this.getComponent(canvasId.get).asInstanceOf[XChartPanel[Chart_XY]]
   
    tick += 1    

    val chartLimits = setLimits(canvas.getChart().asInstanceOf[Chart_XY], depths, deepestArb)

    try {
      canvas.updateSeries(buy, depths._1.seq, depths._2.seq, null)
      canvas.updateSeries(sell, depths._3.seq.reverse, depths._4.seq.reverse, null)
    } catch {
      case e: Exception => logger.error(s"Buy ${e1} - Sell ${e2} ${cp}. Error al anadir series en la actualizacion. " +
        s"Top 5 asks: ${depths._1.take(5).mkString(",")}. " +
        s"Top 5 bids: ${depths._3.take(5).mkString(",")}", e)
    }

    val ms = (new java.util.Date().getTime() - initially) 
    _brush.release()
  }

  def setLimits(canvas: Chart_XY, depthData: AccDepthData, deepestArb: Option[(OrderSpec, OrderSpec)]): ChartLimits = {
    val initially = new java.util.Date().getTime

    val (askPrices, askAcVolumes, bidPrices, bidAcVolumes) = depthData.under

    assert(askPrices.size > 0)
    assert(bidPrices.size > 0)

    /*logger.trace(s"Buy ${e1} - Sell ${e2} ${cp}. " +
      s"Los primeros asks: ${askPrices.take(5).mkString(",")}" +
      s"Los primeros bids: ${bidPrices.take(5).mkString(",")}" +
      s"Los primeros ac asks: ${askPrices.zip(askAcVolumes).take(5).mkString(",")}" +
      s"Los primeros ac bids: ${bidPrices.zip(bidAcVolumes).take(5).mkString(",")}" +
      s"Deepest arb: ${deepestArb}") */

    //def pp(b : LimitOrder): (java.math.BigDecimal, java.math.BigDecimal) = (b.getTradableAmount, b.getLimitPrice)
    //def ppp(os: java.util.List[LimitOrder]): String = os.map(pp).take(5).mkString("\n\t")

    val topAsk = askPrices.head
    val topBid = bidPrices.head

    val overlap = topBid.compareTo(topAsk) > 0

    var (xAxisMin, bidMaxVol): (Price, Volume) = if (overlap) {
      assert(deepestArb.isDefined)
      val highestExecutableAsk = deepestArb.get._1._1
      val lowestExecutableBid = deepestArb.get._2._1
      val lowestBid = if (highestExecutableAsk.compareTo(lowestExecutableBid) < 0) topAsk else deepestArb.get._2._1

      val limits = bidPrices.zipWithIndex.dropWhile(bidPrice => {
        bidPrice._1.compareTo(lowestBid) >= 0
      })

      (limits.headOption.map(_._1).getOrElse(bidPrices.last),
        bidAcVolumes(limits.headOption.map(_._2).getOrElse(bidPrices.size - 1)))
    } else {
      assert (bidPrices.size > 0, bidPrices.mkString(","))
      assert (bidPrices.size > 0, bidPrices.mkString(","))

      logger.trace(s"Buy ${e1} - Sell ${e2} ${cp}. No hay overlap. Tomando maximo bid vol : ${bidAcVolumes.take(2).last}")
      (bidPrices.take(2).last, bidAcVolumes.take(2).last)
    }

    var (xAxisMax, askMaxVol) : (Price, Volume) = if (overlap) {
      assert(deepestArb.isDefined)
      val highestExecutableAsk = deepestArb.get._1._1
      val lowestExecutableBid = deepestArb.get._2._1
      val highestAsk = if (highestExecutableAsk.compareTo(lowestExecutableBid) < 0) topBid else deepestArb.get._1._1

      val limits = askPrices.zipWithIndex.dropWhile(askPrice => {
        askPrice._1.compareTo(highestAsk) <= 0
      })

      (limits.headOption.map(_._1).getOrElse(askPrices.last),
        askAcVolumes(limits.headOption.map(_._2).getOrElse(askPrices.size - 1)))
    } else {
      logger.trace(s"Buy ${e1} - Sell ${e2} ${cp}. No hay overlap. Tomando maximo ask vol : ${askAcVolumes.take(2).last}")
      (askPrices.take(2).last, askAcVolumes.take(2).last)
    }
    
    val yAxisMin = ZERO
    val yAxisMax = max(bidMaxVol, askMaxVol)

    assert(xAxisMin.compareTo(xAxisMax) < 0, s"Buy ${e1} - Sell ${e2} ${cp}. El minimo ${xAxisMin} no es menor que el maximo ${xAxisMax}")

    canvas.getStyler().setXAxisMin(xAxisMin.doubleValue)
    canvas.getStyler().setXAxisMax(xAxisMax.doubleValue)
    canvas.getStyler().setYAxisMin(yAxisMin.doubleValue)
    canvas.getStyler().setYAxisMax(yAxisMax.doubleValue)


    val ms = (new java.util.Date().getTime() - initially) 
    //println("Set limits in "+ms)
    logger.debug(s"Buy ${e1} - Sell ${e2}. DepthChart. Los limites son: " +  (xAxisMin, xAxisMax, yAxisMin, yAxisMax) + s". ")

    (xAxisMin, xAxisMax, yAxisMin, yAxisMax)
    
  }

  def min(x: Precision,y: Precision): Precision = if (x.compareTo(y) < 0 ) x else y
  def max(x: Precision,y: Precision): Precision = if (x.compareTo(y) > 0 ) x else y
}

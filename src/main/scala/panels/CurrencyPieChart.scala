package panels

import java.awt.Color
import javax.swing.JPanel

import bgutils.GlobalConstants
import bgutils.UtilImplicits._
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.dto.account.Balance
import org.apache.logging.log4j.LogManager
import org.knowm.xchart.style.Styler.LegendPosition
import org.knowm.xchart.style.Styler_Pie.AnnotationType
import org.knowm.xchart.{ChartBuilder_XY, ChartBuilder_Pie, Chart_Pie, XChartPanel}
import scala.collection.JavaConversions._
import scala.concurrent.Lock

class CurrencyPieChart {

  import GlobalConstants._
  val logger = LogManager.getLogger(getClass)
  val _brush = new Lock()
  val container = new JPanel()
  container.setLayout(new javax.swing.BoxLayout(container, javax.swing.BoxLayout.PAGE_AXIS));

  val _w = 600
  val _h = 400

  var _chart : XChartPanel[Chart_Pie] = _
  var _cur   : Currency = null

  def create(cur: Currency) : JPanel = {
    val accounts : Set[ExchangeName] = currencyListings.filter(cp => cp._1.base.equals(cur) || cp._1.counter.equals(cur))
      .values.flatMap(e => e.keys).toSet

    logger.info(s"Creando grafica de balances $cur con ${accounts.size} cuentas: ${accounts.mkString(",")}, colores: ${accounts.map(a => (a, COLORS(a))).mkString(",")} }")

    val canvas: Chart_Pie = new ChartBuilder_Pie().width(_w).height(_h).title(cur.toString).build
    canvas.getStyler.setLegendPosition(LegendPosition.InsideNE)
    canvas.getStyler.setPlotContentSize(.7)
    canvas.getStyler.setAnnotationType(AnnotationType.Percentage)

    val sliceColors: Array[Color] = accounts.map(e => COLORS(e)).toArray
    canvas.getStyler.setSeriesColors(sliceColors)

    accounts.foreach( name => {
      canvas.addSeries(name, 0)
    })

    contain(canvas)

    _cur = cur
    return container
  }

  def setTitle(s: String): Unit = {
    val canvas: XChartPanel[Chart_Pie] = container.getComponent(container.getComponentCount-1).asInstanceOf[XChartPanel[Chart_Pie]]
    val chart = canvas.getChart
    chart.setTitle(s)
  }

  def getCurrency = _cur

  def contain(canvas: Chart_Pie){
    _chart = new XChartPanel[Chart_Pie](canvas)
    container.add(_chart)
  }

  def recreate(cur: Currency): JPanel = {
    logger.debug(s"Recreando CurrencyPieChart: ${cur}")
    _brush.acquire()
    logger.debug(s"Removiendo contenido")
    container.removeAll()
    logger.debug(s"Creando")
    val p = create(cur)
    logger.debug(s"Liberando")
    _brush.release()

    val total = GlobalConstants.accountBalances.filter(_._1._2.equals(cur)).map(_._2.getTotal).sum
    setTitle(s"${total} ${cur.toString}")
    updateAccounts(cur)
    p
  }

  private def updateAccounts(cur: Currency): Unit ={
    val accounts = accountBalances.filter(_._1._2.equals(cur))
    logger.info(s"Actualizando grafica de balances con ${accounts.size} cuentas: ${accounts.keys.mkString(",")}")

    accounts.foreach(a => {
      logger.debug(s"Actualizando CurrencyPieChart: ${a._1}")
      this.updateBalance(a._1._1, a._1._2, a._2)
    })
  }

  def updateBalance(n: String, cur: Currency, bal: Balance): Unit = {
    _brush.acquire()
    logger.debug(s"Actualizando CurrencyPieChart: ${n} ${cur} ${bal}")
    val canvas: XChartPanel[Chart_Pie] = container.getComponent(container.getComponentCount-1).asInstanceOf[XChartPanel[Chart_Pie]]

    val chart = canvas.getChart

    if (chart.getSeriesMap.get(n) == null) {
      logger.info(s"CurrencyPieChart ${n}. Picking color ${COLORS(n)} ?")
      val accounts = accountBalances.filter(_._1._2.equals(cur))
      val sliceColors: Array[Color] = accounts.keys.map(_._1).map(e => COLORS(e)).toArray
      chart.getStyler.setSeriesColors(sliceColors)
      val s = chart.addSeries(n, bal.getTotal)
      s.setFillColor(COLORS(n))
    }else {
      val s = chart.getSeriesMap.get(n)
      s.setValue(bal.getTotal)
      s.setFillColor(COLORS(n))
    }

    _brush.release()
  }

}

package panels

import java.awt.{Font, GridLayout}
import java.util.Date
import javax.swing.event.ChangeEvent
import javax.swing.{UIManager, JPanel, JTabbedPane}

import bgutils.{GlobalUtils, UnprofitableOMException, GlobalConstants}
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata.OrderBook
import org.apache.logging.log4j.LogManager

import scala.collection.mutable
import scala.collection.mutable.Set
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.concurrent.Lock

import bgutils.UtilImplicits._

import GlobalConstants._
import scala.collection.JavaConversions._

import scala.collection.parallel.ParIterable
import scala.collection.parallel.mutable.ParSeq

class OpportunityPanel(val onCurrencyPairSelection: Option[CurrencyPair => Boolean => Unit]) extends JTabbedPane{
  type Title = String

  val logger = LogManager.getLogger(getClass)

  val parentTabs : mutable.Map[CurrencyPair, (OpportunityPanel, Title)] = Map()
  val childrenTitles: Map[ArbitrageCombo, Title] = Map()

  val depthCharts: Map[ArbitrageCombo, DepthChart] = Map()
  val executionPanels: Map[ArbitrageCombo, ExecutionPanel] = Map()

  val _brush = new Lock()

  private def updateCombo(combo: ArbitrageCombo): Unit = {
    if (!marketOpportunities.contains(combo))
      return
    val om = marketOpportunities.get(combo).map(_._1).getOrElse(return)

    val (buyer, seller, parentTabTitle) = combo
    // add child

    if (!childrenTitles.contains(combo)) {
      logger.trace(s"Adding combo child: $combo")
      // add child components
      val dc = new DepthChart(combo._1, combo._2, combo._3)
      depthCharts.put(combo, dc)
      val ec = new ExecutionPanel(combo._1, combo._2, combo._3)
      executionPanels.put(combo, ec)

      // add parent
      if (!parentTabs.contains(parentTabTitle)) {
        logger.trace(s"Adding combo parent: $combo")
        if (indexOfTab(combo._3.toString) >= 0)
          removeTabAt(indexOfTab(combo._3.toString))
        assert(indexOfTab(combo._3.toString) < 0)

        val tabs = new OpportunityPanel(None)
        tabs.setOpaque(true)

        var idx = this.getTabCount
        if (idx < 0)
          idx = 0

        parentTabs.put(combo._3, (tabs, combo._3.toString))
        assert(idx >= 0)

        this.insertTab(combo._3.toString, null, tabs, s"${combo._3.toString}", idx)
        this.setBackgroundAt(idx, java.awt.Color.YELLOW)
      }

      addCombo(combo, dc, ec)
    }

    assert(childrenTitles.contains(combo), "child should have been added.")

    val dc = depthCharts(combo)
    val ec = executionPanels(combo)

    assert(dc.e1.equals(combo._1))
    assert(dc.e2.equals(combo._2))
    assert(dc.cp.equals(combo._3))
    assert(ec.e1.equals(combo._1))
    assert(ec.e2.equals(combo._2))
    assert(ec.cp.equals(combo._3))

    val pair = combo._3

    val deepestArb = ec.updateData(om)
    dc.updateData(deepestArb);

    /*else {
      logger.warn(s"Buy ${combo._1} - Sell ${combo._2} ${combo._3}. Dead book. Buyer: ${marketBooks.contains((combo._1, combo._3))}. Seller: ${marketBooks.contains((combo._2, combo._3))}")
      marketBookUpdates.remove((combo._1,combo._3))
      marketBookUpdates.remove((combo._2,combo._3))
      marketBooks.remove((combo._1,combo._3))
      marketBooks.remove((combo._2,combo._3))
      return
    }*/

    /*logger.debug(s"Tabs ${parentTabs.size} (${this.getTabCount}): ${parentTabs.values.map(_._2).mkString(",")}")
    logger.debug(s"Children tabs ${childrenTitles.size} (${parentTabs.values.map(_._1.getTabCount).sum}): ${childrenTitles.map(c => (c._1._3,c._2)).mkString(",")}")
    logger.debug(s"Market ops ${marketOpportunities.keys.size}: ${marketOpportunities.mkString(",")}") */

    val title = childrenTitles(combo)
    val parentTab = parentTabs(pair)._1

    val i = parentTab.indexOfTab(title)

    val updateTs = GlobalConstants.marketBookUpdates.get((combo._1, combo._3)).getOrElse(return)
      .min(GlobalConstants.marketBookUpdates.get((combo._2, combo._3)).getOrElse(return))

    val age = new Date().getTime - updateTs

    logger.trace(s"Looking for tab component at = $i of ${parentTab.getTabCount} .. ${parentTab.getComponentAt(i)}")
    val opState = marketOpportunities.get(combo).map(_._2).getOrElse(return)

    parentTab.setBackgroundAt(i, opState.color)
    parentTab.createToolTip().setFont(new Font("Verdana", Font.BOLD, 16))
    parentTab.setToolTipTextAt(i, s"${opState.name} - Hace $age ms.")
  }


  def updateOpps(cp: CurrencyPair, tick: Int) = {
    logger.trace(s"Tick $tick. $cp. Actualizando op tabs.")
    _brush.acquire()

    try {
      val toUpdate = marketOpportunities.keys
      logger.trace(s"Tick $tick. $cp. Actualizando op tabs ... actuales: ${toUpdate.mkString(",")} ")

      toUpdate.foreach(updateCombo)

      val outdatedCombos = depthCharts.keys.filter(c => !marketOpportunities.contains(c))
      logger.trace(s"Tick $tick. $cp. Borrando $cp combo tabs ... Outdated: ${outdatedCombos.mkString(",")}.")
      outdatedCombos.foreach(removeCombo)
    } catch {
      case e: Exception => logger.warn("Algo estuvo turbio en la actualizacion de op tabs", e)
    }
    _brush.release()
    logger.trace(s"Tick $tick. $cp. Actualizo op tabs.")
  }

  private def addCombo(combo: ArbitrageCombo, depthChart: DepthChart, executionPanel: ExecutionPanel): Unit = {

    logger.trace(s"Adding combo: $combo")
    //soundPlayer ! "/home/campesino/bitgarden/src/main/resources/sounds/beep-21.wav"

    /* add this combo to the currency pair's tab container */
    val parentTab : OpportunityPanel = parentTabs(combo._3)._1
    val tabContent = makeTabPanel(depthChart, executionPanel);

    val child_idx = parentTab.getComponentCount

    val title = s"Buy ${depthChart.e1} - Sell ${depthChart.e2}"
    val text = title
    val icon : javax.swing.Icon = null

    childrenTitles += ((combo, title))
    val opState = marketOpportunities.get(combo).map(_._2).getOrElse(return)

    parentTab.insertTab(title, icon, tabContent, text + s" - ${opState.name}", child_idx)
    tabContent.setOpaque(true)
    tabContent.setBackground(opState.color)

    assert (child_idx >= 0)

    //this.setBackgroundAt(child_idx, stColor)

    logger.debug(s"Buy ${combo._1} - Sell ${combo._2} ${combo._3}. " +
      s"Added combo tab at idx: ${child_idx}. Pair combos: ${parentTab.getComponentCount}")
  }

  private def makeTabPanel(depth: DepthChart, executions: ExecutionPanel): JPanel ={
    val panel = new JPanel()

    panel.setLayout(new GridLayout(2, 1));
    panel.add(depth)
    panel.add(executions)

    depth.setOpaque(true)
    executions.setOpaque(true)
    panel.setOpaque(true)

    panel
  }

  private def removeCombo(combo: ArbitrageCombo): Unit = {
    if (marketOpportunities.contains(combo)) {
      logger.warn("Not removing combo, because it is still in marketOpportunities.")
      return
    }

    assert(!marketOpportunities.contains(combo), s"Before removing combo, opportunities shouldn't have $combo, but keys are: ${marketOpportunities.keys}" )

    if (!parentTabs.contains(combo._3)) {
      logger.trace(s"Buy ${combo._1} - Sell ${combo._2} ${combo._3}. #removeCombo. Already removed")
      return
    }
    logger.info(s"Buy ${combo._1} - Sell ${combo._2} ${combo._3}. #removeCombo. " +
      s"Current for ${combo._3}: ${parentTabs(combo._3)}")

    val (parentTab, parentTitle) = parentTabs(combo._3)

    val siblings = childrenTitles.filter(c=>c._1._3.equals(combo._3) && !c._1.equals(combo))

    val childIndex : Option[Int] = childrenTitles.get(combo).fold({
      logger.warn(s"Buy ${combo._1} - Sell ${combo._2} ${combo._3}. No children tab found. ${childrenTitles.get(combo)}.")
      val n : Option[Int] = None
      n
    })(t => {
      val child_idx = parentTab.indexOfTab(t)
      if (child_idx >= 0) {
        parentTab.removeTabAt(child_idx)

        logger.trace(s"Buy ${combo._1} - Sell ${combo._2} ${combo._3}. " +
          s"Removed combo tab ${child_idx}. Other ${siblings.size} are sibling combos " +
          s"Siblings: ${siblings.map(_._2).mkString(",")}")

        Option(child_idx)
      } else None
    })

    childrenTitles.remove(combo)

    if (siblings.size == 0){
      logger.debug(s"Buy ${combo._1} - Sell ${combo._2} ${combo._3}. " +
        s"Removing parent tab. Other parents left: ${this.getTabCount-1}")

      parentTabs.remove(combo._3)
      if (indexOfTab(parentTitle) > 0)
        this.removeTabAt(indexOfTab(parentTitle))
    }

    depthCharts.remove(combo)
    executionPanels.remove(combo)

    if (childIndex.isDefined)
      assert(siblings.size == parentTab.getTabCount(), s" " +
        s"siblings de Buy ${combo._1} - Sell ${combo._2} ${combo._3} are ${siblings.size} i.e. (${siblings.map(_._2).mkString(",")}) deben ser ${parentTab.getTabCount} tabs")

    assert(!depthCharts.contains(combo))
  }

  addChangeListener(new javax.swing.event.ChangeListener() {
    override def stateChanged(ev : ChangeEvent) {

      if (getSelectedIndex >= 0){
        val currencyPairTitle = getTitleAt(getSelectedIndex()).split('/')

        if (currencyPairTitle.size > 1) {
          logger.info(s"Selected index ${getSelectedIndex} with title ${getTitleAt(getSelectedIndex)} .. ${currencyPairTitle.mkString(",")}")
          val base = currencyPairTitle(0)
          val counter = currencyPairTitle(1)

          if (onCurrencyPairSelection.isDefined)
            onCurrencyPairSelection.get(new CurrencyPair(base, counter))(true)
        }
      }

    }
  })

}

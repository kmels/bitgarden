package panels

import java.awt.{Font, Color}
import java.util.Date
import javax.swing.{JLabel, JPanel, SwingConstants}

import bgutils.GlobalConstants
import bgutils.GlobalConstants._
import bgutils.UtilImplicits._
import org.knowm.xchange.currency.CurrencyPair
import org.apache.logging.log4j.LogManager

import scala.collection.mutable
import scala.concurrent.duration._

/* Imprime los ultimos precios, que tan viejos son. Ordenados de menor a mayor.
   Imprime los spreads, que tan viejos son. Ordenados de menor a mayor
  */
class JMobar[A, B](val series: ConcurrentMarket[A], val updates: ConcurrentMarket[Long],
               val lastValFun: A => Option[B],   val sorting: scala.math.Ordering[B]) extends JPanel{

  setOpaque(true)
  setBackground(Color.BLACK)

  val logger = LogManager.getLogger()
  var labels : Seq[JLabel] = Seq()

  def create(pair: CurrencyPair) = {
    update(pair)
  }

  def update(pair: CurrencyPair): Unit = {
    val pairSeries : Market[A] = series.filter(_series => _series._1._2.equals(pair))

    val latestVals : mutable.Map[(String, B), (Color, Long)] = pairSeries.flatMap( _series => {

      val seriesKey = _series._1
      val seriesName : String = seriesKey._1
      val maybeLast : Option[B] = lastValFun(_series._2)

      maybeLast.map(lastVal => {
        if (!updates.contains(seriesKey))
          logger.error (s"No se encontro update para $seriesKey en ${updates.mkString(",")}")
        ((seriesName, lastVal), (COLORS(seriesName), updates(seriesKey)))
      } )
    })

    val sortedVals = latestVals.toSeq.sortBy(_._1._2)(sorting)

    var c = 0

    def hex(color: Color) = "#"+Integer.toHexString(color.getRGB()).substring(2);

    val font = new Font("Verdana", Font.BOLD, 12)

    if (getGraphics==null)
      return;

    assert (getGraphics != null)
    val fontMetrics = getGraphics.getFontMetrics(font)
    val charWidth = fontMetrics.stringWidth("X")
    val textWidth = getWidth/charWidth

    var htmlText = ""
    var displayText = ""
    sortedVals.map( _series => {

      val name = _series._1._1
      val lastVal = _series._1._2

      val color = _series._2._1
      val rgb = color.getRGB
      val age = new Date().getTime - _series._2._2

      //{hex(COLORS(name))}

      displayText += s"$name $lastVal ${pair.counter} - ${Duration(age, MILLISECONDS).toSeconds}s "

      val ht = if (c>1)
          s"""<html>- <font color="#66BBFF">$name</font>
             |<font color="#ffffff"> $lastVal ${pair.counter}</font>
             | - <font color="#ffffff">${Duration(age, MILLISECONDS).toSeconds}s </font>""".stripMargin
          else
            s"""<html><font color="#66BBFF">$name</font>
             |<font color="#ffffff"> $lastVal ${pair.counter}</font>
             | - <font color="#ffffff">${Duration(age, MILLISECONDS).toSeconds}s </font>""".stripMargin

      htmlText += ht
    })

    removeAll()

    val stripped = displayText //.take(textWidth)
    val label = new JLabel(htmlText, null, SwingConstants.LEFT)
    label.setFont(new Font("Verdana", Font.BOLD, 15))

    /*logger.trace(s"JMobar sortedVals: ${sortedVals.map(_._1).mkString(",")}. " +
      s"Width: ${getWidth}, " +
      s"Text width: $textWidth. " +
      s"Text stripped: $stripped. ") +
      s"Label width: ${label.getWidth}. "*/

    add(label)

  }
}

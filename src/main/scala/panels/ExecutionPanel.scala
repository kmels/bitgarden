package panels

import javax.swing.{JPanel, JScrollPane, JTable, JViewport}
import javax.swing.table.DefaultTableModel

import bgutils.GlobalConstants._
import bgutils.MemoryData.logger
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata._

import scala.collection.mutable.{ListBuffer}
import scala.collection.parallel.mutable.{ParSeq, ParMap}
import bgutils._
import MemoryData._
import UtilImplicits._

class ExecutionPanel(val e1: String, val e2: String, val cp: CurrencyPair) extends JPanel {
  val fees = GlobalConstants.FEE_SPEC

  val combo = (e1, e2, cp)

  this.setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.PAGE_AXIS));

  this.setBorder(javax.swing.BorderFactory.createTitledBorder(
    "Arbitrage"))

  val orderBookIndexes  = scala.collection.mutable.Map[(String,String),Int]()
  val executionTableIndexes  = scala.collection.mutable.Map[(String,String),Int]()

  private def create(x: String, y: String, cp: CurrencyPair): JPanel = {
    contain(x, y, cp)
    this
  }
  
  private def contain(x: String, y: String, cp: CurrencyPair): Unit = {
    val buy = "Buy " + x
    val sell = "Sell " + y
    val arbitrage = (buy,sell)

    logger.debug(s"Containing executions $buy and $sell")

    /* get the data */
    val bd = bookData((x,cp),(y,cp))

    /* add order books */
    val btContainer = new JScrollPane()
    val bt = bookJTable(bd, x, y, cp)
    btContainer.getViewport().add(bt)

    /* add book table's container to index */
    orderBookIndexes += ((arbitrage, this.getComponentCount))
    this.add(btContainer)
    
    /* add opportunities matrix */
    val otContainer = new JScrollPane()
    val ot = opportunitiesJTable(x, y, cp)
    otContainer.getViewport().add(ot)
    
    executionTableIndexes += ((arbitrage, this.getComponentCount))
    this.add(otContainer)
  }

  create(e1,e2,cp)

  /*private def recreate(exchange1: String, exchange2: String, cp: CurrencyPair): Unit = {
    container.removeAll()
    orderBookIndexes.clear()
    executionTableIndexes.clear()
    create(exchange1, exchange2, cp)
  }*/
    
  /* print the order books */
  def bookFormat(bd: BookData, x: String, y: String, cp: CurrencyPair, cb: Option[(Int,Int) => String => Unit] = None, maxrows: Int = 15):
    ParSeq[Array[Object]] = {

    val asks = bd._1.par
    val bids = bd._2.par

    /* for every line , format */
    val counterSym = cp.counter.getCurrencyCode
    val baseSym = cp.base.getCurrencyCode

    //logger.info(s"Buy ${x} - Sell ${y} ${cp}. ExecutionPanel. Formatting ${asks.zip(bids).take(maxrows).size} rows")

    asks.zip(bids).take(maxrows).zipWithIndex.map( l => {
      val (line, i) = l
      val askPrice = line._1.getLimitPrice + " " + counterSym
      val bidPrice = line._2.getLimitPrice + " " + counterSym
      val askVolum = line._1.getTradableAmount + " " + baseSym
      val bidVolum = line._2.getTradableAmount + " " + baseSym

      if (cb.isDefined) {
        val up = cb.get
        up(i, 0)(askPrice)
        up(i, 1)(askVolum)
        up(i, 2)(bidPrice)
        up(i, 3)(bidVolum)
      }

      Array[Object](askPrice,askVolum,bidPrice,bidVolum)
    })
  }

  /* order books as a JTable */ 
  def bookJTable(bd: BookData, x: String, y: String, cp: CurrencyPair, nrows: Int = 15): JTable = {
    val dm : Array[Array[Object]] = bookFormat(bd, x, y, cp, None, nrows).seq.toArray

    val model = new DefaultTableModel(dm, Array[Object](
      "Ask " + x,
      "Volume",
      "Bid "+y,
      "Volume"
    ))
    val table = new JTable(model)
    table
  }
  
  /* convert an opportunity matrix to a JTable */ 
  private def opportunitiesJTable(x: String, y: String, cp: CurrencyPair, nrows: Int = 5): JTable = {
    val HUNDRED = java.math.BigDecimal.valueOf(100)

    if (GlobalConstants.marketOpportunities.get(combo).isDefined) {
      val (opps, opsState) = GlobalConstants.marketOpportunities(combo)
      val model = new DefaultTableModel(opps.executionsModel, Array[Object](
        "Pips",
        "Amount",
        "Buy " + x,
        "Sell " + y,
        "Cost (+" + fees(x).multiply(HUNDRED) + "%)",
        "Revenue (-" + fees(y).multiply(HUNDRED) + "%)",
        "Profit"
      ))
      val table = new JTable(model)
      table
    } else {
      new JTable(new DefaultTableModel())
    }

  }

  def updateData(om: OpportunityMatrix): Option[(OrderSpec, OrderSpec)] = {
    if (!marketOpportunities.contains(combo))
      return None

    val buy = "Buy " + e1
    val sell = "Sell " + e2
    val arbitrage = (buy,sell)
    logger.trace(s"Actualizando execution panel: ${buy} - ${sell}")

    val booksTableContainer: JScrollPane =
      this.getComponent(
        orderBookIndexes(arbitrage)
      ).asInstanceOf[JScrollPane]

    val data = marketBooks.contains((e1,cp)) && marketBooks.contains((e2,cp))
    if (!data) {
      logger.trace(s"Buy $e1 - Sell $e2 ${cp}. Won't update panel. Reason: There is no data.")
      return None
    }

    val bd@(asks, bids) = bookData((e1,cp), (e2,cp))

    /* Update book data */
    val booksTable: JTable = booksTableContainer.getComponent(0).asInstanceOf[JViewport].getComponent(0).asInstanceOf[JTable]

    val books = bookFormat(bd, e1, e2, cp, Some((row,col) => v => {
      booksTable.setValueAt( v, row, col)
    }), maxrows = booksTable.getRowCount)

    /* Update executions table */
    val executionsTableContainer : JScrollPane = this.getComponent(
      executionTableIndexes(arbitrage)
    ).asInstanceOf[JScrollPane]
    
    val executionsTable : JTable = executionsTableContainer.getComponent(0).asInstanceOf[JViewport].getComponent(0).asInstanceOf[JTable]

    val executions = om.executionsModel
    val arbitrages = om.executables

    //update the executions table
    val model : DefaultTableModel = executionsTable.getModel().asInstanceOf[DefaultTableModel]
    model.setRowCount(executions.size)

    for ( row <- 0 to ( executions.size-1 ) ){
      for ( col <- 0 to ( executions(row).size-1 ) ) {
        executionsTable.setValueAt( executions(row)(col), row, col)    
      }
    }

    val deepestArb = arbitrages.lastOption
    deepestArb
  }
}

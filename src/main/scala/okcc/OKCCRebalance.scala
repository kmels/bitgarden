package okcc

import org.knowm.xchange.currency.{Currency, CurrencyPair}
import org.knowm.xchange.{ExchangeFactory, ExchangeSpecification}

class OKCCRebalance() {

  import org.knowm.xchange.okcoin._
  import org.knowm.xchange.service.polling.account._
  import org.knowm.xchange.service.polling.trade._
  import org.knowm.xchange.service.polling.marketdata._
  import org.knowm.xchange.dto.trade._
  import org.knowm.xchange.dto._

  val okccAPI = "f08cc006-87d8-4d5f-a86e-161e11d74f8a"
  val okccSEC = "F39715FBDF6C0D714EE624DD7D0BD142"

  val okccSpec: ExchangeSpecification = new ExchangeSpecification(classOf[OkCoinExchange])

  okccSpec.setSecretKey(okccSEC);
  okccSpec.setApiKey(okccAPI);

  val okcc = ExchangeFactory.INSTANCE.createExchange(okccSpec);
  val okccAccountService: PollingAccountService  = okcc.getPollingAccountService();
  val okccTradeAgent: PollingTradeService  = okcc.getPollingTradeService();
  val okccPoller: PollingMarketDataService = okcc.getPollingMarketDataService

  println("found " + okccAccountService.getAccountInfo.getWallets + " wallets")
  val okccAccount = okccAccountService.getAccountInfo.getWallet

  val okccBTC = okccAccount.getBalance(Currency.BTC)
  val okccCNY = okccAccount.getBalance(Currency.CNY)

  /* Checkpoint 1
   * ---
   *
   * 0.003 BTC
   * 5966.3244 CNY
   */

  /* Checkpoint 2
   * ---
   *
   * 1.033 BTC
   * 2988.5228 CNY
   */


  val okccPrices = okccPoller.getOrderBook(CurrencyPair.BTC_CNY)
  val BTC_CNY_value = okccPrices.getBids.get(0).getLimitPrice

  val okccBTC_CNY = okccBTC.getAvailable.multiply(BTC_CNY_value)

  val diff = okccCNY.getAvailable.subtract(okccBTC_CNY)

  println ("BTC: " + okccBTC)
  println ("CNY: " + okccCNY)

  val currencyPair = CurrencyPair.BTC_CNY

  val order = if (okccBTC_CNY.compareTo(okccCNY.getAvailable) < 0) {
    println("Sell CNY. Buy BTC")

    val okccPrices = okccPoller.getOrderBook(CurrencyPair.BTC_CNY)
    val BTC_CNY_value = okccPrices.getBids.get(0).getLimitPrice

    val amount = diff.divide(java.math.BigDecimal.valueOf(2d)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP)

    println(amount)

    val order: MarketOrder   = new MarketOrder(Order.OrderType.BID, amount, currencyPair);
    //val orderId = accountAgent.placeMarketOrder(order);
    //okccTradeAgent
    order
  } else {
    println("Sell BTC. Buy CNY")
    val cnyamount = diff.abs().divide(java.math.BigDecimal.valueOf(2d)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP)

    val btcamount = cnyamount.divide(BTC_CNY_value, 2, java.math.RoundingMode.HALF_UP)
    println(btcamount)

    val order: MarketOrder = new MarketOrder(Order.OrderType.ASK, btcamount, currencyPair);
    order
  }

  //okccTradeAgent.placeMarketOrder(order)
}



object ImplicitTypes {
  type Precision = java.math.BigDecimal
  
  implicit def numericPrecision: Numeric[Precision] = new Numeric[Precision]{
    override def compare(x: Precision, y: Precision): Int = x.compareTo(y)
    override def fromInt(x: Int): Precision = new java.math.BigDecimal(x)
    override def negate(x: Precision): Precision = x.negate()
    override def minus(x: Precision, y: Precision): Precision = x.subtract(y)
    override def plus(x: Precision, y: Precision) = x.add(y)
    override def times(x: Precision, y: Precision) = ???
    override def toDouble(x: Precision) = x.doubleValue
    override def toFloat(x: Precision) = x.floatValue
    override def toInt(x: Precision) = x.intValue
    override def toLong(x: Precision) = x.longValue
  }
}
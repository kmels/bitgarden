import bgutils.OpportunityMatrix
import bgutils.UtilImplicits._
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.Order.OrderType
import org.knowm.xchange.dto.marketdata.OrderBook
import org.knowm.xchange.dto.trade.LimitOrder
import org.scalatest.{Matchers, FlatSpec}
import java.util.Date
import org.scalatest._
import scala.collection.JavaConversions._
import bgutils.GlobalUtils._

import java.math.BigDecimal._
import java.math.RoundingMode

class TestGlobalUtils extends FlatSpec with Matchers {

  def lift(x: Double): Price = java.math.BigDecimal.valueOf(x)
  def liftTuple(t: (Double, Double)) = (lift(t._1), lift(t._2))

  "Cost of buying ETH/BTC" should "match " in {
    val pair = CurrencyPair.ETH_BTC
    val asks: List[LimitOrder] = List((0.01299000,121d),(0.01299989,118d), (0.01299997, 50d))
        .map(liftTuple)
        .map({case (price,volume) => new LimitOrder(OrderType.ASK, volume, pair, "id", null, price)})
    val bids: List[LimitOrder] = Nil
    val book = new OrderBook(new Date(), asks, bids)

    val buyFee = lift(0.002)

    println(costOfBuying(lift(1d), book, ZERO).getClass)
    println(lift(0.01299).getClass)
    costOfBuying(lift(1d), book, ZERO) should equal (new java.math.BigDecimal("0.012990"))
    costOfBuying(lift(1d), book, buyFee) should equal (new java.math.BigDecimal("0.013015980"))

    val TEN = new java.math.BigDecimal("10")
    costOfBuying(TEN, book, ZERO) should equal (costOfBuying(ONE, book, ZERO).multiply(TEN))
    costOfBuying(TEN, book, ZERO) should equal (new java.math.BigDecimal("0.12990"))
    costOfBuying(TEN, book, buyFee) should equal (new java.math.BigDecimal("0.13015980"))

    costOfBuying(new Precision("200"), book, ZERO) should equal (new java.math.BigDecimal("2.598781310"))
    costOfBuying(new Precision("200"), book, buyFee) should equal (new java.math.BigDecimal("2.603978872620"))

    costOfBuying(new Precision("121"), book, ZERO) should equal (new java.math.BigDecimal("1.571790"))
    costOfBuying(new Precision("121"), book, buyFee) should equal (new java.math.BigDecimal("1.574933580"))
  }
}

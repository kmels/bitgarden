import java.text.DecimalFormat

import bgutils.{UnprofitableOMException, OpportunityMatrix, UtilImplicits}
import UtilImplicits._
import org.knowm.xchange.currency.CurrencyPair

import org.scalatest._
import scala.collection.parallel.mutable.ParSeq
import java.math.BigDecimal._
import java.math.RoundingMode

class TestOpportunityMatrix extends FlatSpec with Matchers {
  def priced(d : Double) = java.math.BigDecimal.valueOf(d)

  def makeAccDepthData(locs : MatrixOf[Double]): AccDepthData = {
    val askPrices : ParSeq[Price] = locs.map(r => priced(r(0))).par
    val askVolumes : ParSeq[Price] = locs.map(r => priced(r(1))).par
    val bidPrices : ParSeq[Price] = locs.map(r => priced(r(2))).par
    val bidVolumes : ParSeq[Price] = locs.map(r => priced(r(3))).par

    new AccDepthData((askPrices, askVolumes, bidPrices, bidVolumes))
  }

  val scenario1 = makeAccDepthData(Array(
    //ask, bid
    Array(353.33, 0.22, 355.99, 0.01),
    Array(357.0, 15.0, 355.03, 0.51),
    Array(358.0, 15.0, 355.02, 9.51),
    Array(359.0, 15.0, 355.01, 9.51)
  ))

  def lift(x: Double): Precision = java.math.BigDecimal.valueOf(x)
  def liftTuple(t: (Double, Double)) = (lift(t._1), lift(t._2))

  "Opportunity matrix" should "have the bought and sold the same volume" in {

    val opportunityMatrix = new OpportunityMatrix(scenario2, ZERO, ZERO, new CurrencyPair("A","B"))
    val arbs: (Seq[(OrderSpec,OrderSpec)]) = opportunityMatrix.executables

    val buyOrders = arbs.map(_._1)
    val sellOrders = arbs.map(_._2)

    val bought = buyOrders.map({case (price,volume) => volume}).sum
    val sold = buyOrders.map({case (price,volume) => volume}).sum

    (bought compareTo sold) should be (0)
  }

  //BTC/USD
  val scenario2 = makeAccDepthData(Array(
    //ask, bid
    Array(364.99, 9.54, 366.07, 0.01),
    Array(365, 10.6, 366.06, 4.51),
    Array(365.68, 11.6, 366.05, 11.34),
    Array(366.17, 25.28, 366.03, 18.13),
    Array(366.18, 34.18, 366.02, 30.13)
  ))

  //without fees
  val _scenario2SellOrders: Seq[(Double, Double)] = Seq(
                                 (366.07, 0.01),
                                 (366.06, 4.51),
                                 (366.05, 5.02),
                                 (366.05, 6.32),
                                 (366.03, 4.28),
                                 (366.03, 11.6)
                               )

  //without fees
  val _scenario2BuyOrders: Seq[(Double, Double)] = Seq(
                                (364.99, 0.01),
                                (364.99, 4.51),
                                (364.99, 5.02),
                                (365d, 6.32),
                                (365d, 4.28),
                                (365.68, 11.6)
                              )

  "Scenario 2 BTC/USD" should "have executable orders without fees."  in {
    val scenario2BuyOrders: Seq[(Price, Volume)] = _scenario2BuyOrders.map( t => (lift(t._1), lift(t._2)))
    val scenario2SellOrders: Seq[(Price, Volume)] = _scenario2SellOrders.map({case t => (lift(t._1), lift(t._2))})

    val om = new OpportunityMatrix(scenario2, ZERO, ZERO, new CurrencyPair("BTC", "USD"))
    val arbs : Seq[(OrderSpec,OrderSpec)] = om.executables

    (arbs.unzip._1) should be (scenario2BuyOrders)
    (arbs.unzip._2) should be (scenario2SellOrders)
  }

  "Scenario 2" should "have no orders with fees."  in {
    try {
      val om = new OpportunityMatrix(scenario2, lift(0.001), lift(0.002), new CurrencyPair("BTC", "USD"))
      val arbs : Seq[(OrderSpec,OrderSpec)] = om.executables
      (arbs.unzip._1) should be (Nil)
      (arbs.unzip._2) should be (Nil)
    } catch {
      case _ : Exception => { /* expected */ }
    }
  }

  "Scenario 2" should "be profitable withgout fees" in {
    val om = new OpportunityMatrix(scenario2, ZERO, ZERO, new CurrencyPair("BTC", "USD"))
    val arbs = om.executables
    val (buyOrders, sellOrders) = arbs.unzip
    val revenue = sellOrders.map(o => o._1.multiply(o._2)).sum
    val cost = buyOrders.map(o => o._1.multiply(o._2)).sum
    (revenue compareTo cost) should be (1)
    (revenue subtract cost) should be (lift(25.2621))

    val revenues = om.executionsModel.map(r => r(5))
    //val expectedRevenues = Array("3.66", "1650.93", "1837.57", "2313.43", "1566.60", "4245.94").map(_ + " USD")
    val expectedRevenues = Array("3.6607", "1650.9306", "1837.5710", "2313.4360", "1566.6084", "4245.9480").map(_ + " USD")
    (revenues.size) should equal (expectedRevenues.size)
    (revenues.size) should equal (sellOrders.size)
    for (r <- expectedRevenues.zipWithIndex){
      val expected = r._1
      val idx = r._2
      val actual   = revenues(idx)
      (actual) should be (expected)
    }

    val costs = om.executionsModel.map(r => r(4))
    //val expectedCosts = Array("3.65", "1646.11", "1832.25", "2306.80", "1562.20", "4241.89").map(_ + " USD")
    val expectedCosts = Array("3.6499", "1646.1049", "1832.2498", "2306.8000", "1562.2000", "4241.8880").map(_ + " USD")
    (costs.size) should equal (expectedCosts.size)
    (costs.size) should equal (buyOrders.size)
    for (r <- costs.zipWithIndex){
      val actual = r._1
      val idx = r._2
      val expected   = expectedCosts(idx)
      (actual) should be (expected)
    }
  }

  "Scenario 2" should "not be profitable with fees" in {
    try {
      val om = new OpportunityMatrix(scenario2, lift(0.001), lift(0.002), new CurrencyPair("BTC", "USD"))
      val arbs = om.executables
      om.poc should equal ("0.00/0.00 USD")
      om.pocPercentage should equal("0.00 %")
      om.executionsModel.size should equal(0)
    } catch {
      case _ : Exception => { /* expected */ }
    }
  }

  "Scenario 2" should "have following pips" in {
    val om = new OpportunityMatrix(scenario2, ZERO, ZERO, new CurrencyPair("BTC", "USD"))
    val pips = om.executionsModel.map(row => Integer.parseInt(row(0).toString))
    val expectedPips: Array[Int] = Array(10800, 10700, 10600, 10500, 10300, 3500)

    for (r <- pips.zipWithIndex){
      val actual = r._1
      val idx = r._2
      val expected   = expectedPips(idx)
      (actual) should be (expected)
    }
  }


  //LTC/EUR
  val scenario3 = makeAccDepthData(Array(
    //ask, bid
    Array(2.72, 255.431, 2.75, 2.42483462),
    Array(2.73998, 300, 2.746, 3.009),
    Array(2.73999, 96.949, 2.741, 24.79371791),
    Array(2.74, 156, 2.732, 0.274987),
    Array(2.74777, 24.807, 2.73, 3.009)
  ))

  "Scenario 3 LTC/EUR" should "match orders, revenues and pips." in {
    println("Scenario 3.")
    val om = new OpportunityMatrix(scenario3, lift(0.001), lift(0.002), new CurrencyPair("LTC", "EUR"))
    val arbs : Seq[(OrderSpec,OrderSpec)] = om.executables

    val buyOrders: Seq[(Price, Volume)] = Seq((2.72, 2.42483462), (2.72, 3.009),
      (2.72, 24.79371791), (2.72, 0.274987), (2.72, 3.009)).map(liftTuple)
    (arbs.unzip._1) should be (buyOrders)

    val sellOrders: Seq[(Price, Volume)] = Seq((2.75, 2.42483462), (2.746, 3.009),
      (2.741, 24.79371791), (2.732, 0.274987), (2.73, 3.009)).map(liftTuple)
    (arbs.unzip._2) should be (sellOrders)

    val revenues = om.executionsModel.map(r => r(5))

    //val expectedRevenues = Array("6.65", "8.24", "67.82").map(_ + " EUR")
    val expectedRevenues = Array("6.6550", "8.2462", "67.8237", "0.7498", "8.1981").map(_ + " EUR")
    (revenues.size) should equal (sellOrders.size)
    (revenues.size) should equal (expectedRevenues.size)

    for (r <- expectedRevenues.zipWithIndex){
      val expected = r._1
      val idx = r._2
      val actual   = revenues(idx)
      (actual) should be (expected)
    }

    val costs = om.executionsModel.map(r => r(4))
    val expectedCosts = Array("6.6021", "8.1927", "67.5064", "0.7487", "8.1927").map(_ + " EUR")
    (costs.size) should equal (buyOrders.size)
    for (r <- costs.zipWithIndex){
      val actual = r._1
      val idx = r._2
      val expected   = expectedCosts(idx)
      (actual) should be (expected)
    }

    val expectedProfits = Array("0.04", "0.04", "0.31").map(_ + " EUR")
    val pips = om.executionsModel.map(row => Integer.parseInt(row(0).toString))
    val expectedPips: Array[Int] = Array(3,3,2,1,1)
    //(pips.toList) should equal (expectedPips.toList)
  }

  "Scenario 3 LTC/EUR" should "have following pips." in {
    val om = new OpportunityMatrix(scenario3, lift(0.001), lift(0.002), new CurrencyPair("LTC", "EUR"))
    val pips = om.executionsModel.map(row => Integer.parseInt(row(0).toString))
    val expectedPips: Array[Int] = Array(3,3,2,1,1)
    //(pips.toList) should equal (expectedPips.toList)
  }

  "OM with executable" should "have greather-than-0 profit" in {

    println("Greater than 0 profit")

    val scenario = makeAccDepthData(Array(
      //ask, bid
      Array(419.21, 7.435, 421.242, 0.011044)
    ))
    val om = new OpportunityMatrix((scenario), lift(0.0025), lift(0.002), CurrencyPair.BTC_USD)
    om.accumulatedProfit.compareTo(ZERO) should equal (1)
  }

  "OM with no executables" should "throw an unprofitable exception" in {

    println("No executables => unprofitable exception")

    val scenario = makeAccDepthData(Array(
      //ask, bid ETH/USD
      Array(6.50, 647.305, 6.52, 0.46251767),
      Array(6.52220, 50.0, 6.51576229, 0.100201),
      Array(6.54318, 800.0, 6.48246354, 0.110804)
    ))

    try {
      val om = new OpportunityMatrix((scenario), lift(0.0025), lift(0.002), new CurrencyPair("ETH","USD"))
    } catch {
      case e : UnprofitableOMException => {}
    }

    val scenario2 = makeAccDepthData(Array(
      //ask, bid LTC/BTC
      Array(0.00784451,0.00001002, 0.00788,260.39785936),
      Array(0.00788375,18.07697213, 0.00787,2796.90817781),
      Array(0.00788376,18.09000111, 0.00786,3119.28480971),
      Array(0.00789971,45.34877306, 0.00785,3228.90291334)
    ))

    try {
      val om = new OpportunityMatrix((scenario2), lift(0.002), lift(0.002), CurrencyPair.LTC_BTC)
    } catch {
      case e : UnprofitableOMException => {}
    }

    println("No profit, no executables")
    val scenario3 = makeAccDepthData(Array(
      //ask, bid BTC/USD
      Array(432.27, 6.11, 434.222, 0.01110159), // low volume
      Array(432.53, 6.51200000, 434.211, 0.01110058),
      Array(432.56, 20.00000000, 434.197, 0.01109963)
    ))

    try {
      val om = new OpportunityMatrix((scenario3), lift(0.0025), lift(0.002), new CurrencyPair("BTC","USD"))
    } catch {
      case e : UnprofitableOMException => {}
    }
  }

  "ETH/USD" should "have sufficient pip 0's" in {

    println ("ETH/USD pips")

    val scenario = makeAccDepthData(Array(
      //ask, bid ETH/USD
      Array(6.39398,0.888,6.45754999,0.00002192), //low volume
      Array(6.44999,58.365,6.42356289,0.10032861),
      Array(6.45000,78.365,6.42356288,0.21113261),
      Array(6.46180,84.616,6.38957577,0.33366561)
    ))

    //Buy fee: 0.001. Sell fee: 0.002 (ETH/USD)
    try {
      val om = new OpportunityMatrix((scenario), lift(0.001), lift(0.002), new CurrencyPair("ETH", "USD"))
    } catch {
      case e: UnprofitableOMException => {}
    }

    val scenario2 = makeAccDepthData(Array(
      //ask, bid ETH/USD
      Array(6.39398,0.888,6.45754999, 500d), //good volume
      Array(6.44999,58.365,6.42356289,0.10032861),
      Array(6.45000,78.365,6.42356288,0.21113261),
      Array(6.46180,84.616,6.38957577,0.33366561)
    ))

    //Buy fee: 0.001. Sell fee: 0.002 (ETH/USD)
    val om2 = new OpportunityMatrix((scenario2), lift(0.001), lift(0.002), new CurrencyPair("ETH","USD"))
    om2.executionsModel.nonEmpty should be (true)
  }

  /*
   *
   *
   *
   [info] 3/01 19:44:50.125 [#updateArbitrage:223] ERROR - Error procesando arbitrage (Bitstamp,Btc-e,BTC/USD). Comprador: Bitstamp, LimitOrder [limitPrice=432.27, Order [type=ASK, tradableAmount=6.11000000, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=432.53, Order [type=ASK, tradableAmount=6.51200000, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=432.56, Order [type=ASK, tradableAmount=20.00000000, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=432.58, Order [type=ASK, tradableAmount=7.29900000, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=432.83, Order [type=ASK, tradableAmount=16.70890000, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]]. Vendedor: Btc-e, LimitOrder [limitPrice=434.222, Order [type=BID, tradableAmount=0.01110159, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=434.211, Order [type=BID, tradableAmount=0.01110058, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=434.197, Order [type=BID, tradableAmount=0.01109963, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=434.169, Order [type=BID, tradableAmount=0.01109109, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=434.156, Order [type=BID, tradableAmount=0.01110928, averagePrice=0, currencyPair=BTC/USD, id=, timestamp=null, status=PENDING_NEW]]
[info] bgutils.UnprofitableOMException: KEIN OM - ACPIPS: 0. PROFIT: 0 USD. EXECUTABLES: 1 (((432.27,0.01110159),(434.222,0.01110159)))ROI: (0.0000 %)
* 
   *
   * 
  TODO: TEST
  [info] 2/28 23:06:09.066 [#updateArbitrage:223] ERROR - Error procesando arbitrage (Kraken,Poloniex,ETH/BTC). Comprador: Kraken, LimitOrder [limitPrice=0.015050, Order [type=ASK, tradableAmount=0.001, averagePrice=0, currencyPair=ETH/BTC, id=, timestamp=Sun Feb 28 23:05:48 CST 2016, status=PENDING_NEW]],LimitOrder [limitPrice=0.015060, Order [type=ASK, tradableAmount=2.023, averagePrice=0, currencyPair=ETH/BTC, id=, timestamp=Sun Feb 28 23:05:49 CST 2016, status=PENDING_NEW]],LimitOrder [limitPrice=0.015070, Order [type=ASK, tradableAmount=1.660, averagePrice=0, currencyPair=ETH/BTC, id=, timestamp=Sun Feb 28 23:05:40 CST 2016, status=PENDING_NEW]],
  LimitOrder [limitPrice=0.015090, Order [type=ASK, tradableAmount=75.223, averagePrice=0, currencyPair=ETH/BTC, id=, timestamp=Sun Feb 28 23:05:39 CST 2016, status=PENDING_NEW]],LimitOrder [limitPrice=0.015100, Order [type=ASK, tradableAmount=2.023, averagePrice=0, currencyPair=ETH/BTC, id=, timestamp=Sun Feb 28 23:05:25 CST 2016, status=PENDING_NEW]]. Vendedor: Poloniex, LimitOrder [limitPrice=0.01510000, Order [type=BID, tradableAmount=3726.6949106, averagePrice=0, currencyPair=ETH/BTC, id=null, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=0.01504002, Order [type=BID, tradableAmount=35.03910367, averagePrice=0, currencyPair=ETH/BTC, id=null, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=0.01504001, Order [type=BID, tradableAmount=0.05319145, averagePrice=0, currencyPair=ETH/BTC, id=null, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=0.01504000, Order [type=BID, tradableAmount=0.90114283, averagePrice=0, currencyPair=ETH/BTC, id=null, timestamp=null, status=PENDING_NEW]],LimitOrder [limitPrice=0.01502001, Order [type=BID, tradableAmount=13.28899237, averagePrice=0, currencyPair=ETH/BTC, id=null, timestamp=null, status=PENDING_NEW]]
[info] java.lang.Exception: No puede ser que hayan 1 ejecutables  en un modelo vacio ... ACPIPS: 0. PROFIT: 0 BTC. EXECUTABLES: 1 (((0.015050,0.001),(0.01510000,0.001)))ROI: (0.00000000 %) ... Asks: (0.015050,0.001),(0.015060,2.024),(0.015070,3.684),(0.015090,78.907),(0.015100,80.930) ... Bids: (0.01510000,3726.6949106),(0.01504002,3761.73401427),(0.01504001,3761.78720572),(0.01504000,3762.68834855),(0.01502001,3775.97734092) ... Buy fee: 0.001. Sell fee: 0.002 (ETH/BTC)
   */

}

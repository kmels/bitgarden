import time
import json
import requests
import datetime

_api_token = "b56514c86c9324f924252ce33f038ff2"

def get_listings():
    url = "https://1broker.com/api/v1/market/list.php?token={0}&pretty=0".format(_api_token)
    return json.loads(requests.get(url).text)['response']

def get_bars(_symbol, _resolution, _from = 1, _to = int(time.time())):
    """_resolution: 60 or 3600 or 86400""" 
    url="https://1broker.com/api/v1/market/get_bars.php"
    url += "?symbol={0}&from={1}&to={2}&resolution={3}&token={4}&pretty={5}".format(_symbol, _from, _to, _resolution, _api_token, str(0))
    print url
    _bars = json.loads(requests.get(url).text)['response']
    bars = []
    for _bar in _bars:
        bars.append({
            'Timestamp': _bar['time'],
            'Datetime' : str(datetime.datetime.utcfromtimestamp(int(_bar['time']))),
            'High'     : _bar['h'],
            'Low'      : _bar['l'],
            'Close'    : _bar['c'],
            'Open'     : _bar['o']
        })
    return bars

from elasticsearch import Elasticsearch
es = Elasticsearch(['http://campesino:morral-tipico@eratostenes'])
def save_one(category, _sym, _res, bar):
    res = _res/60
    typ = _sym.lower() + '-' + str(res)+'m'
    key = bar['Timestamp']
    es.index(index=category.lower(), doc_type=typ, id=key, body=bar)
    
def last_one(_cat, _sym, _res):
    print 'last'
    _res = _res/60
    typ = _sym.lower() + '-' + str(_res)+'m'
    try:
        hit = es.search(index=_cat.lower(), doc_type=typ, sort=['Timestamp:desc'], size=1, body={"query": {"match_all": {}}})
        
        print hit
        if len(hit['hits']['hits']) > 0:
            return hit['hits']['hits'][0]['_source']
    except:
        return None
    return None

def create(*resolutions):
    for res in resolutions:
        supported = (res==60 or res==60**2 or res==60**2*24)
        if supported:
            print res, 'supported'
        else:
            print res, 'find lower timeframe support'

import logging
LOG_FILENAME = '1b.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)

def download_and_save(_cat, _sym, _res):
    _response = True
    _from = 1
    _to = int(time.time())
    _lastone = last_one(_cat, _sym, _res)

    if _lastone is not None:
        logging.info('Last: ' + _lastone['Datetime'])

    if _response:
        _bars  = get_bars(_sym, _res)
        _first = _bars[0]['Datetime']
        _last  = _bars[-1]['Datetime']

        logging.info('Processing {3}s response, |bars| = {0}. {1} - {2}'.format(str(len(_bars)), _bars[-1]['Datetime'], _bars[0]['Datetime'], str(_res)))        

        for _bar in _bars:
            logging.debug('Inserted bar ' + _bar['Datetime'])
            
            if _lastone is None or _bar['Timestamp'] > _lastone['Timestamp']:
                print 'new: ', _bar['Datetime']
                save_one(_cat, _sym, _res, _bar)
                _lastone = _bar
            else:
                print 'old', _bar['Datetime']
            
        _to = _bars[0]
        there_is_response = len(_bars) > 0

for listing in get_listings():
    _cat = listing['category']
    _sym = listing['symbol']
    download_and_save(_cat, _sym, 60)       #1m
    download_and_save(_cat, _sym, 60**2)    #1h
    download_and_save(_cat, _sym, 60**2*24) #1d

#bars = get_bars('EURUSD', 3600, 24*3)
#listings = get_market_listings()
#print len(bars), ' bars'

#listings = get_market_listings()
#for listing in listings:
#    candles = make_candles(60) #, 60*5, 60*15, 60*30, 60**2, 60**2*2, 60**2*4, 60**2*24, 60**2*24*7)
    #print 'Saving ', listing['category'], listing['name'], listing['symbol'], listing['type']

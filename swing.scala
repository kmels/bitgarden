import java.awt.{BorderLayout, GridLayout}
import java.lang.reflect.InvocationTargetException
import java.util.{Date, Timer, TimerTask}
import javax.swing._

import bgutils.UtilImplicits._
import bgutils._
import org.knowm.xchange._
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata._
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService
import org.apache.logging.log4j.LogManager
import panels._
import types.BufferedTimeSeries

import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.duration._

object Hi {
  import GlobalConstants._

    /* initial settings */
  //var currencyPair : () => CurrencyPair = () => shuffle(currencyListings.keys.toSeq).head
  var currencyPair : () => CurrencyPair = () => shuffle(currencyListings.filter(_._2.size>1).keys.toSeq).head
  val shuffledExchanges= shuffle(currencyListings(currencyPair()).keys.toSeq)

  var exchange1: () => String = () => shuffledExchanges.head
  var exchange2: () => String = () => shuffledExchanges.last

  val logger = LogManager.getLogger();

  System.setProperty("logback.configurationFile", "/home/campesino/github/BitGarden/logback.xml");

  import org.knowm.xchange.service.streaming.StreamingExchangeService
  type Streamer = StreamingExchangeService
  type DataPoller = Either[PollingMarketDataService, StreamingExchangeService]

  val frame: JFrame = new JFrame("XChart")

  assert ( exchanges.forall(exchange=> FEE_SPEC(exchange).isInstanceOf[java.math.BigDecimal] ))

  val priceChart = new PriceChart(15 minutes)

  val jmobarPrices : JMobar[BufferedTimeSeries[Trade], Price] = {
    val lastPrice : BufferedTimeSeries[Trade] => Option[Price]= ts => ts.lastOption.map(_.getPrice)

    new JMobar[BufferedTimeSeries[Trade], Price](
      series = marketTrades,
      updates = marketTradeUpdates,
      lastValFun = lastPrice,
      sorting = implicitly[Ordering[Price]]
    )
  }

  val jmobarBooks : JMobar[OrderBook, (Price,Price)]= {
    val lastOrderBook : OrderBook => Option[OrderBook] = o => Option(o)
    val sortingPicker : OrderBook => OrderBook = o => o
    val spread: OrderBook => Option[(Price, Price)] = o => if (o.getAsks.isEmpty || o.getBids.isEmpty) None
    else Option((o.getAsks.get(0).getLimitPrice, o.getBids.get(0).getLimitPrice))

    new JMobar(
     series = marketBooks,
     updates = marketBookUpdates,
     lastValFun = spread,
     sorting = new Ordering[(Price,Price)] {
       override def compare(spread1: (Price,Price), spread2: (Price,Price)): Int = {
         val spreadX = spread1._1.subtract(spread1._2)
         val spreadY = spread2._1.subtract(spread2._2)
         spreadX.compareTo(spreadY)
       }
     }
   )
  }

  val opsPanel = new OpportunityPanel(Option(onCurrencyPairSelection))

  val acsPanel = new AccountsPanel()

  def createPanels() : ListBuffer[(java.awt.Component, String)] = {

    val panels = ListBuffer[(java.awt.Component, String)]()

    /* Status */
    val _priceChart = priceChart.create( currencyPair() )
    val _accountsPanel = acsPanel.create( currencyPair() )

    //create with colors assigned
    jmobarPrices.create(currencyPair())
    jmobarBooks.create(currencyPair())

    val mobars: JPanel = new JPanel()
    mobars.setLayout(new GridLayout(2,1))
    mobars.add(jmobarPrices)
    mobars.add(jmobarBooks)

    panels += ((mobars, BorderLayout.PAGE_START))

    /* Prices */
    val main = new JPanel()
    val mainLeft = new JPanel()

    panels += ((main, BorderLayout.CENTER))

    main.setLayout(new GridLayout(1,2))
    mainLeft.setLayout(new GridLayout(2,1))

    /* Buy X Sell Y */
    mainLeft.add(_priceChart)
    mainLeft.add(_accountsPanel)
    main.add(mainLeft)
    main.add(opsPanel)

    return panels
  }

  def getExchangeBook(exchangeName: String, exchange: Exchange, pair: CurrencyPair): Option[OrderBook] = currencyListing(exchangeName)(pair).map( listing => {
    logger.info(s"Descargando libros: $exchangeName" )

    val _exchange = ExchangeFactory.INSTANCE.createExchange(exchange.getDefaultExchangeSpecification)
    val poller = _exchange.getPollingMarketDataService
    val before = new Date().getTime
    val limitBook: OrderBook = if (listing._2) poller.getOrderBook(listing._1)
                               else CurrencyPriceListing.invert(poller.getOrderBook(listing._1))
    val now = new Date().getTime

    val ask = limitBook.getAsks().get(0);    val bid = limitBook.getBids().get(0)
    logger.trace(s"En ${now-before}ms libros iniciales de ${exchange.getClass.getSimpleName}, ${pair}.\t\task : ${ask.getTradableAmount} ${pair.base} @ ${ask.getLimitPrice} ${pair.counter} \tbid : ${bid.getTradableAmount} ${pair.base} @ ${bid.getLimitPrice} ${pair.counter} " )

    limitBook
  })

  def recreatePanels(): Unit = {
    logger.info("RECREATE PANELS")
    priceChart.recreate(currencyPair())
    acsPanel.recreate(currencyPair())
  }

  def onExchange1Selection(_exchange1 : String)(repaint: Boolean): Unit = {
    exchange1 = () => _exchange1

    if (repaint)
      recreatePanels()
  }

  def onExchange2Selection(_exchange2 : String)(repaint: Boolean): Unit = {
    exchange2 = () => _exchange2

    if (repaint)
      recreatePanels()
  }

  def onCurrencyPairSelection(newCurrencyListing: CurrencyPair)(repaint: Boolean): Unit = {
    logger.info(s"Updating currency pair, repaint = $repaint")
    currencyPair = () => newCurrencyListing

    if (repaint)
      recreatePanels()
  }

  def startUI(): Unit = {
    try {
      javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
        override def run() {
          frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          //frame.setLayout(new java.awt.GridLayout(2,2))

          frame.setLayout(new BorderLayout())

          val menus = new MenuBar(currencyListings.mapValues(k => k.keys.toSeq)
            , onExchange1Selection
            , onExchange2Selection
            , onCurrencyPairSelection
            , currencyPair
          )

          frame.setJMenuBar(menus)

          /* Add each of the panels*/
          createPanels().foreach(x => {
            frame.add(x._1, x._2)
          })

          /* Set icon image */
          val image = javax.imageio.ImageIO.read(new java.io.File("icon.png"))
          frame.setIconImage(image)

          /* Open the window. */
          frame.pack();
          frame.setVisible(true);
        }
      })
    } catch {
      case e: InvocationTargetException => logger.error("Invocation error", e.getTargetException)
    }
  }

  def updatePriceChart(tick: Int): Unit = {
    val before = new java.util.Date().getTime

    val pair = currencyPair()
    val pairs = currencyListings(pair)
    pairs.keys.foreach{ exchangeName => {
      if (marketTrades.contains((exchangeName,pair)))
        priceChart.updatePrices((exchangeName, pair))
    }}

    frame.validate()
    frame.repaint()
    val after = new java.util.Date().getTime
    logger.debug(s"Se pintaron los precios en ${(after-before)}ms (tick "+tick+")")
  }

  def updateMobars(tick: Int): Unit = {
    val before = new Date().getTime
    jmobarPrices.update(currencyPair())
    jmobarBooks.update(currencyPair())
    frame.validate()
    frame.repaint()
    val after = new Date().getTime
  }

  def updateDepthCharts(tick: Int): Unit = {
    val before = new java.util.Date().getTime
    frame.validate()
    frame.repaint()
    val after = new Date().getTime
    logger.debug(s"Se pintaron los libros en ${(after-before)}ms (tick "+tick+")")
  }

  def updateAccountsPanel(tick: Int): Unit = {
    val before = new java.util.Date().getTime

    currencies.foreach(c => {
      exchanges.foreach(e => {
        val balance = accountBalances.get((e,c))
        if (balance.isDefined) {
          logger.trace(s"$e $c. Updating balance = ${balance.get}")
          acsPanel.updateBalances(e, c, balance.get)
        }
      })
    })

    frame.validate()
    frame.repaint()
    val after = new Date().getTime
    logger.debug(s"Se pintaron los balances en ${(after-before)}ms (tick "+tick+")")
  }

  class StatusUpdate(val s: String)

  class PriceLoop(tick: Int = 1) extends SwingWorker[Unit, StatusUpdate]{

    override def doInBackground(): Unit = {
      logger.trace(s"Haciendo updatePriceData(${tick})")
      MemoryData.updatePriceData(priceChart, currencyPair(), tick)
    }

    override def done(): Unit = {
      logger.trace(s"Termino updatePriceData(). Tick ${tick}")
      updatePriceChart(tick)

      val timer = new Timer()

      timer.schedule(new TimerTask(){
        override def run(): Unit = {
          logger.trace("Nuevo PriceLoop")
          (new PriceLoop(tick + 1)).execute()
        }
      }, Duration(5, SECONDS).toMillis)
    }
  }

  class DepthLoop(tick: Int = 1) extends SwingWorker[Unit, StatusUpdate]{
    val creation = new Date().getTime

    override def doInBackground(): Unit = {
      logger.trace(s"DepthLoop: Haciendo updateDepthData(${tick})")
      val now = new Date().getTime
      MemoryData.updateDepthData(tick)
      logger.trace(s"DepthLoop: Termino updateDepthData(${tick}) en ${new Date().getTime - now}ms")
    }

    override def done(): Unit = {
      logger.trace(s"DepthLoop: Haciendo DepthLoop $tick")
      //new Thread(new DepthLoop(tick + 1)).run()
      logger.trace(s"DepthLoop: Termino updateDepthData(${tick}) en ${new Date().getTime - creation}ms")
      updateDepthCharts(tick)
      currencyListings.keys.foreach(cp => opsPanel.updateOpps(cp, tick))

      val timer = new Timer()

      timer.schedule(new TimerTask(){
        override def run(): Unit = {

          (new DepthLoop(tick + 1)).execute()
        }
      }, 1000)

      //new Thread(new DepthLoop(tick + 1)).run()
    }
  }

  class MobarLoop(tick: Int = 1) extends SwingWorker[Unit, Any]{
    override def doInBackground(): Unit = {
      updateMobars(tick)
    }

    override def done(): Unit = {
      updateMobars(tick)
      (new MobarLoop(tick + 1)).execute()
    }
  }

  class BalanceLoop(tick: Int = 1) extends SwingWorker[Unit, StatusUpdate]{

    final val f = Duration(6, SECONDS)

    override def doInBackground(): Unit = {
      logger.debug(s"Haciendo balanceLoop(${tick})")
      MemoryData.updateAccountBalances(acsPanel, tick, currencyPair())
    }

    override def done(): Unit = {
      logger.trace(s"Termino balanceLoop(). Tick ${tick}")
      updateAccountsPanel(tick)

      val timer = new Timer()

      timer.schedule(new TimerTask(){
        override def run(): Unit = {
          logger.trace("Nuevo BalanceLoop")
          (new BalanceLoop(tick + 1)).execute()
        }
      }, f.toMillis)

    }
  }

  def main(args: Array[String]) = {
    GlobalUtils.soundPlayer ! "/home/campesino/bitgarden/src/main/resources/sounds/b52takeoff.wav"

    //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel")
    //UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel")
    //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel")
    //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel")

    val before= new Date().getTime
    startUI()
    val after = new Date().getTime
    logger.info(s"Se hizo la GUI en ${(after - before)}ms. Calendarizando actualizaciones.")

    (new BalanceLoop).execute()
    (new MobarLoop).execute()
    //new Thread(new DepthLoop).run()
    (new DepthLoop).execute()
    (new PriceLoop).execute()
  }


}


from datetime import *

import urllib2 
import zlib
import json

### Make the request, and return a dictionary of *unparsed* candles.
### Each candle indexed by timeframe in minutes
def cryptowatch_request(exchange, pair):
    uri = 'https://cryptowat.ch/%s/%s.json' % (exchange, pair)
    req = urllib2.Request(uri)
    resp = zlib.decompress(urllib2.urlopen(req).read(), 16+zlib.MAX_WBITS)
    _json = json.loads(resp)
    candles = {}
    
    for k in _json: 
        candles[int(k)/60] = _json[k][:-1] #index by minutes, only finished 
    return candles

def sample_one(candle):
    candle = candle.split(' ')
    return {
        'Datetime': str(datetime.utcfromtimestamp(int(candle[0]))),
        'Timestamp': candle[0],
        'Open': candle[1],
        'High': candle[2],
        'Low':  candle[3],
        'Close': candle[4]
    }

markets = {
    'kraken': ['etheur', 'ethbtc', 'ethusd', 'btceur', 'btcusd', 'btcjpy'],
    'poloniex': ['ethbtc'],
    'bitmex': ['eth-weekly-futures','xbt-daily-futures','xbu-weekly-futures'],
    'bitfinex': ['btcusd','ethusd','ethbtc'],
    'btce': ['btcusd'],
    'cexio': ['btcusd', 'btceur'],
    'bitflyer': ['btcjpy'],
    'quoine': ['btcjpy', 'btcsgd','ethjpy','ethbtc','ethusd'],
    'mexbt': ['btcmxn'],
    'okcoin': ['btcusd','btccny','btcusd-weekly-futures', 'btcusd-biweekly-futures', 'btcusd-quarterly-futures'],
}

from elasticsearch import Elasticsearch
es = Elasticsearch(['http://campesino:morral-tipico@eratostenes'])
def save_one(candle, exchange, pair, res):
    typ = pair + '-' + str(res)+'m'
    key = candle['Timestamp']    
    es.index(index=exchange, doc_type=typ, id=key, body=candle)

def last_one(exchange, pair, res):
    typ = pair + '-' + str(res)+'m'
    try:
        hit = es.search(index=exchange, doc_type=typ, sort=['Timestamp:desc'], size=1, body={"query": {"match_all": {}}})
        if len(hit['hits']['hits']) > 0:
            return hit['hits']['hits'][0]['_source']
    except:
        return None
    return None

import logging
LOG_FILENAME = 'cw.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)

def download_and_save(exchange,pair):
    candles = cryptowatch_request(exchange, pair)
    
    for res, cs in candles.iteritems():
        n = 0
        
        last_candle = last_one(exchange, pair, res)
        for c in cs:
            candle = sample_one(c)
            
            if last_candle is not None and candle['Timestamp'] < last_candle['Timestamp']:
                logging.debug("Not inserting old " + candle['Timestamp'])
                continue
            logging.debug("Inserting new " + candle['Timestamp'] + " (last is " + ("None" if last_candle is None else last_candle['Timestamp']) +")")
            logging.debug('Saving '+exchange+ ' ' + pair + ' ' + str(res) + 'm ' + candle['Datetime'])
            save_one(candle, exchange, pair, res)
            n = n + 1
        logging.info('Inserted '+str(n) + ' into ' + exchange+ ' ' + pair + ' ' + str(res) + 'm')

import thread
for exchange, pairs in markets.iteritems():
    for pair in pairs:
        #spawn = thread.start_new_thread(download_and_save, (exchange, pair, )) 
        print 'spawning', exchange, pair,
        download_and_save(exchange, pair)
        print '.. done'
        

#import sys
#print("press any key to finish")
#sys.stdin.read(1)

import bgutils.Encryption

object DeployKey {

  def main(args: Array[String]) = {
    if (args.size < 1)
      println("Incorrect")
    else {

      val filename = args(0)
      print("Deploying ... ")

      Encryption.writeDecryptedFile(
        scala.io.Source.fromFile(filename).getLines.toList,
        filename.replace(".api", ".fold")
      )

      println("done.")
    }
  }

}
